xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLDIS/ConsultaSaldos/Resp/IMPL/v2017.10";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_IMPL_Resp.xsd" ::)
declare namespace ns1="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_FLCLDIS.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare namespace ns5 = "http://commons.soapwebservices.ease/xsd";

declare variable $runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::) external;

declare function local:funcionNotacion($monto as xs:string)as xs:string
{
  if(contains($monto,"E")) then(
    let $numeroAntesPuntos := substring-before($monto,'.')
    let $numeroDespuesE := substring-after($monto,'E')
    let $numroAntesDeE := substring-before($monto,'E')
    let $numroAntesDeELimpio := substring-after(xs:string($numroAntesDeE),'.')
    let $numeroLogica := fn-bea:pad-right(xs:string($numroAntesDeELimpio),xs:int($numeroDespuesE),'0')
    let $postComa := substring($numroAntesDeELimpio,xs:int($numeroDespuesE)+1,2)
    let $postComaC := if(string-length($postComa)>0)then $postComa else '00'
    let $numeroEntero := xs:string(concat($numeroAntesPuntos,$numeroLogica,'.',$postComaC))
    return
     xs:string($numeroEntero)
  )else(
    xs:string($monto)
  )
};

declare function local:func($runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::)) as element() (:: schema-element(ns2:runServiceResponse) ::) {
    <ns2:runServiceResponse>
        {
            if ($runServiceResponse/ns1:return)
            then <return>
                {
                    if ($runServiceResponse/ns1:return/ns3:claveFin)
                    then <claveFin>{fn:data($runServiceResponse/ns1:return/ns3:claveFin)}</claveFin>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:claveInicio)
                    then <claveInicio>{fn:data($runServiceResponse/ns1:return/ns3:claveInicio)}</claveInicio>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:descRetorno)
                    then <descRetorno>{fn:data($runServiceResponse/ns1:return/ns3:descRetorno)}</descRetorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:indMasDatos)
                    then <indMasDatos>{fn:data($runServiceResponse/ns1:return/ns3:indMasDatos)}</indMasDatos>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:pantPagina)
                    then <pantPagina>{fn:data($runServiceResponse/ns1:return/ns3:pantPagina)}</pantPagina>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:retorno)
                    then <retorno>{fn:data($runServiceResponse/ns1:return/ns3:retorno)}</retorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:tiempoEjecucion)
                    then <tiempoEjecucion>{fn:data($runServiceResponse/ns1:return/ns3:tiempoEjecucion)}</tiempoEjecucion>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:totalRegistros)
                    then <totalRegistros>{fn:data($runServiceResponse/ns1:return/ns3:totalRegistros)}</totalRegistros>
                    else ()
                }
                {
                    for $registros_FLCLDIS in $runServiceResponse/ns1:return/ns4:registros_FLCLDIS
                    return 
                    <registros_FLCLDIS>
                        {
                            if ($registros_FLCLDIS/ns5:registro_Numero)
                            then <registro_Numero>{fn:data($registros_FLCLDIS/ns5:registro_Numero)}</registro_Numero>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:clamon)
                            then <clamon>{fn:data($registros_FLCLDIS/ns4:clamon)}</clamon>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:codblq)
                            then <codblq>{fn:data($registros_FLCLDIS/ns4:codblq)}</codblq>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:destipolin)
                            then <destipolin>{fn:data($registros_FLCLDIS/ns4:destipolin)}</destipolin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:dislin)
                            then <dislin>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:dislin)))}</dislin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:dislinreal)
                            then <dislinreal>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:dislinreal)))}</dislinreal>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:indporlim)
                            then <indporlim>{fn:data($registros_FLCLDIS/ns4:indporlim)}</indporlim>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:indvig)
                            then <indvig>{fn:data($registros_FLCLDIS/ns4:indvig)}</indvig>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:limcrelin)
                            then <limcrelin>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:limcrelin)))}</limcrelin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:linref)
                            then <linref>{fn:data($registros_FLCLDIS/ns4:linref)}</linref>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:monaplfact)
                            then <monaplfact>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:monaplfact)))}</monaplfact>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:monminfact)
                            then <monminfact>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:monminfact)))}</monminfact>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:montotfact)
                            then <montotfact>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:montotfact)))}</montotfact>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:salautlin)
                            then <salautlin>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:salautlin)))}</salautlin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:saldeulin)
                            then <saldeulin>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:saldeulin)))}</saldeulin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:saldislin)
                            then <saldislin>{local:funcionNotacion(xs:string(fn:data($registros_FLCLDIS/ns4:saldislin)))}</saldislin>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:texblq)
                            then <texblq>{fn:data($registros_FLCLDIS/ns4:texblq)}</texblq>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:tipolim)
                            then <tipolim>{fn:data($registros_FLCLDIS/ns4:tipolim)}</tipolim>
                            else ()
                        }
                        {
                            if ($registros_FLCLDIS/ns4:tipolin)
                            then <tipolin>{fn:data($registros_FLCLDIS/ns4:tipolin)}</tipolin>
                            else ()
                        }</registros_FLCLDIS>
                }</return>
            else ()
        }</ns2:runServiceResponse>
};

local:func($runServiceResponse)