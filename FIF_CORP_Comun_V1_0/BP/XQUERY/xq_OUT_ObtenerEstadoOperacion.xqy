xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/FIF/CORP/schema/obtener/v2018.12";
declare namespace xf = "http://tempuri.org/FIF_CORP_Comun/FCC/XQUERY/xq_OUT_ObtenerEstadoOperacion/";

declare namespace functx = "http://www.functx.com";
declare variable $entradaXML as element(*) external;

declare function functx:change-element-ns-deep
  ( $nodes as node()* ,
    $newns as xs:string ,
    $prefix as xs:string )  as node()* {

  for $node in $nodes
  return if ($node instance of element())
         then (element { concat($prefix, ':', local-name($node)) }
               {$node/@*,
                functx:change-element-ns-deep($node/node(),
                                           $newns, $prefix)}
		)
        else if ($node instance of document-node()
        )
         then functx:change-element-ns-deep($node/node(),
                                           $newns, $prefix)
         else $node
 } ;

declare function xf:xq_OUT_ObtenerError($entradaXML as element(*))
    as element() {
    
    	let $respuesta:= functx:change-element-ns-deep($entradaXML, 'http://mdwcorp.falabella.com/FIF/CORP/schema/obtener/v2018.12','ns1')
    	let $mensaje := ''
        let $mensaje1 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[1]/ns1:EDESC)
        let $mensaje2 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[2]/ns1:EDESC)
        let $mensaje3 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[3]/ns1:EDESC)
        let $codigoError := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[1]/ns1:ECODE)
        let $CNT_FCUBS_WARNING_RESP := count($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)
        let $CNT_WARNING := count($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING)
        let $messagestat := $respuesta/ns1:FCUBS_HEADER/ns1:MSGSTAT/text() 
        let $operation := $respuesta/ns1:FCUBS_HEADER/ns1:OPERATION/text() 	    
        return
        if(exists($respuesta/ns1:estadoOperacion))then(
        	<estadoOperacion_TYPE>
                <codigoOperacion>{
                	if(data($respuesta/ns1:estadoOperacion/ns1:codigoOperacion) = '00')then(
                		'00'
                	)else(
                		'01'
                	)
                }</codigoOperacion>
                <glosaOperacion>{
                  if(fn:contains($respuesta/ns1:estadoOperacion/ns1:glosaOperacion,')'))then
                    fn:substring-after($respuesta/ns1:estadoOperacion/ns1:glosaOperacion,')')
                  else
                    fn:concat('[EJB]',$respuesta/ns1:estadoOperacion/ns1:glosaOperacion)
                }</glosaOperacion>
            </estadoOperacion_TYPE>
        
        )else(
	        if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityQuery') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
		        <estadoOperacion_TYPE>
	                <codigoOperacion>00</codigoOperacion>
	                <glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']','El registro se ha recuperado correctamente')}</glosaOperacion>
	            </estadoOperacion_TYPE>
	    	)else(
	    		if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityModify') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
			       <estadoOperacion_TYPE>
	                	<codigoOperacion>00</codigoOperacion>
	                	<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']','El registro se ha modificado correctamente')}</glosaOperacion>
	               </estadoOperacion_TYPE>
				)
				else(
					if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityCreate') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
						<estadoOperacion_TYPE>
	                		<codigoOperacion>00</codigoOperacion>
	                		<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']','Registro creado correctamente')}</glosaOperacion>
	               		</estadoOperacion_TYPE>
					)
					else(
						if (upper-case($messagestat) = 'SUCCESS')then (
							if ((not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) or (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP/ns1:WARNING)))then(
						        <estadoOperacion_TYPE>
	                				<codigoOperacion>00</codigoOperacion>
	                				<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']','Operacion realizada')}</glosaOperacion>
	               				</estadoOperacion_TYPE>
							)else( 
						        <estadoOperacion_TYPE>
	                				<codigoOperacion>00</codigoOperacion>
	                				<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']',data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING[$CNT_WARNING]/ns1:WDESC))}</glosaOperacion>
	               				</estadoOperacion_TYPE>
					    	)
					    )else(
					    	if (upper-case($messagestat) = 'WARNING')then (
						        <estadoOperacion_TYPE>
	                				<codigoOperacion>01</codigoOperacion>
	                				<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']',data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING[1]/ns1:WDESC))}</glosaOperacion>
	               				</estadoOperacion_TYPE>
						    )else(
						    	if ((not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP)) or (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP/ns1:ERROR)))then(
							        <estadoOperacion_TYPE>
	                					<codigoOperacion>01</codigoOperacion>
	                					<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']','Operacion fallida')}</glosaOperacion>
	               					</estadoOperacion_TYPE>
						    	)else(
						    		let $resp := if(fn:exists($mensaje3))then(fn:substring(fn:concat($mensaje1,'. ',$mensaje2,'. ',$mensaje3,'.'),1,300))else(if(fn:exists($mensaje2))then(fn:substring(fn:concat($mensaje1,'. ',$mensaje2,'.'),1,300))else(fn:substring(fn:concat($mensaje1,'.'),1,300) ))
						    		return
							    	<estadoOperacion_TYPE>
	                					<codigoOperacion>01</codigoOperacion>
	                					<glosaOperacion>{fn:concat('[',data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION),']',$resp)}</glosaOperacion>
	               					</estadoOperacion_TYPE>
						        )
					        )
					    )
					)
				)
	    	)
        )
};



xf:xq_OUT_ObtenerError($entradaXML)