xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1 = "http://mdwcorp.falabella.com/common/schema/clientservice";
declare namespace xf = "http://tempuri.org/UT_Comun_Core_V1_0/Specifications/XQUERY/xq_OUT_ComponenteSeguridad/";
declare namespace con = "http://www.bea.com/wli/sb/services/security/config";

declare namespace functx = "http://www.functx.com";
declare function functx:obtenerNombreServicio($clientService1 as element(ns1:ClientService)) as xs:string
{
    let $nombreProyecto := "FIF_CORP_SEGURIDAD"
    let $backend := "GGNG"
    let $pais:= data($clientService1/ns1:country)
    let $comercio:= data($clientService1/ns1:commerce)
    let $canal:= data($clientService1/ns1:channel) 
    
    let $servicio:=concat($nombreProyecto,"/",$backend,"/",upper-case($pais),"_",upper-case($comercio),"_",upper-case($canal))
    
    return $servicio
};

declare function xf:xq_OUT_ObtenerCredenciales($clientService1 as element(ns1:ClientService), $comercio as xs:string)
    as element() {
            let $credenciales := fn-bea:lookupBasicCredentials(functx:obtenerNombreServicio($clientService1))
    	    return
            <ServiceAccount_TYPE>
                <username>{data($credenciales/con:username)}</username>
                <password>{data($credenciales/con:password)}</password>
            </ServiceAccount_TYPE>            
};

declare variable $clientService1 as element(ns1:ClientService) external;
declare variable $comercio as xs:string external;

xf:xq_OUT_ObtenerCredenciales($clientService1,$comercio)