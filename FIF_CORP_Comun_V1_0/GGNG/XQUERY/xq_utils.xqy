xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace ggng="http://www.w3.org/2005/xquery-ggng-functions";
declare namespace cli="http://mdwcorp.falabella.com/common/schema/clientservice";

declare function ggng:obtenerPais($pais as xs:string) as xs:string{
  let $pais_ := fn:upper-case(fn:data($pais))
  return
  
  if($pais_ = "AR")then('ARG')else 
  if($pais_ = "CL")then('CHI')else 
  if($pais_ = "CO")then('COL')else 
  if($pais_ = "PE")then('PER')else 
  if($pais_ = "MX")then('MEX')else ()
 
};

declare function ggng:obtenerComercio($comercio as xs:string) as xs:string{

  let $comercio := (fn:data($comercio))
  return
  
  if($comercio = "CMR")then('CMR')else 
  if($comercio = "Banco")then('BF')else ()
 };
 
 declare function ggng:organizationCode($pais as xs:string, 
                            $negocio as xs:string) 
                            as xs:string {
    concat(ggng:obtenerComercio($negocio),ggng:obtenerPais($pais))
};

declare function ggng:obtenerClientService($ClientService) as element(cli:ClientService){
      <cli:ClientService>
         <cli:country>{fn:data($ClientService/cli:country)}</cli:country>
         <cli:commerce>{fn:data($ClientService/cli:commerce)}</cli:commerce>
         <cli:channel>GGNG</cli:channel>
      </cli:ClientService>
};