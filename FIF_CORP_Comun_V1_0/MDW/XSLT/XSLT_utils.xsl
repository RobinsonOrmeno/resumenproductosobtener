<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  
    <Table name="Monedas">
      <Record name="AR">
        <numero>032</numero>
        <codigo>ARS</codigo>
      </Record>
      <Record name="CL">
        <numero>152</numero>
        <codigo>CLP</codigo>
      </Record>
      <Record name="CO">
        <numero>170</numero>
        <codigo>COP</codigo>
      </Record>
      <Record name="MX">
        <numero>484</numero>
        <codigo>MXN</codigo>
      </Record>
      <Record name="US">
        <numero>840</numero>
        <codigo>USD</codigo>
      </Record>
    </Table> 
  </xsl:template>
</xsl:stylesheet>