xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace ns1="http://schemas.xmlsoap.org/soap/envelope/";
(:: import schema at "../../../UT_EsquemasComunes_V.2.0/Specifications/XSD/Common/MdwCorp_Common_soapFault.xsd" ::)

declare variable $fault1 as element(*) external;
declare variable $transport as element(*) external;

declare function local:func($fault1 as element(*), 
                            $transport as element(*)) 
                            as element() (:: schema-element(ns1:Fault) ::) {
    <ns1:Fault>
      <faultcode>
	                {
	                    if (data($fault1/ctx:errorCode) = 'BEA-382505') then
	                        (data("Client"))
	                    else 
	                        data("Server")
	                }
			</faultcode>
			<faultstring>{ concat("Error ",data($fault1/ctx:errorCode),": ",data($fault1/ctx:reason)) }</faultstring>
			<faultactor>
                {
                    if (data($fault1/ctx:errorCode) = "BEA-382505") then
                        (
                        if (exists($transport/ctx:request/ctx:client-host)) then
                        	data($transport/ctx:request/ctx:client-host)
                        	else
                        	()
                        )
                        
                    else 
                        data("OSB")
                }
			</faultactor>
			<detail>
            {$fault1}
            </detail>
    </ns1:Fault>
};

local:func($fault1, $transport)