xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/schema/EstadoOperacion/v2018.12";
(:: import schema at "../XSD/FIF_CORP_BO_EstadoOperacion.xsd" ::)

declare variable $codigoOperacion as xs:string external;
declare variable $glosaOperacion as xs:string external;

declare function local:XQ_OUT_string_TO_estadoOperacion($codigoOperacion as xs:string, 
                                                        $glosaOperacion as xs:string) 
                                                        as element() (:: element(*, ns1:estadoOperacion_TYPE) ::) {
    <ns1:estadoOperacion_TYPE>
        <codigoOperacion>{fn:data($codigoOperacion)}</codigoOperacion>
        <glosaOperacion>{fn:data($glosaOperacion)}</glosaOperacion>
    </ns1:estadoOperacion_TYPE>
};

local:XQ_OUT_string_TO_estadoOperacion($codigoOperacion, $glosaOperacion)