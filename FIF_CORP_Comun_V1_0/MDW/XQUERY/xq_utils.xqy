xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace my="http://www.w3.org/2005/xquery-local-functions";

declare function my:funcionNotacion($monto as xs:string)as xs:string
{
  if(contains($monto,"E")) then(
    let $numeroAntesPuntos := substring-before($monto,'.')
    let $numeroDespuesE := substring-after($monto,'E')
    let $numroAntesDeE := substring-before($monto,'E')
    let $numroAntesDeELimpio := substring-after(xs:string($numroAntesDeE),'.')
    let $numeroLogica := fn-bea:pad-right(xs:string($numroAntesDeELimpio),xs:int($numeroDespuesE),'0')
    let $postComa := substring($numroAntesDeELimpio,xs:int($numeroDespuesE)+1,2)
    let $postComaC := if(string-length($postComa)>0)then $postComa else '00'
    let $numeroEntero := xs:string(concat($numeroAntesPuntos,$numeroLogica,'.',$postComaC))
    return
     xs:string($numeroEntero)
  )else(
    xs:string($monto)
  )
};

declare function my:dateTimeToString($dateFormat as xs:string, $dateTime) as xs:string{
  let $dateTime :=
    fn-bea:pad-left(
      fn-bea:dateTime-to-string-with-format($dateFormat, $dateTime), 12)
  return $dateTime
};

(: FORMATO ENTRADA : YYYY-MM-DD
   FORMATO SALIDA  : DD-DD-YYYY:)
declare function my:converDateOutSAT($dateValue){
    let $value := xs:string(fn:concat(fn:substring(xs:string($dateValue), 7, 4), '-', fn:substring(xs:string($dateValue), 4, 2),  '-', fn:substring(xs:string($dateValue), 1, 2)))
    return $value
  };