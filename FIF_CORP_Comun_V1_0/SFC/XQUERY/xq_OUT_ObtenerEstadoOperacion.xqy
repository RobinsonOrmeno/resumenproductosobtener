xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/FIF/CORP/schema/obtener/v2018.12";
declare namespace xf = "http://tempuri.org/FIF_CORP_Comun_V1_0/SFC/XQUERY/xq_OUT_ObtenerEstadoOperacion/";

declare namespace functx = "http://www.functx.com";

declare function functx:change-element-ns-deep
  ( $nodes as node()* ,
    $newns as xs:string ,
    $prefix as xs:string )  as node()* {

  for $node in $nodes
  return if ($node instance of element())
         then (element { concat($prefix, ':', local-name($node)) }
               {$node/@*,
                functx:change-element-ns-deep($node/node(),
                                           $newns, $prefix)}
		)
        else if ($node instance of document-node()
        )
         then functx:change-element-ns-deep($node/node(),
                                           $newns, $prefix)
         else $node
 } ;

declare function xf:xq_OUT_ObtenerError($entradaXML as element(*),$catalogo as xs:string)
    as element() {
        let $respuesta := functx:change-element-ns-deep($entradaXML, 'http://mdwcorp.falabella.com/FIF/CORP/schema/obtener/v2018.12','ns1')
		let $registroT := $respuesta/ns1:return/ns1:*/ns1:codrespsal
		let $descripTr := $respuesta/ns1:return/ns1:*/ns1:desrespsal
		let $codigoSFC := if(string-length(xs:string(fn:data($registroT))) > 0)then fn:data($registroT) else fn:data(xs:string('SIN_CODIGO'))
		let $valida	   := if(fn:data($codigoSFC) = 'MPA0166' or fn:data($codigoSFC) = 'SIN_CODIGO')then '0' else '1'
        return
            <estadoOperacion_TYPE>{
                if(exists($respuesta/ns1:return))then(
                  <codigoOperacion>{
                          if(data($respuesta/ns1:return/ns1:retorno) = '000' and data($valida) = '0')then(
                                  '00'
                          )else(
                                  '01'
                          )
                  }</codigoOperacion>,
                  <glosaOperacion>{
					if(data($valida) = '0')then(
						concat("[",$catalogo,"]"," ",data($respuesta/ns1:return/ns1:descRetorno))
					)else(
						concat("[",$catalogo,"]"," ",data($descripTr))
					)
				  }</glosaOperacion>
                )else if(exists($respuesta/ns1:mensaje))then(
                  <codigoOperacion>{'01'}</codigoOperacion>,
                  <glosaOperacion>{data($respuesta/ns1:mensaje)}</glosaOperacion>
                )else(
                  <codigoOperacion>{
                          if(data($respuesta/ns1:OperationResult/ns1:estadoOperacion/ns1:codigoOperacion) = '00')then(
                                  '00'
                          )else(
                                  '01'
                          )
                  }</codigoOperacion>,
                  <glosaOperacion>{concat("[",$catalogo,"]"," ",data($respuesta/ns1:OperationResult/ns1:estadoOperacion/ns1:glosaOperacion))}</glosaOperacion>
                )
            }</estadoOperacion_TYPE>
};

declare variable $entradaXML as element(*) external;
declare variable $catalogo as xs:string external;

xf:xq_OUT_ObtenerError($entradaXML, $catalogo)