<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="2.0" encoding="UTF-8" 
indent="yes"/>
  <xsl:template match="Operaciones">
      <listaParametros>
         <glosa name="P7542600-i">INSERT</glosa>
         <glosa name="P7542600-s">SELECT</glosa>
         <glosa name="P755302">VIEW</glosa>
		 <glosa name="P7542602">VIEW</glosa>
         <glosa name="P7552800">VIEW</glosa>
         <glosa name="P7543000">VIEW</glosa>
         <glosa name="P7543200">VIEW</glosa>
      </listaParametros>
  </xsl:template>
  
   <xsl:template match="Entidades">
      <listaParametros>
          <glosa name="AR">7012</glosa>	  
          <glosa name="MX">0252</glosa>
          <glosa name="PE">0142</glosa>	
          <glosa name="CO">0062</glosa>
          <glosa name="CL">0015</glosa>
      </listaParametros>
  </xsl:template>
  
</xsl:stylesheet>