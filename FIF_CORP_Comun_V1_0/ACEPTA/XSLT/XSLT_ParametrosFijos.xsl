<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="2.0" encoding="UTF-8" 
indent="yes"/>

  <xsl:template match="PE">
      <listaParametros>
          <glosa name="INSTITUCION">BANCOFALABELLA</glosa>
          <glosa name="GROUP_TAGS">AgrupadorEnvio</glosa>
          <glosa name="TEMPLATE_EMAIL">Email_captacion</glosa>
      </listaParametros>
  </xsl:template>
  
</xsl:stylesheet>