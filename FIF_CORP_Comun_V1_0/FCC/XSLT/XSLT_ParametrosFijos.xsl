<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="2.0" encoding="UTF-8" 
indent="yes"/>

  <xsl:template match="Service_FCUBSAccService">
      <listaParametros>
          <glosa name="Service">FCUBSAccService</glosa>
          <glosa name="CheckBookModify">CheckBookModify</glosa>
          <glosa name="CheckBookQuery">CheckBookQuery</glosa>
          <glosa name="CheckDetailsQuery">CheckDetailsQuery</glosa>
          <glosa name="CreateCustAcc">CreateCustAcc</glosa>
          <glosa name="CreateStopPaymentsReversal">CreateStopPaymentsReversal</glosa>
          <glosa name="ModifyCustAcc">ModifyCustAcc</glosa>
          <glosa name="QueryAccBal">QueryAccBal</glosa>
          <glosa name="QueryAccMove">QueryAccMove</glosa>
          <glosa name="QueryChqAccDtls">QueryChqAccDtls</glosa>
          <glosa name="QueryChqStopPayDtls">QueryChqStopPayDtls</glosa>
          <glosa name="QueryCustAcc">QueryCustAcc</glosa>
          <glosa name="QueryCustRelAcc">QueryCustRelAcc</glosa>
          <glosa name="StopPaymentsNew">StopPaymentsNew</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSCLService">
      <listaParametros>
          <glosa name="Service">FCUBSCLService</glosa>
          <glosa name="CreateAccount">CreateAccount</glosa>
          <glosa name="CreateAccountSimulation">CreateAccountSimulation</glosa>
          <glosa name="CreateCreditRestructure">CreateCreditRestructure</glosa>
          <glosa name="CreatePayment">CreatePayment</glosa>
          <glosa name="QueryAccount">QueryAccount</glosa>
          <glosa name="QueryClAccounts">QueryClAccounts</glosa>
          <glosa name="QueryCLEventDairy">QueryCLEventDairy</glosa>
          <glosa name="QueryCustSum">QueryCustSum</glosa>
          <glosa name="QueryCLPayment">QueryCLPayment</glosa>
          <glosa name="QuerySimPrePay">QuerySimPrePay</glosa>
          <glosa name="QuerySimTotPay">QuerySimTotPay</glosa>
          <glosa name="QueryPayment">QueryPayment</glosa>
          <glosa name="SaveCLAdhoc">SaveCLAdhoc</glosa>
          <glosa name="SaveReversal">SaveReversal</glosa>    
          <glosa name="SimPayment">SimPayment</glosa>
          <glosa name="QueryProduct">QueryProduct</glosa>
          </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSCoreService">
      <listaParametros>
          <glosa name="Service">FCUBSCoreService</glosa>
          <glosa name="QueryCustAccDtls">QueryCustAccDtls</glosa>
          <glosa name="QueryEmbargos">QueryEmbargos</glosa>
          <glosa name="QueryEmbargoEntry">QueryEmbargoEntry</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSCGService">
      <listaParametros>
          <glosa name="Service">FCUBSCGService</glosa>
          <glosa name="CreateChkProtContract">CreateChkProtContract</glosa>
          <glosa name="CreateTransaction">CreateTransaction</glosa>
          <glosa name="ReverseChkProtContract">ReverseChkProtContract</glosa>
          <glosa name="QueryChqProtestAccDtls">QueryChqProtestAccDtls</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSCustomerService">
      <listaParametros>
          <glosa name="Service">FCUBSCustomerService</glosa>
          <glosa name="CreateCustomer">CreateCustomer</glosa>
          <glosa name="QueryCustomer">QueryCustomer</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSDDService">
      <listaParametros>
          <glosa name="Service">FCUBSDDService</glosa>
          <glosa name="CancelTransaction">CancelTransaction</glosa>
          <glosa name="CreateTransaction">CreateTransaction</glosa>
          <glosa name="CreateDDInstrumentOperation">CreateDDInstrumentOperation</glosa>
          <glosa name="QueryDDTransaction">QueryDDTransaction</glosa>
          <glosa name="LiquidateTransaction">LiquidateTransaction</glosa>
          <glosa name="UploadReprint">UploadReprint</glosa>
          <glosa name="ReverseTransaction">ReverseTransaction</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_LSFacilityService">
      <listaParametros>
          <glosa name="Service">LSFacilityService</glosa>
          <glosa name="FacilityCreate">FacilityCreate</glosa>
          <glosa name="FacilityModify">FacilityModify</glosa>
          <glosa name="QueryCustomer">FacilityQuery</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_LSCustomerLiabService">
      <listaParametros>
          <glosa name="Service">LSCustomerLiabService</glosa>
          <glosa name="CustomerLiabCreate">CustomerLiabCreate</glosa>
          <glosa name="CustomerLiabQuery">CustomerLiabQuery</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_LSLiabilityService">
      <listaParametros>
          <glosa name="Service">LSLiabilityService</glosa>
          <glosa name="LiabilityCreate">LiabilityCreate</glosa>
          <glosa name="LiabilityQuery">LiabilityQuery</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="Service_FCUBSSTService">
      <listaParametros>
          <glosa name="Service">FCUBSSTService</glosa>
          <glosa name="CreateManualStatChg">CreateManualStatChg</glosa>
          <glosa name="CreateReqAccntClosure">CreateReqAccntClosure</glosa>
          <glosa name="QueryAccClasMaint">QueryAccClasMaint</glosa>
          <glosa name="QueryBranchSysDates">QueryBranchSysDates</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="AR">
      <listaParametros>
          <glosa name="SOURCE">FLEXSCHFS</glosa>
          <glosa name="UBSCOMP">FCUBS</glosa>
          <glosa name="USERID">SERARUSR01</glosa>
          <glosa name="BRANCH">999</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="CL">
      <listaParametros>
          <glosa name="SOURCE">FLEXSCHFS</glosa>
          <glosa name="UBSCOMP">FCUBS</glosa>
          <glosa name="USERID">SERCLUSR01</glosa>
          <glosa name="BRANCH">999</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="CO">
      <listaParametros>
          <glosa name="SOURCE">FLEXSCHFS</glosa>
          <glosa name="UBSCOMP">FCUBS</glosa>
          <glosa name="USERID">SERCOUSR01</glosa>
          <glosa name="BRANCH">170</glosa>
      </listaParametros>
  </xsl:template>
  
  <xsl:template match="MX">
      <listaParametros>
          <glosa name="SOURCE">FLEXSCHFS</glosa>
          <glosa name="UBSCOMP">FCUBS</glosa>
          <glosa name="USERID">SERMEX01</glosa>
          <glosa name="BRANCH">999</glosa>
      </listaParametros>
  </xsl:template>
  
    <xsl:template match="PE">
      <listaParametros>
          <glosa name="SOURCE">FLEXSCHFS</glosa>
          <glosa name="UBSCOMP">FCUBS</glosa>
          <glosa name="USERID">SERPEUSR01</glosa>
          <glosa name="BRANCH">603</glosa>
      </listaParametros>
  </xsl:template>
  
</xsl:stylesheet>