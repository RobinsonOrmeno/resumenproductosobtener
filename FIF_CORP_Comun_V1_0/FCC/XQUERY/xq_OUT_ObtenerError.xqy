xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns0="http://mdwcorp.falabella.com/FIF/CORP/schema/Resultado/v2015.11";
(:: import schema at "../XSD/FIF_CORP_BO_Resultado.xsd" ::)
declare namespace ns1 = "http://mdwcorp.falabella.com/FIF/CORP/schema/Error/v2016.02";

declare variable $anyType1 as element(*) external;

declare function local:change-element-ns-deep
  ( $nodes as node()* ,
    $newns as xs:string ,
    $prefix as xs:string )  as node()* {

  for $node in $nodes
  return if ($node instance of element())
         then (element { concat($prefix, ':', local-name($node)) }
               {$node/@*,
                local:change-element-ns-deep($node/node(),
                                           $newns, $prefix)}
		)
        else if ($node instance of document-node()
        )
         then local:change-element-ns-deep($node/node(),
                                           $newns, $prefix)
         else $node
 } ;

declare function local:func($anyType1 as element(*)) as element() (:: element(*, ns0:ResultadoType) ::) {

            let $respuesta:= local:change-element-ns-deep($anyType1, 'http://mdwcorp.falabella.com/FIF/CORP/schema/Error/v2016.02','ns1')
            let $mensaje := ''
	    let $mensaje1 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[1]/ns1:EDESC)
	    let $mensaje2 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[2]/ns1:EDESC)
	    let $mensaje3 := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[3]/ns1:EDESC)
            let $codigoError := data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP[1]/ns1:ERROR[1]/ns1:ECODE)
	    let $CNT_FCUBS_WARNING_RESP := count($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)
	    let $CNT_WARNING := count($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING)
	    let $messagestat := $respuesta/ns1:FCUBS_HEADER/ns1:MSGSTAT/text() 
	    let $operation := $respuesta/ns1:FCUBS_HEADER/ns1:OPERATION/text() 	    
	    return

if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityQuery') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
    		<ns0:ResultadoType>
	            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
    			<ns0:codigo>0</ns0:codigo>
    			<ns0:mensaje>El registro se ha recuperado correctamente</ns0:mensaje>    			
	        </ns0:ResultadoType>
    	)else(
    		if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityModify') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
				<ns0:ResultadoType>
		            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
					<ns0:codigo>0</ns0:codigo>
					<ns0:mensaje>El registro se ha modificado correctamente</ns0:mensaje>    			
		        </ns0:ResultadoType>
			)
			else(
				if ((upper-case($messagestat) = 'SUCCESS') and (data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION) = 'FacilityCreate') and (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) )then (
					<ns0:ResultadoType>
						<ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
						<ns0:codigo>0</ns0:codigo>
						<ns0:mensaje>Registro creado correctamente</ns0:mensaje>    			
					</ns0:ResultadoType>
				)
				else(
					if (upper-case($messagestat) = 'SUCCESS')then (
						if ((not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP)) or (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP/ns1:WARNING)))then(
							<ns0:ResultadoType>
					            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
				    			<ns0:codigo>0</ns0:codigo>
				    			<ns0:mensaje>Operacion realizada</ns0:mensaje>    			
					        </ns0:ResultadoType>
						)else( 
					        <ns0:ResultadoType>
					            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
				    			<ns0:codigo>0</ns0:codigo>
				    			<ns0:mensaje>{ data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING[$CNT_WARNING]/ns1:WDESC)}</ns0:mensaje>    			
					        </ns0:ResultadoType>
				    	)
				    )else(
				    	if (upper-case($messagestat) = 'WARNING')then (
					        <ns0:ResultadoType>
					            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
				    			<ns0:codigo>1</ns0:codigo>
				    			<ns0:mensaje>{ data($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_WARNING_RESP[1]/ns1:WARNING[1]/ns1:WDESC)}</ns0:mensaje>    			
					        </ns0:ResultadoType>
					    )else(
					    	if ((not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP)) or (not($respuesta/ns1:FCUBS_BODY/ns1:FCUBS_ERROR_RESP/ns1:ERROR)))then(
					    		<ns0:ResultadoType>
						            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
					    			<ns0:codigo>{if($codigoError='2')then(2)else(1)}</ns0:codigo>
					    			<ns0:mensaje>Operacion fallida</ns0:mensaje>    			
						        </ns0:ResultadoType>
					    	)else(
						    	<ns0:ResultadoType>
						            <ns0:catalogo>{ fn:concat(data($respuesta/ns1:FCUBS_HEADER/ns1:SERVICE),'.' , data($respuesta/ns1:FCUBS_HEADER/ns1:OPERATION)) }</ns0:catalogo>
					    			<ns0:codigo>{if($codigoError='2')then(2)else(1)}</ns0:codigo>
					    			<ns0:mensaje>{ if(fn:exists($mensaje3))then(fn:substring(fn:concat($mensaje1,'. ',$mensaje2,'. ',$mensaje3,'.'),1,300))else(if(fn:exists($mensaje2))then(fn:substring(fn:concat($mensaje1,'. ',$mensaje2,'.'),1,300))else(fn:substring(fn:concat($mensaje1,'.'),1,300) )) }</ns0:mensaje>    			
						        </ns0:ResultadoType>
					        )
				        )
				    )
					
				)
			)
	    )
};

local:func($anyType1)