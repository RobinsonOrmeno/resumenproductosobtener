xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace xf = "http://tempuri.org/FIF_CORP_Comun_V1_0/SAT/XQUERY/xq_OUT_DesfragmentarContrato_CL/";

declare function xf:xq_OUT_DesfragmentarContrato_CL($contrato as xs:string)
    as element(*) {
    
        if(string-length($contrato) = 20)then(
          <contrato>
                  <entidad>{substring(data($contrato),1,4)}</entidad>
                  <centalta>{substring(data($contrato),5,4)}</centalta>
                  <cuenta>{substring(data($contrato),9,12)}</cuenta>
          </contrato>
        )else(
          <contrato>
            <mensaje>El contrato ingresado no cumple con el largo adecuado (20)</mensaje>
          </contrato>
        )
};

declare variable $contrato as xs:string external;

xf:xq_OUT_DesfragmentarContrato_CL($contrato)