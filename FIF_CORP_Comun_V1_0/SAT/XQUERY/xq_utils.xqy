xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace sat="http://www.w3.org/2005/xquery-sat-functions";


declare function sat:getFechaFacturacion($grupoliq as xs:integer, $fechaFact as xs:date) as xs:date{

    let $isBisiesto := sat:is-leap-year(xs:date($fechaFact))
    let $targetMonth := fn:month-from-date(xs:date($fechaFact))
    let $targetYear := fn:year-from-date($fechaFact)
    let $factDay := if (
    
    $grupoliq = 30 and $targetMonth = 2)
    then if($isBisiesto) then 29 else 28
    else ($grupoliq)
    let $result := xs:date(
    fn:concat(fn-bea:pad-left(xs:string($targetYear),4,"0"),
    '-',
    fn-bea:pad-left(xs:string($targetMonth),2,"0"),
    '-',
    fn-bea:pad-left(xs:string($factDay),2,"0")))
    return $result
};

declare function sat:getFechaVencimiento($grupoliq as xs:integer, $fechaVen as xs:date) as xs:date{

    let $targetMonth := fn:month-from-date(xs:date($fechaVen))
    let $targetYear := fn:year-from-date($fechaVen)
    let $targetDay := fn:day-from-date($fechaVen)
    let $factDay := 
        if($grupoliq = 15 and $targetMonth = 2)then
            data($targetDay)
        else
            data($targetDay + 1)

    let $result := xs:date(
    fn:concat(fn-bea:pad-left(xs:string($targetYear),4,"0"),
    '-',
    fn-bea:pad-left(xs:string($targetMonth),2,"0"),
    '-',
    fn-bea:pad-left(xs:string($factDay),2,"0")))
    return $result
};

declare function sat:is-leap-year($date as xs:date) as xs:boolean {
    for $year in xs:integer(substring(string($date), 1, 4))
    return ($year mod 4 = 0 and
    $year mod 100 != 0) or
    $year mod 400 = 0
};