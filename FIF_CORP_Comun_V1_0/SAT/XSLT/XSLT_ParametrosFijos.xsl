<?xml version="1.0" encoding="windows-1252" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="2.0" encoding="UTF-8" 
indent="yes"/>
  <xsl:template match="Operaciones">
      <listaParametros>
         <glosa name="ADALCON">INSERT</glosa>
         <glosa name="ADALTAR">INSERT</glosa>
         <glosa name="ADMOLIM">UPDATE</glosa>
         <glosa name="FLALDMI">INSERT</glosa>
         <glosa name="FLBAPCF">DELETE</glosa>
         <glosa name="FLCLCC2">VIEW</glosa>
         <glosa name="FLCLCCT">VIEW</glosa>
         <glosa name="FLCLCL2">VIEW</glosa>
         <glosa name="FLCLCOM">VIEW</glosa>
         <glosa name="FLCLCTD">VIEW</glosa>
         <glosa name="FLCLDIS">VIEW</glosa>
         <glosa name="FLCLMOV">VIEW</glosa>
         <glosa name="FLCLPCF">VIEW</glosa>
         <glosa name="FLCLTCO">VIEW</glosa>
         <glosa name="FLCOPCF">SELECT</glosa>
         <glosa name="FLCORE2">SELECT</glosa>
         <glosa name="FLCOTPC">SELECT</glosa>
         <glosa name="FLCOSCC">VIEW</glosa>
         <glosa name="FLMOLIM">UPDATE</glosa>
         <glosa name="FLMOCTD">UPDATE</glosa>
         <glosa name="FLCLTRA">VIEW</glosa>
         <glosa name="IMCLICA">VIEW</glosa>
         <glosa name="OPALACC">INSERT</glosa>
         <glosa name="OPALCUO">VIEW</glosa>
         <glosa name="OPCLCCC">VIEW</glosa>
         <glosa name="OPCLCEM">VIEW</glosa>
         <glosa name="OPCLCRP">VIEW</glosa>
         <glosa name="OPCLCUO">VIEW</glosa>
         <glosa name="OPCLEXT">VIEW</glosa>
         <glosa name="OPCLIMP">VIEW</glosa>
         <glosa name="OPCLMOC">VIEW</glosa>
         <glosa name="OPCLRMA">VIEW</glosa>
         <glosa name="OPCNCAN">SELECT</glosa>
         <glosa name="OPCOCUO">SELECT</glosa>
         <glosa name="OPCOETI">VIEW</glosa>
         <glosa name="OPCOEXT">SELECT</glosa>
         <glosa name="OPCOIMS">VIEW</glosa>
         <glosa name="OPCOMOC">SELECT</glosa>
         <glosa name="OPCORMA">SELECT</glosa>
         <glosa name="OPMOCUO">UPDATE</glosa>
         <glosa name="TIALBCT">INSERT</glosa>
         <glosa name="TICLRTL">VIEW</glosa>
         <glosa name="TICO409">SELECT</glosa>
         <glosa name="TICOBLQ">VIEW</glosa>
         <glosa name="TICOLIM">SELECT</glosa>
         <glosa name="TICOTAR">SELECT</glosa>
         <glosa name="TICOUPP">SELECT</glosa>
         <glosa name="TIMOARE">UPDATE</glosa>
         <glosa name="TIMOBLO">UPDATE</glosa>
         <glosa name="TIMOCCT">UPDATE</glosa>
         <glosa name="TIMOCTT">UPDATE</glosa>
         <glosa name="TIMONTT">UPDATE</glosa>
         <glosa name="TIMOUPM">UPDATE</glosa>
         <glosa name="TIMOUPP">UPDATE</glosa>
         <glosa name="TIMOUPT">UPDATE</glosa>
         <glosa name="TIRERTT">UPDATE</glosa>		  
      </listaParametros>
  </xsl:template>
  
   <xsl:template match="Entidades">
      <listaParametros>
          <glosa name="AR">7012</glosa>	  
          <glosa name="MX">0252</glosa>
          <glosa name="PE">7012</glosa>
      </listaParametros>
  </xsl:template>
  
</xsl:stylesheet>