xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

module namespace az="http://www.w3.org/2005/xquery-local-az-functions";

  declare function az:tipoDocumento($documento as xs:string)as xs:string{
 	if($documento = 'DNI') then '01'
 	else if($documento = 'CARNET_DE_EXTRANJERIA') then '02'
 	else if($documento = 'CARNET_FFPP') then '03'
 	else if($documento = 'CARNET_FFAA') then '04'
 	else if($documento = 'PASAPORTE') then '05'
 	else '01'
 };
 
 
 
 declare function az:formateaMontoTotal($decimal as item()*) as xs:string{
	let $cantidadEnteros:=10
	let $cantidadDecimales:=2
	let $separadorDecimalReemplazo  := ','
	let $strDecimal := az:trimNotEmpty($decimal)
	let $double  := fn-bea:trim($strDecimal)
	let $enteros := (
		if(string-length(fn:substring-before($double, '.'))>0)
		then fn:substring-before($double, '.')
		else $double
	)
	let $decimales  := fn:substring-after($double, '.')
	let $decimalesCortados  := fn:substring(
			fn-bea:pad-right($decimales, $cantidadDecimales, '0'), 
			1,
			$cantidadDecimales
	)
	let $llevaSigno := xs:boolean(
				if (fn:starts-with($double,'-')) 
				then 'true'
		        else 'false'
	)
	let $enterosSinSigno := (
		        if ($llevaSigno) 
		        then fn:substring-after($enteros, '-')
		        else $enteros
	)
	let $signo := (
				if ($llevaSigno) 
				then '-'
		        else ''
	)
	let $numeroCompletoSinSigno := (
		if($cantidadDecimales>0) then 
			concat($enterosSinSigno,$decimalesCortados) 
		else 
			$enterosSinSigno
	)
	let $largoTotal := $cantidadEnteros + $cantidadDecimales
	let $salida := (
		if ($llevaSigno) then
			fn-bea:pad-left($numeroCompletoSinSigno, $largoTotal -1, '0')
		else
			fn-bea:pad-left($numeroCompletoSinSigno, $largoTotal, '0')
	)
	return concat($signo,$salida)
};


declare function az:trimNotEmpty($str as item()*)as xs:string{
  	if ( empty($str) ) then ('')
  	else (fn-bea:trim( xs:string($str) ))
 };
 
 
 
 declare function az:getFormatTotalAmountToCharge($numero as xs:string)
    as xs:string
    {

let $res :=$numero
let $largo :=fn:string-length($numero)
let $parteEntera :=  fn:substring($numero,1,xs:int($largo)-2)
let $parteDecimal :=  fn:substring($numero,xs:int($largo)-1,2)
return
if ($parteEntera = "") then 
fn:concat('0.', $parteDecimal)
else
fn:concat(xs:string(xs:integer($parteEntera)) , '.', $parteDecimal)
};

declare function az:getFormatAmountOfInstallment($bloque120 as xs:string, $tipoMonto as  xs:string)
    as xs:string
    {
let $res1 :=fn:substring($bloque120,3,2)
let $res2 :=fn:substring($bloque120,23,2)
let $res3 :=fn:substring($bloque120,43,2)
let $res4 :=fn:substring($bloque120,63,2)
let $res5 :=fn:substring($bloque120,83,2)
 
return
if(fn:data($res1 = $tipoMonto )) then
			(xs:string(xs:integer(fn:substring($bloque120,9,12))))
		else if(fn:data($res2 = $tipoMonto )) then
			(xs:string(xs:integer(fn:substring($bloque120,29,12))))
		else if(fn:data($res3 = $tipoMonto )) then
			(xs:string(xs:integer(fn:substring($bloque120,49,12))))
		else if(fn:data($res4 = $tipoMonto )) then
			(xs:string(xs:integer(fn:substring($bloque120,69,12))))
		else if(fn:data($res5 = $tipoMonto )) then
			(xs:string(xs:integer(fn:substring($bloque120,89,12))))
                                               else
                                '0'
};

declare function az:functx_MMdd($DateString as xs:string)as xs:string{
  	xs:string(replace($DateString,'^\D*(\d{4})\D*(\d{2})\D*(\d{2})\D*$','$2$3'))
 };
 
 declare function az:esTotal($str as xs:string)as xs:string{
  	if ( fn:upper-case($str) = 'SI' ) then 'T'
  	else 'P'
 };