<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />
	<xsl:template match="/">
		<TablaCodigoSubProducto	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<Table>
				<Record>
					<codigoEntrada>00</codigoEntrada>
					<codigoSalida>0</codigoSalida>
					<glosa>Aprobado</glosa>
				</Record>
				<Record>
					<codigoEntrada>01</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [01 - Validar error con emisor]</glosa>
				</Record>
				<Record>
					<codigoEntrada>02</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [02 - Validar con emisor condiciones especiales]</glosa>
				</Record>
				<Record>
					<codigoEntrada>03</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [03 - Codigo de comercio invalido]</glosa>
				</Record>
				<Record>
					<codigoEntrada>04</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [04 - Tarjeta en recepcion]</glosa>
				</Record>
				<Record>
					<codigoEntrada>05</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [05 - No honra compra original]</glosa>
				</Record>
				<Record>
					<codigoEntrada>06</codigoEntrada>
					<codigoSalida>98</codigoSalida>
					<glosa>Error técnico Switch [06 - Error]</glosa>
				</Record>
				<Record>
					<codigoEntrada>07</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [07 - Tarjeta invalida]</glosa>
				</Record>
				<Record>
					<codigoEntrada>11</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [11 - Cheque preautorizado]</glosa>
				</Record>
				<Record>
					<codigoEntrada>12</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [12 - Transaccion invalida]</glosa>
				</Record>
				<Record>
					<codigoEntrada>13</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [13 - Monto invalido]</glosa>
				</Record>
				<Record>
					<codigoEntrada>14</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [14 - Numero de tarjeta invalida]</glosa>
				</Record>
				<Record>
					<codigoEntrada>15</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [15 - No existe el emisor]</glosa>
				</Record>
				<Record>
					<codigoEntrada>17</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [17 - Cancelado por el cliente]</glosa>
				</Record>
				<Record>
					<codigoEntrada>19</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [19 - Reingreso de transaccion]</glosa>
				</Record>
				<Record>
					<codigoEntrada>21</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [21 - No se puede reversar]</glosa>
				</Record>
				<Record>
					<codigoEntrada>25</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [25 - No existe el registro o la cuenta no existe]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>28</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [28 - Archivo no disponible]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>29</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [29 - Archivo no actualizado con adquirente]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>30</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [30 - Error de formato]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>31</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [31 - Banco no soportado por Switch]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>32</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [32 - Completado parcialmente]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>33</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [33 - Tarjeta expirada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>34</codigoEntrada>
					<codigoSalida>97</codigoSalida>
					<glosa>Sospecha Fraude [34 - Se sospecha fraude]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>35</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [35 - Contactar al adquirente]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>36</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [36 - Tarjeta restringida]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>37</codigoEntrada>
					<codigoSalida>97</codigoSalida>
					<glosa>Sospecha Fraude [37 - Se debe validar seguridad con adquirente]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>38</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [38 - Excede el numero de ingresos del PIN]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>39</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [39 - Cuenta no existe]</glosa>
				</Record>				
				<Record>
					<codigoEntrada>40</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [40 - Funcion no soportada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>41</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [41 - Tarjeta perdida o bloqueada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>42</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [42 - Sin cuenta universal]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>43</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [43 - Tarjeta robada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>44</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [44 - Sin cuenta de inversion]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>45</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error tecnico Backend [45 -  Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>50</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error tecnico Backend [50 -  Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>51</codigoEntrada>
					<codigoSalida>90</codigoSalida>
					<glosa>Saldo insuficiente [51 - Sin fondos suficientes]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>52</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [52 - No existe la cuenta]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>53</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [53 - Sin cuenta de ahorro]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>54</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [54 - Tarjeta expirada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>55</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [55 - PIN incorrecto]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>56</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [56 - Sin registro de tarjeta]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>57</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [57 - Transaccion no permitida a tarjeta]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>58</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [58 - Transaccion no permitida a terminal]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>59</codigoEntrada>
					<codigoSalida>97</codigoSalida>
					<glosa>Sospecha Fraude [59 - Se sospecha fraude]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>60</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [60 - Se debe contactar al adquirente]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>61</codigoEntrada>
					<codigoSalida>90</codigoSalida>
					<glosa>Saldo insuficiente [61 - Excede monto de avance]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>62</codigoEntrada>
					<codigoSalida>95</codigoSalida>
					<glosa>Transacción no es permitida [62 - Tarjeta restringida]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>63</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Sospecha Fraude [63 - Violacion de seguridad]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>64</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [64 - Monto original incorrecto]</glosa>
				</Record>		
				<Record>
					<codigoEntrada>65</codigoEntrada>
					<codigoSalida>90</codigoSalida>
					<glosa>Saldo insuficiente [65 - Excede el monto límite de retiro o compra diario acumulado]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>66</codigoEntrada>
					<codigoSalida>97</codigoSalida>
					<glosa>Sospecha fraude [66 - Se sospecha fraude]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>67</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [67 - Tarjeta bloqueada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>68</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [68 - Respuesta recibida muy tarde]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>69</codigoEntrada>
					<codigoSalida>98</codigoSalida>
					<glosa>Error técnico Switch [69 - Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>74</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [74 - Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>75</codigoEntrada>
					<codigoSalida>91</codigoSalida>
					<glosa>Datos de entrada inválidos [75 - Numero de reintentos de PIN excedido]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>76</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [76 - Error en sincronizacion de la llave]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>77</codigoEntrada>
					<codigoSalida>98</codigoSalida>
					<glosa>Error técnico Switch [77 - Error tecnico]</glosa>
				</Record>	
				<Record>
					<codigoEntrada>79</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [79 - Error al registrar la transacción para gestión de comisiones]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>89</codigoEntrada>
					<codigoSalida>98</codigoSalida>
					<glosa>Error técnico Switch [89 - Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>90</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [90 - Corte horario en curso, reintentar]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>91</codigoEntrada>
					<codigoSalida>98</codigoSalida>
					<glosa>Error técnico Switch [91 - Emisor o Switch no conectado]</glosa>
				</Record>				
				<Record>
					<codigoEntrada>92</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Switch [92 - Error en el ruteo]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>93</codigoEntrada>
					<codigoSalida>93</codigoSalida>
					<glosa>Transacción no encontrada [93 - Transaccion no puede ser identificada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>94</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [94 - Transmicion duplicada]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>95</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [95 - Error en conciliacion]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>96</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [96 - Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>97</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [97 - Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>99</codigoEntrada>
					<codigoSalida>99</codigoSalida>
					<glosa>Error técnico Backend [Error generico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>N1</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [N1 - Cuenta no ha sido activada]</glosa>
				</Record>
				<Record>
					<codigoEntrada>0A</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [N1 - Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>9Z</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [9Z - Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>A0</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A0 - Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>MZ</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [MZ - Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>N0</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [N0 - Error tecnico]</glosa>
				</Record>
				<Record>
					<codigoEntrada>ZY</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>uenta con problemas [Error tecnico]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A1</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A1 - Tipo venta diferida no valida]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A2</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A2 - Cliente no es empleado]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A3</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A3 - Cuota no valida]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A4</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A4 - Monto excede maximo de pago]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A5</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A5 - Sin repactacion]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A6</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A6 - No corresponde devolucion]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A7</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A7 - Puntos insuficientes]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A8</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A8 - Producto no esta en catalogo]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>A9</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A9 - Cliente con saldo en mora]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AA</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AA - Cuenta Supera limite de $30,000 mensual]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AB</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AB - Cuenta CMR Bloqueada Para Cargos]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AC</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AC - Celular cargando con otra cuenta]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AD</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AD - Celular supera los 30000 mensual]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AE</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AE - Movimiento no corresponde a compania]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AF</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AF - Ya tiene un contrato anterior]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AG</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AG - Maximo de contratos RAC o PAC]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AH</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AH - Contrato bloqueado sin desbloqueo temporal]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AI</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AI - RUT invalido]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AJ</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AJ - Falla limite monto por Comercio]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AK</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AK - Falla limite operaciones por Comercio]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AL</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AL - Falla limite de cuotas por Comercio]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AM</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AM - No encontro original a reversar]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AN</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AN - Valor cuota fuera de rango por tipo compra en cuotas]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AO</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [A0 - Importe transaccion fuera de rango por tipo compra cuotas]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>AP</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [AP - Pago aceptado con repactacion]</glosa>
				</Record>			
				<Record>
					<codigoEntrada>TI</codigoEntrada>
					<codigoSalida>94</codigoSalida>
					<glosa>Cuenta con problemas [TI - Tarjeta Invalida]</glosa>
				</Record>			
			</Table>
		</TablaCodigoSubProducto>
	</xsl:template>
</xsl:stylesheet>