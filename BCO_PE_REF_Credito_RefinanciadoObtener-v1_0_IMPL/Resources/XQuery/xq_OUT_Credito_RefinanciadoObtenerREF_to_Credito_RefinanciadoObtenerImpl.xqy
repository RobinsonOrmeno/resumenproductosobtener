xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$obtenerCreditosRefinanciadosResponse1" element="ns0:ObtenerCreditosRefinanciadosResponse" location="../WSDL/Refinanciados.asmx.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:creditoRefinanciadoObtenerRespParam" location="../Schemas/OSB_REF_Credito_Refinanciado_ObtenerResp.xsd" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ref/credito/refinanciado/obtener/Resp-v2016.02";
declare namespace ns0 = "http://falabella.refinanciados.servicio.pe";
declare namespace xf = "http://tempuri.org/BCO_PE_REF_Credito_RefinanciadoObtener-v1_0_IMPL/Resources/XQuery/xq_OUT_Credito_RefinanciadoObtenerREF_to_Credito_RefinanciadoObtenerImpl/";

declare function xf:xq_OUT_Credito_RefinanciadoObtenerREF_to_Credito_RefinanciadoObtenerImpl($obtenerCreditosRefinanciadosResponse1 as element(ns0:ObtenerCreditosRefinanciadosResponse))
    as element(ns1:creditoRefinanciadoObtenerRespParam) {
        <ns1:creditoRefinanciadoObtenerRespParam>
            <ns1:estadoOperacion>
                {
                if(fn:empty($obtenerCreditosRefinanciadosResponse1/ns0:ObtenerCreditosRefinanciadosResult/ns0:LCreditos/*[1]) 
                )then
                	<ns1:codigoOperacion>01</ns1:codigoOperacion>
                else
                	<ns1:codigoOperacion>00</ns1:codigoOperacion>
                }
                {
                if(fn:empty($obtenerCreditosRefinanciadosResponse1/ns0:ObtenerCreditosRefinanciadosResult/ns0:LCreditos/*[1]) 
                )then
                	<ns1:glosaOperacion>No se encontraron registros</ns1:glosaOperacion>
                else
                	<ns1:glosaOperacion>Exito</ns1:glosaOperacion>
                }
            </ns1:estadoOperacion>
            {
                for $CreditoRef in $obtenerCreditosRefinanciadosResponse1/ns0:ObtenerCreditosRefinanciadosResult/ns0:LCreditos/ns0:CreditoRef
                return
                    <ns1:creditoRefinanciado>
                        <ns1:codigoProducto>{ data($CreditoRef/ns0:CodProducto) }</ns1:codigoProducto>
                        <ns1:identificadorProducto>{ data($CreditoRef/ns0:NumCuenta) }</ns1:identificadorProducto>
                        <ns1:saldoTotal>{ data($CreditoRef/ns0:SaldoTotal) }</ns1:saldoTotal>
                        <ns1:montoRef>{ data($CreditoRef/ns0:MontoRef) }</ns1:montoRef>
                        <ns1:cuotasPactadas>{ data($CreditoRef/ns0:CuotasPactadas) }</ns1:cuotasPactadas>
                        <ns1:cuotasPagadas>{ data($CreditoRef/ns0:CuotasPagadas) }</ns1:cuotasPagadas>
                        <ns1:coutasAtrasadas>{ data($CreditoRef/ns0:CuotasAtrasadas) }</ns1:coutasAtrasadas>
                        <ns1:montoProximaCuota>{ data($CreditoRef/ns0:MontoCuota) }</ns1:montoProximaCuota>
                        <ns1:fechaUltimoPago>{ data($CreditoRef/ns0:FecUltPago) }</ns1:fechaUltimoPago>
                        <ns1:fechaRef>{ data($CreditoRef/ns0:FecRef) }</ns1:fechaRef>
                    </ns1:creditoRefinanciado>
            }
        </ns1:creditoRefinanciadoObtenerRespParam>
};

declare variable $obtenerCreditosRefinanciadosResponse1 as element(ns0:ObtenerCreditosRefinanciadosResponse) external;

xf:xq_OUT_Credito_RefinanciadoObtenerREF_to_Credito_RefinanciadoObtenerImpl($obtenerCreditosRefinanciadosResponse1)