xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$creditoRefinanciadoObtenerReqParam1" element="ns1:creditoRefinanciadoObtenerReqParam" location="../Schemas/OSB_REF_Credito_Refinanciado_ObtenerReq.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ObtenerCreditosRefinanciados" location="../WSDL/Refinanciados.asmx.wsdl" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ref/credito/refinanciado/obtener/Req-v2016.02";
declare namespace ns0 = "http://falabella.refinanciados.servicio.pe";
declare namespace xf = "http://tempuri.org/BCO_PE_REF_Credito_RefinanciadoObtener-v1_0_IMPL/Resources/XQuery/xq_IN_Credito_RefinanciadoObtenerImpl_to_Credito_RefinanciadoObtenerREF/";

declare function xf:xq_IN_Credito_RefinanciadoObtenerImpl_to_Credito_RefinanciadoObtenerREF($creditoRefinanciadoObtenerReqParam1 as element(ns1:creditoRefinanciadoObtenerReqParam))
    as element(ns0:ObtenerCreditosRefinanciados) {
        <ns0:ObtenerCreditosRefinanciados>
            <ns0:TipDoc>{ data($creditoRefinanciadoObtenerReqParam1/ns1:tipoDocumento) }</ns0:TipDoc>
            <ns0:NumDoc>{data($creditoRefinanciadoObtenerReqParam1/ns1:numeroDocumento)}</ns0:NumDoc>
        </ns0:ObtenerCreditosRefinanciados>
};

declare variable $creditoRefinanciadoObtenerReqParam1 as element(ns1:creditoRefinanciadoObtenerReqParam) external;

xf:xq_IN_Credito_RefinanciadoObtenerImpl_to_Credito_RefinanciadoObtenerREF($creditoRefinanciadoObtenerReqParam1)