xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$clienteResumenproductosObtenerReqParam1" element="ns0:ClienteResumenproductosObtenerReqParam" location="../Schemas/OSB_Cliente_Resumenproductos_ObtenerReq.xsd" ::)
(:: pragma bea:global-element-return element="ns1:ClienteResumenproductosObtenerReqParam" location="../Schemas/WLS_PIF_Cliente_Resumenproductos_ObtenerReq.xsd" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/WLS/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Req-v2015.12";
declare namespace ns0 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Req-v2015.12";
declare namespace xf = "http://tempuri.org/BCO_PE_PIF_Cliente_ResumenproductosObtener-v1_0_IMPL/Resources/XQuery/xq_IN_PX_PE_Cliente_ResumenproductosObtenerImpl_to_BS_PE_PIF_ClienteResumenproductosObtener/";

declare function xf:xq_IN_PX_PE_Cliente_ResumenproductosObtenerImpl_to_BS_PE_PIF_ClienteResumenproductosObtener($clienteResumenproductosObtenerReqParam1 as element(ns0:ClienteResumenproductosObtenerReqParam))
    as element(ns1:ClienteResumenproductosObtenerReqParam) {
        <ns1:ClienteResumenproductosObtenerReqParam>
            <ns1:documentoIdentidad>
                <ns1:tipoDocumento>{ data($clienteResumenproductosObtenerReqParam1/ns0:documentoIdentidad/ns0:tipoDocumento) }</ns1:tipoDocumento>
                <ns1:numeroDocumento>{ data($clienteResumenproductosObtenerReqParam1/ns0:documentoIdentidad/ns0:numeroDocumento) }</ns1:numeroDocumento>
            </ns1:documentoIdentidad>
        </ns1:ClienteResumenproductosObtenerReqParam>
};

declare variable $clienteResumenproductosObtenerReqParam1 as element(ns0:ClienteResumenproductosObtenerReqParam) external;

xf:xq_IN_PX_PE_Cliente_ResumenproductosObtenerImpl_to_BS_PE_PIF_ClienteResumenproductosObtener($clienteResumenproductosObtenerReqParam1)