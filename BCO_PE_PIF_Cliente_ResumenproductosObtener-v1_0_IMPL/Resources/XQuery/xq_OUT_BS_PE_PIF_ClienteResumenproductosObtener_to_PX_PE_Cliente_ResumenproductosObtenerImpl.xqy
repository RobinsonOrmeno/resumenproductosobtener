xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$clienteResumenproductosObtenerRespParam1" element="ns1:ClienteResumenproductosObtenerRespParam" location="../Schemas/WLS_PIF_Cliente_Resumenproductos_ObtenerResp.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ClienteResumenproductosObtenerRespParam" location="../Schemas/OSB_Cliente_Resumenproductos_ObtenerResp.xsd" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/WLS/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Resp-v2015.12";
declare namespace ns0 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Resp-v2015.12";
declare namespace xf = "http://tempuri.org/BCO_PE_PIF_Cliente_ResumenproductosObtener-v1_0_IMPL/Resources/XQuery/xq_OUT_BS_PE_PIF_ClienteResumenproductosObtener_to_PX_PE_Cliente_ResumenproductosObtenerImpl/";

declare function xf:xq_OUT_BS_PE_PIF_ClienteResumenproductosObtener_to_PX_PE_Cliente_ResumenproductosObtenerImpl($clienteResumenproductosObtenerRespParam1 as element(ns1:ClienteResumenproductosObtenerRespParam))
    as element(ns0:ClienteResumenproductosObtenerRespParam) {
        <ns0:ClienteResumenproductosObtenerRespParam>
            {
                let $estadoOperacion := $clienteResumenproductosObtenerRespParam1/ns1:estadoOperacion
                return
                    <ns0:estadoOperacion>
                        <ns0:codigoOperacion>{ data($estadoOperacion/ns1:codigoOperacion) }</ns0:codigoOperacion>
                        <ns0:glosaOperacion>{ data($estadoOperacion/ns1:glosaOperacion) }</ns0:glosaOperacion>
                    </ns0:estadoOperacion>
            }
            {
                for $credito in $clienteResumenproductosObtenerRespParam1/ns1:credito
                return
                    <ns0:credito>
                        {
                            for $creditoConsumo in $credito/ns1:creditoConsumo
                            return
                                <ns0:creditoConsumo>
                                    <ns0:codigoProducto>{ data($creditoConsumo/ns1:codigoProducto) }</ns0:codigoProducto>
                                    <ns0:codigoSubProducto>{ data($creditoConsumo/ns1:codigoSubProducto) }</ns0:codigoSubProducto>
                                    <ns0:identificadorProducto>{ data($creditoConsumo/ns1:identificadorProducto) }</ns0:identificadorProducto>
                                    <ns0:moneda>
                                        <ns0:codigoMoneda>{ data($creditoConsumo/ns1:moneda/ns1:codigoMoneda) }</ns0:codigoMoneda>
                                    </ns0:moneda>
                                    <ns0:situacion>
                                        <ns0:estado>{ data($creditoConsumo/ns1:situacion/ns1:estado) }</ns0:estado>
                                        <ns0:glosa>{ data($creditoConsumo/ns1:situacion/ns1:glosa) }</ns0:glosa>
                                    </ns0:situacion>
                                    <ns0:montoCredito>
                                        <ns0:montoSolicitado>{ data($creditoConsumo/ns1:montoCredito/ns1:montoSolicitado) }</ns0:montoSolicitado>
                                    </ns0:montoCredito>
                                    <ns0:cuota>
                                        <ns0:totalCuota>{ data($creditoConsumo/ns1:cuota/ns1:totalCuota) }</ns0:totalCuota>
                                        <ns0:numeroCuotasPagadas>{ data($creditoConsumo/ns1:cuota/ns1:numeroCuotasPagadas) }</ns0:numeroCuotasPagadas>
                                        <ns0:fechaVencimientoProximaCuota>{ data($creditoConsumo/ns1:cuota/ns1:fechaVencimientoProximaCuota) }</ns0:fechaVencimientoProximaCuota>
                                        <ns0:montoProximaCuota>{ data($creditoConsumo/ns1:cuota/ns1:montoProximaCuota) }</ns0:montoProximaCuota>
                                        <ns0:numeroProximaCuota>{ data($creditoConsumo/ns1:cuota/ns1:numeroProximaCuota) }</ns0:numeroProximaCuota>
                                    </ns0:cuota>
                                </ns0:creditoConsumo>
                        }
                    </ns0:credito>
            }
            {
                for $cuentaAhorro in $clienteResumenproductosObtenerRespParam1/ns1:cuentaAhorro
                return
                    <ns0:cuentaAhorro>
                        <ns0:codigoProducto>{ data($cuentaAhorro/ns1:codigoProducto) }</ns0:codigoProducto>
                        <ns0:codigoSubProducto>{ data($cuentaAhorro/ns1:codigoSubProducto) }</ns0:codigoSubProducto>
                        <ns0:identificadorProducto>{ data($cuentaAhorro/ns1:identificadorProducto) }</ns0:identificadorProducto>
                        {
                            for $CCI in $cuentaAhorro/ns1:CCI
                            return
                                <ns0:CCI>{ data($CCI) }</ns0:CCI>
                        }
                        <ns0:situacion>
                            <ns0:codigo>{ data($cuentaAhorro/ns1:situacion/ns1:codigo) }</ns0:codigo>
                            <ns0:estado>{ data($cuentaAhorro/ns1:situacion/ns1:estado) }</ns0:estado>
                            <ns0:glosa>{ data($cuentaAhorro/ns1:situacion/ns1:glosa) }</ns0:glosa>
                        </ns0:situacion>
                        <ns0:saldo>
                            <ns0:saldoDisponible>{ data($cuentaAhorro/ns1:saldo/ns1:saldoDisponible) }</ns0:saldoDisponible>
                            <ns0:saldoContable>{ data($cuentaAhorro/ns1:saldo/ns1:saldoContable) }</ns0:saldoContable>
                        </ns0:saldo>
                        <ns0:moneda>
                            <ns0:codigoMoneda>{ data($cuentaAhorro/ns1:moneda/ns1:codigoMoneda) }</ns0:codigoMoneda>
                        </ns0:moneda>
                    </ns0:cuentaAhorro>
            }
            {
                for $inversion in $clienteResumenproductosObtenerRespParam1/ns1:inversion
                return
                    <ns0:inversion>
                        {
                            for $depositoPlazo in $inversion/ns1:depositoPlazo
                            return
                                <ns0:depositoPlazo>
                                    <ns0:codigoProducto>{ data($depositoPlazo/ns1:codigoProducto) }</ns0:codigoProducto>
                                    <ns0:codigoSubProducto>{ data($depositoPlazo/ns1:codigoSubProducto) }</ns0:codigoSubProducto>
                                    <ns0:identificadorProducto>{ data($depositoPlazo/ns1:identificadorProducto) }</ns0:identificadorProducto>
                                    {
                                        let $moneda := $depositoPlazo/ns1:moneda
                                        return
                                            <ns0:moneda>
                                                <ns0:codigoMoneda>{ data($moneda/ns1:codigoMoneda) }</ns0:codigoMoneda>
                                            </ns0:moneda>
                                    }
                                    {
                                        let $situacion := $depositoPlazo/ns1:situacion
                                        return
                                            <ns0:situacion>
                                                <ns0:estado>{ data($situacion/ns1:estado) }</ns0:estado>
                                                <ns0:glosa>{ data($situacion/ns1:glosa) }</ns0:glosa>
                                            </ns0:situacion>
                                    }
                                    <ns0:fechaInversion>{ data($depositoPlazo/ns1:fechaInversion) }</ns0:fechaInversion>
                                    <ns0:montoInvertido>{ data($depositoPlazo/ns1:montoInvertido) }</ns0:montoInvertido>
                                    <ns0:fechaVencimiento>{ data($depositoPlazo/ns1:fechaVencimiento) }</ns0:fechaVencimiento>
                                    <ns0:valorRecate>{ data($depositoPlazo/ns1:valorRescate) }</ns0:valorRecate>
                                    <ns0:puedeCobrar>{ data($depositoPlazo/ns1:puedeCobrar) }</ns0:puedeCobrar>
                                    <ns0:esRenovacion>{ data($depositoPlazo/ns1:esRenovacion) }</ns0:esRenovacion>
                                </ns0:depositoPlazo>
                        }
                    </ns0:inversion>
            }
        </ns0:ClienteResumenproductosObtenerRespParam>
};

declare variable $clienteResumenproductosObtenerRespParam1 as element(ns1:ClienteResumenproductosObtenerRespParam) external;

xf:xq_OUT_BS_PE_PIF_ClienteResumenproductosObtener_to_PX_PE_Cliente_ResumenproductosObtenerImpl($clienteResumenproductosObtenerRespParam1)