xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;
declare variable $clienteResumenProductosObtenerCompResp1 as element(ns2:clienteResumenProductosObtenerCompResp) external;

declare function local:xq_OUT_PX_SAT_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:clienteResumenProductosObtenerCompResp) ::) {
    <ns2:clienteResumenProductosObtenerCompResp>
     <estadoOperacion>
                <codigoOperacion>
				{
                    if (fn:empty($clienteResumenProductosObtenerCompResp1/tarjetaCredito[1]) and 
                    data($clienteResumenProductosObtenerCompResp1/estadoOperacion/codigoOperacion) = "01"
                    ) then
                        ("01")
                    else
                        ("00")
                }
				</codigoOperacion>
                <glosaOperacion>
                {
                    if (fn:empty($clienteResumenProductosObtenerCompResp1/tarjetaCredito[1]) and 
                    data($clienteResumenProductosObtenerCompResp1/estadoOperacion/codigoOperacion) = "01"
                    ) then
                        ("No se encontraron resultados")
                    else 
                        ("Exito Exito -2")
                }
				</glosaOperacion>
            </estadoOperacion>            
           
            {
                for $tarjetaCredito in $clienteResumenProductosObtenerCompResp1/tarjetaCredito
                return
                	(: Valida lista :)
                	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
	                	if(data($tarjetaCredito/situacion/estado) = "-1") then
	                	()
	                	else 
	                		<tarjetaCredito>{ $tarjetaCredito/@* , $tarjetaCredito/node() }</tarjetaCredito>
	            	else
	            		for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
		            	return
		            		(: Compara estado del Request y Response :)
		            		if(data($situacionReq/estado) = data($tarjetaCredito/situacion/estado)) then
		            		<tarjetaCredito>{ $tarjetaCredito/@* , $tarjetaCredito/node() }</tarjetaCredito>
		            		else ()
            }
            {
                for $inversion in $clienteResumenProductosObtenerCompResp1/inversion
                return
                    <inversion>
                        {
                            for $depositoPlazo in $inversion/depositoPlazo
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
                                	if (data($depositoPlazo/situacion/estado) = "-1") then 
                                	()
                                	else 
                                		<depositoPlazo>{ $depositoPlazo/@* , $depositoPlazo/node() }</depositoPlazo>
                                else 
				                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
					            	return
					            		if(data($situacionReq/estado) = data($depositoPlazo/situacion/estado)) then
					                    <depositoPlazo>{ $depositoPlazo/@* , $depositoPlazo/node() }</depositoPlazo>
					                    else ()
                        }
                        {
                            for $fondosMutuos in $inversion/fondosMutuos
                            return
                                <fondosMutuos>{ $fondosMutuos/@* , $fondosMutuos/node() }</fondosMutuos>
                        }
                    </inversion>
            }
            {
                for $cuenta in $clienteResumenProductosObtenerCompResp1/cuenta
                return
                    <cuenta>
                        {
                            for $cuentaCorriente in $cuenta/cuentaCorriente
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
                                	if (data($cuentaCorriente/situacion/estado) = "-1") then
                                		()
                                	else
                                		<cuentaCorriente>{ $cuentaCorriente/@* , $cuentaCorriente/node() }</cuentaCorriente> 
                                else
				                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
					                return
					                	if(data($situacionReq/estado) = data($cuentaCorriente/situacion/estado)) then
					                    <cuentaCorriente>{ $cuentaCorriente/@* , $cuentaCorriente/node() }</cuentaCorriente>
					                    else ()
                        }
                        {
                            for $cuentaAhorro in $cuenta/cuentaAhorro
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
                                	if (data($cuentaAhorro/situacion/estado) = "-1") then
                                	()
                                	else
                                		<cuentaAhorro>{ $cuentaAhorro/@* , $cuentaAhorro/node() }</cuentaAhorro>
                                else
				                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
					                return
					                	if(data($situacionReq/estado) = data($cuentaAhorro/situacion/estado)) then
					                    <cuentaAhorro>{ $cuentaAhorro/@* , $cuentaAhorro/node() }</cuentaAhorro>
					                    else ()
                        }
                        {
                            for $cuentaVista in $cuenta/cuentaVista
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
                                		if (data($cuentaVista/situacion/estado) = "-1") then
                                		()
                                		else
                                			<cuentaVista>{ $cuentaVista/@* , $cuentaVista/node() }</cuentaVista>
                                	else
					                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
						                return
						                	if(data($situacionReq/estado) = data($cuentaVista/situacion/estado)) then
						                    <cuentaVista>{ $cuentaVista/@* , $cuentaVista/node() }</cuentaVista>
						                    else ()
                        }
                    </cuenta>
            }
            {
                for $credito in $clienteResumenProductosObtenerCompResp1/credito
                return
                    <credito>
                        {
                            for $creditoConsumo in $credito/creditoConsumo
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
                                	if (data($creditoConsumo/situacion/estado) = "-1") then
                                	()
                                	else
                                		<creditoConsumo>{ $creditoConsumo/@* , $creditoConsumo/node() }</creditoConsumo>
                                else
				                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
					                return
					                	if(data($situacionReq/estado) = data($creditoConsumo/situacion/estado)) then
					                    <creditoConsumo>{ $creditoConsumo/@* , $creditoConsumo/node() }</creditoConsumo>
					                    else ()
                        }
                        {
                            for $creditoHipotecario in $credito/creditoHipotecario
                            return
                            	if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
	                                if (data($creditoHipotecario/situacion/estado) = "-1") then
	                                ()
	                                else
	                                	<creditoHipotecario>{ $creditoHipotecario/@* , $creditoHipotecario/node() }</creditoHipotecario>
                                else
				                	for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
					                return
					                	if(data($situacionReq/estado) = data($creditoHipotecario/situacion/estado)) then
					                    <creditoHipotecario>{ $creditoHipotecario/@* , $creditoHipotecario/node() }</creditoHipotecario>
					                    else ()
                        }
                    </credito>
            }
            {
                for $lineaCredito in $clienteResumenProductosObtenerCompResp1/lineaCredito
                return	
                   if(fn:empty($clienteResumenProductosObtenerCompReq1/situacion)) then
	                	if (data($lineaCredito/situacion/estado) = "-1") then
	                	()
	                	else
	                		<lineaCredito>{ $lineaCredito/@* , $lineaCredito/node() }</lineaCredito>
	            	else
	            		for $situacionReq in $clienteResumenProductosObtenerCompReq1/situacion
		            	return
		            		if(data($situacionReq/estado) = data($lineaCredito/situacion/estado)) then
		            		<lineaCredito>{ $lineaCredito/@* , $lineaCredito/node() }</lineaCredito>
		            		else ()
            }
    </ns2:clienteResumenProductosObtenerCompResp>
};

local:xq_OUT_PX_SAT_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($clienteResumenProductosObtenerCompReq1)
