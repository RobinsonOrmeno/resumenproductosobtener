xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLCCT/ConsultaClienteContratos/Resp/IMPL/v2017.10";
(:: import schema at "../../../FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_IMPL_Resp.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCLCCC/ConsultaCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCOCUO/DetalleCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns4="http://www.example.org/OSB_Cliente_ListaCreditoEfectivo";
(:: import schema at "../Schemas/OSB_Cliente_ListaCreditoEfectivo.xsd" ::)

declare variable $runServiceResponseCUO as element() (:: schema-element(ns1:runServiceResponse) ::) external;
declare variable $runServiceResponseCCC as element() (:: schema-element(ns2:runServiceResponse) ::) external;
declare variable $runServiceResponseCCT as element() (:: schema-element(ns3:runServiceResponse) ::) external;
declare variable $satTarjetaCreditoFLCLCCTContratosObtenerImplResp1 as element(ns3:satTarjetaCreditoFLCLCCTContratosObtenerImplResp) external;
declare variable $satFlclcccwsCompraCuotasResumenObtenerImplResp1 as element(ns2:satFlclcccwsCompraCuotasResumenObtenerImplResp) external;
declare variable $satOpclcuowsCompraCuotasDetalleObtenerImplResp1 as element(ns1:satOpclcuowsCompraCuotasDetalleObtenerImplResp) external;
declare variable $numfinan as xs:string external;
declare variable $numopecuo as xs:string external;
declare variable $contrato as xs:string external;

declare function local:xq_OUT_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($runServiceResponseCUO as element() (:: schema-element(ns1:runServiceResponse) ::), 
                                                                                                                          $runServiceResponseCCC as element() (:: schema-element(ns2:runServiceResponse) ::), 
                                                                                                                          $runServiceResponseCCT as element() (:: schema-element(ns3:runServiceResponse) ::)) 
                                                                                                                          as element() (:: schema-element(ns4:listaCE) ::) {
    <ns4:listaCE>
     {
                (: XQuery Contiene la logica para mostrar la cuota correlativa mas cercano con estado estcuo=01 :)
                for $registros in $satFlclcccwsCompraCuotasResumenObtenerImplResp1/registros
                return
                    for $registrosCUO in $satOpclcuowsCompraCuotasDetalleObtenerImplResp1/registros
                    return
	                    if(data($registros/numopecuo) = data($numopecuo) and 
	                    	data($registros/numfinan) = data($numfinan) and 
	                    	data($registrosCUO/numcuota) =  local:obtenerNumeroPrimeraCuotaPendiente($satOpclcuowsCompraCuotasDetalleObtenerImplResp1) 
                        	) then
                    	<credito>
                    	{
		                     for $centalta in $registros/centalta
		                    return
		                        <centalta>{ data($centalta) }</centalta>
		                }
		                {
		                     for $codent in $registros/codent
		                    return
		                        <codent>{ data($codent) }</codent>
		                }
		                {
		                       for $cuenta in $registros/cuenta
		                    return
		                        <cuenta>{ data($cuenta) }</cuenta>
		                }
                        {
                            for $numopecuo in $registros/numopecuo
                            return
                                <numopecuo>{ data($numopecuo) }</numopecuo>
                        }
                        {
                            for $numfinan in $registros/numfinan
                            return
                                <numfinan>{ data($numfinan) }</numfinan>
                        }
                        {
                            for $clamon in $registros/clamon
                            return
                                <clamon>{ xs:string(data($clamon)) }</clamon>
                        }
                        {
                            for $estcompra in $registros/estcompra
                            return
                                <estcompra>{ xs:string(data($estcompra)) }</estcompra>
                        }
                        {
                            for $impope in $registros/impope
                            return
                                <impope>{ data($impope) }</impope>
                        }
                        {
                            for $numcuot in $registros/numcuot
                            return
                                <numcuot>{ data($numcuot) }</numcuot>
                        }
                        {
                            for $numcuotpen in $registros/numcuotpen
                            return
                                <numcuotpen>{ data($numcuotpen) }</numcuotpen>
                        }
                        {
                                (: La cuota correlativa mas cercana con el estado pendiente 
                            	Se obtiene la cuota pagada y se le suma 1, se compara con las cuotas del cronograma :)
                                    for $valueDate in $registrosCUO/fecprocuo/valueDate
                                    return
                                        <fecprocuo>{ data($valueDate) }</fecprocuo>
                        }
                        {
                                (: La cuota correlativa mas cercana con el estado pendiente :)
                                    for $impcuotapp in $registrosCUO/impcuotapp
                                    return
                                        <impcuotapp>{ xs:double(xs:string(data($impcuotapp))) }</impcuotapp>
                        }
                        {
                                (: La cuota correlativa mas cercana con el estado pendiente :)
                                    for $estcuo in $registrosCUO/estcuo
                                    return
                                        <estcuo>{ data($estcuo) }</estcuo>
                                        
                        }
                         <contrato>{ data($contrato) }</contrato>
                        
                    </credito>
	                	else ()
            }
    </ns4:listaCE>
};

declare function local:obtenerNumeroPrimeraCuotaPendiente($resp_OPCLCUOWS as element(ns1:satOpclcuowsCompraCuotasDetalleObtenerImplResp)) as xs:int {
 	min(
    	for $cuotas in $resp_OPCLCUOWS/registros 
    	where $cuotas/estcuo='01' 
    	return 
    	xs:int($cuotas/numcuota)
    	)
 };



local:xq_OUT_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($runServiceResponseCUO, $runServiceResponseCCC, $runServiceResponseCCT)
