xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Resp-v2015.12";
(:: import schema at "../../../BCO_PE_ISAAC_Cliente_ResumencreditoObtener-v1_0_IMPL/Resources/Schema/OSB_Cliente_Resumencredito_ObtenerResp.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Resp-v2015.12";
(:: import schema at "../../../BCO_PE_PIF_Cliente_ResumenproductosObtener-v1_0_IMPL/Resources/Schemas/OSB_Cliente_Resumenproductos_ObtenerResp.xsd" ::)
declare namespace ns3="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ref/credito/refinanciado/obtener/Resp-v2016.02";
(:: import schema at "../../../BCO_PE_REF_Credito_RefinanciadoObtener-v1_0_IMPL/Resources/Schemas/OSB_REF_Credito_Refinanciado_ObtenerResp.xsd" ::)
declare namespace ns4="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ven/credito/vencido/obtener/Resp-v2016.02";
(:: import schema at "../../../BCO_PE_VEN_Credito_VencidoObtener-v1_0_IMPL/Resources/Schemas/OSB_VEN_Credito_Vencido_ObtenerResp.xsd" ::)
declare namespace ns5="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)

declare variable $ISAAC_ResumenCreditoObtenerImplRes as element() (:: schema-element(ns1:ISAAC_ResumenCreditoObtenerImplRes) ::) external;
declare variable $ClienteResumenproductosObtenerRespParam as element() (:: schema-element(ns2:ClienteResumenproductosObtenerRespParam) ::) external;
declare variable $creditoRefinanciadoObtenerRespParam as element() (:: schema-element(ns3:creditoRefinanciadoObtenerRespParam) ::) external;
declare variable $creditoVencidoObtenerRespParam as element() (:: schema-element(ns4:creditoVencidoObtenerRespParam) ::) external;


declare variable $iSAAC_ResumenCreditoObtenerImplRes1 as element(ns1:ISAAC_ResumenCreditoObtenerImplRes) external;
declare variable $clienteResumenproductosObtenerRespParam1 as element(ns2:ClienteResumenproductosObtenerRespParam) external;
declare variable $creditoRefinanciadoObtenerRespParam1 as element(ns3:creditoRefinanciadoObtenerRespParam) external;
declare variable $creditoVencidoObtenerRespParam1 as element(ns1:creditoVencidoObtenerRespParam) external;
declare variable $clienteResumenProductosObtenerCompResp1 as element(ns4:clienteResumenProductosObtenerCompResp) external;

declare function local:xq_OUT_PX_CORP_Cliente_ResumenProductosObtenerImpl_to_PX_PE_Cliente_ResumenproductosObtenerComp($ISAAC_ResumenCreditoObtenerImplRes as element() (:: schema-element(ns1:ISAAC_ResumenCreditoObtenerImplRes) ::), 
                                                                                                                       $ClienteResumenproductosObtenerRespParam as element() (:: schema-element(ns2:ClienteResumenproductosObtenerRespParam) ::), 
                                                                                                                       $creditoRefinanciadoObtenerRespParam as element() (:: schema-element(ns3:creditoRefinanciadoObtenerRespParam) ::), 
                                                                                                              $creditoVencidoObtenerRespParam as element() (:: schema-element(ns4:creditoVencidoObtenerRespParam) ::)) 
                                                                                                                       as element() (:: schema-element(ns5:clienteResumenProductosObtenerCompResp) ::) {
    <ns5:clienteResumenProductosObtenerCompResp>
      <estadoOperacion>
                <codigoOperacion>
                    {
                        if (((fn:empty($clienteResumenproductosObtenerRespParam1/ns2:inversion/*[1]) and fn:empty($clienteResumenproductosObtenerRespParam1/ns2:cuentaAhorro/*[1])) and fn:count($iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito) <= 0)) then
                            ("01")
                        else 
                            "00"
                    }
</codigoOperacion>
                <glosaOperacion>
                    {
                        if (((fn:empty($clienteResumenproductosObtenerRespParam1/ns2:inversion/*[1]) and fn:empty($clienteResumenproductosObtenerRespParam1/ns2:cuentaAhorro/*[1])) and fn:count($iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito) <= 0)) then
                            ("No se encontraron resultados")
                        else 
                            "Exito -3"
                    }
</glosaOperacion>
            </estadoOperacion>
			{
                for $tarjetaCredito in $clienteResumenProductosObtenerCompResp1/tarjetaCredito
                return
                    <tarjetaCredito>{ $tarjetaCredito/@* , $tarjetaCredito/node() }</tarjetaCredito>
            }
            {
            	if( fn:empty($creditoRefinanciadoObtenerRespParam1/ns3:creditoRefinanciado)) then
            		()
            	else
                for $creditoRefinanciado in $creditoRefinanciadoObtenerRespParam1/ns3:creditoRefinanciado
                return
                	if ( data($creditoRefinanciado/ns3:codigoProducto) = '82'
                	) then
                		
                	 (: 82 Tarjeta de Credito :)
                	<tarjetaCredito>
                        <tarjetaCredito>
                            <codigoProducto>500</codigoProducto>
                            <codigoSubProducto>{ concat('500-0-',$creditoRefinanciado/ns3:codigoProducto) }</codigoSubProducto>
                            <identificadorProducto>{ data($creditoRefinanciado/ns3:identificadorProducto) }</identificadorProducto>
                        </tarjetaCredito>
                        <situacion>
                            <estado>01</estado>(: Informativo :)
                        </situacion>
                        <moneda>
                            <codigoMoneda>604</codigoMoneda>
                        </moneda>
                        <plastico>
                        	<titularidad>0</titularidad>
                        	<situacion>
                        		<estado>0</estado>
                        	</situacion>
                        </plastico>
                        <cuentaTarjetaCredito>
                        	<identificador>0</identificador>
                        </cuentaTarjetaCredito>
                        <cupoNacional>
                        	<cupoDisponible>0</cupoDisponible>
                        	<cupoTotal>0</cupoTotal>
                            <cupoUtilizado>{ data($creditoRefinanciado/ns3:saldoTotal) }</cupoUtilizado>
                        </cupoNacional>
                        <cupoInternacional>
                        	<cupoDisponible>0</cupoDisponible>
                        	<cupoTotal>0</cupoTotal>
                        </cupoInternacional>
                        <avance>
                        	<cupoDisponible>0</cupoDisponible>
                    	</avance>
                    	
                    </tarjetaCredito>
				else ()
            }
            {
                for $creditoVencido in $creditoVencidoObtenerRespParam1/ns1:creditoVencido
                return
                	if ( data($creditoVencido/ns1:codigoProducto) = '07'  or 
	                    		data($creditoVencido/ns1:codigoProducto) = '08') then
                    <tarjetaCredito>
                        <tarjetaCredito>
                            <codigoProducto>600</codigoProducto>
                            <codigoSubProducto>{ concat('600-0-',$creditoVencido/ns1:codigoProducto) }</codigoSubProducto>
                            <identificadorProducto>{ data($creditoVencido/ns1:identificadorProducto) }</identificadorProducto>
                        </tarjetaCredito>
                        <situacion>
                            <estado>01</estado>(: Informativo :)
                        </situacion>
                        <moneda>
                            <codigoMoneda>
                            { 
	                        	if (data($creditoVencido/ns1:moneda) = 'Soles') then
	                        	'01'
	                        	else if (data($creditoVencido/ns1:moneda) = 'Dolares') then
	                        	'02'
	                        	else
	                        	'01'
	                        }
	                        </codigoMoneda>
                        </moneda>
                        <plastico>
                        	<titularidad>0</titularidad>
                        	<situacion>
                        		<estado>0</estado>
                        	</situacion>
                        </plastico>
                        <cuentaTarjetaCredito>
                        	<identificador>0</identificador>
                        </cuentaTarjetaCredito>
                        <cupoNacional>
                        	<cupoDisponible>0</cupoDisponible>
                        	<cupoTotal>0</cupoTotal>
                            <cupoUtilizado>{ data($creditoVencido/ns1:saldoTotal) }</cupoUtilizado>
                        </cupoNacional>
                        <cupoInternacional>
                        	<cupoDisponible>0</cupoDisponible>
                        	<cupoTotal>0</cupoTotal>
                        </cupoInternacional>
                        <avance>
                        	<cupoDisponible>0</cupoDisponible>
                    	</avance>
                    </tarjetaCredito>
                    else ()
            }
            (:  :)
            {
            if (fn:empty($clienteResumenproductosObtenerRespParam1/*[1]))then
            ()
            else(
            <inversion>
                {
                    for $depositoPlazo in $clienteResumenproductosObtenerRespParam1/ns2:inversion/ns2:depositoPlazo
                    return
                        <depositoPlazo>
                            <depositoPlazo>
                                <codigoProducto>{ data($depositoPlazo/ns2:codigoProducto) }</codigoProducto>
                                <codigoSubProducto>{ data($depositoPlazo/ns2:codigoSubProducto) }</codigoSubProducto>
                                <identificadorProducto>{ data($depositoPlazo/ns2:identificadorProducto) }</identificadorProducto>
                            </depositoPlazo>
                            <situacion>
                                <estado>
                                { 
                                	(: Conversion de estados :)
                                	if (data($depositoPlazo/ns2:situacion/ns2:estado) = "20" or
	                                	data($depositoPlazo/ns2:situacion/ns2:estado) = "41"
                                	) then
                                	"00"
                                	else if (data($depositoPlazo/ns2:situacion/ns2:estado) = "10" ) then 
                                	"01"
                                	else 
                                	("-1")
                                }
                                </estado>
                                <glosa>
                                { 
                                	if (data($depositoPlazo/ns2:situacion/ns2:estado) = "20" or
	                                	data($depositoPlazo/ns2:situacion/ns2:estado) = "41"
	                                ) then
                                	"Activo"
                                	else if (data($depositoPlazo/ns2:situacion/ns2:estado) = "10" ) then
                                	"Informativo"
                                	else ()
                                	
                                }</glosa>
                            </situacion>
                            <moneda>
                                <codigoMoneda>{ data($depositoPlazo/ns2:moneda/ns2:codigoMoneda) }</codigoMoneda>
                            </moneda>                            
                            {
                            if (fn:empty($depositoPlazo/ns2:fechaInversion/text()))then
                            ()
                            else(
                            <fechaInversion>{                             
                            fn:concat(
                            	fn:substring(xs:string(data($depositoPlazo/ns2:fechaInversion)),1,4),'-',
                            	fn:substring(xs:string(data($depositoPlazo/ns2:fechaInversion)),6,2),'-',
                                fn:substring(xs:string(data($depositoPlazo/ns2:fechaInversion)),9,2)
                                                                
                                )
                                }
                            </fechaInversion>
                            )
                            }
                            {
                            	for $montoInvertido in $depositoPlazo/ns2:montoInvertido
                            	return
                            		<montoInvertido>{ data($montoInvertido) }</montoInvertido>
                            }
                            {
	                            if (fn:empty($depositoPlazo/ns2:fechaVencimiento/text()))then
	                            ()
	                            else(
	                            <fechaVencimiento>
	                            { 
	                            fn:concat(
	                            	fn:substring(xs:string($depositoPlazo/ns2:fechaVencimiento),1,4),'-',
	                            	fn:substring(xs:string($depositoPlazo/ns2:fechaVencimiento),6,2),'-',
	                                fn:substring(xs:string($depositoPlazo/ns2:fechaVencimiento),9,2)
	                                )
	                            }
	                            </fechaVencimiento>
                            )
                            }
                            {
	                            if (fn:empty($depositoPlazo/ns2:valorRecate/text()))then
	                            ()else(
	                            <valorRescate>{ data($depositoPlazo/ns2:valorRecate) }</valorRescate>
	                            )
                            }
                            <puedeCobrar>{ data($depositoPlazo/ns2:puedeCobrar) }</puedeCobrar>
                            <esRenovacion>{ data($depositoPlazo/ns2:esRenovacion) }</esRenovacion>
                        </depositoPlazo>
	                }
	            </inversion>
	           )
           }
           { (: PIF Filtro :)
	           if (fn:empty($clienteResumenproductosObtenerRespParam1/*[1]))then
	           ()
	           else(
		           <cuenta>
		           {
	            	for $cuentaAhorro in $clienteResumenproductosObtenerRespParam1/ns2:cuentaAhorro
	                return
	                <cuentaAhorro>
	                    <cuentaAhorro>
	                        <codigoProducto>{ data($cuentaAhorro/ns2:codigoProducto) }</codigoProducto>
	                        <codigoSubProducto>{ data($cuentaAhorro/ns2:codigoSubProducto) }</codigoSubProducto>
	                        <identificadorProducto>{ data($cuentaAhorro/ns2:identificadorProducto) }</identificadorProducto>
	                    </cuentaAhorro>
	                    <situacion>
	                    (
	                    	(: Codigo situacion desde PIF :)
	                    	<codigo>{data($cuentaAhorro/ns2:situacion/ns2:codigo)}</codigo>
	                        <estado>
	                        { 
	                        	(: Conversion de estados :)
	                        	if(data($cuentaAhorro/ns2:situacion/ns2:estado) = "1" or 
		                        	data($cuentaAhorro/ns2:situacion/ns2:estado) = "2" 
	                        	) then
	                        	"00"
	                        	else if(data($cuentaAhorro/ns2:situacion/ns2:estado) = "4"
	                        	) then
	                        	"01"
	                        	else 
	                        	("-1")
	                        }
	                        </estado>
	                        <glosa>
	                        { 
	                        	(:data($cuentaAhorro/ns55:situacion/ns55:glosa):)
	                        	(: Conversion descripcion de estados :)
	                        	if(data($cuentaAhorro/ns2:situacion/ns2:estado) = "1" or 
		                        	data($cuentaAhorro/ns2:situacion/ns2:estado) = "2" 
	                        	) then
	                        	"Activo"
	                        	else if(data($cuentaAhorro/ns2:situacion/ns2:estado) = "4"
	                        	) then
	                        	"Informativo"
	                        	else () 
	                       	}
	                       	</glosa>
	                    </situacion>
	                    {
	                    	for $cci in $cuentaAhorro/ns2:CCI
	                    	return
	                    		<CCI>{ data($cci) }</CCI>
	                    }
	                    {
		                    if (fn:empty($cuentaAhorro/ns2:saldo/ns2:saldoDisponible/text()) and 
		                    	fn:empty($cuentaAhorro/ns2:saldo/ns2:saldoContable/text())
		                    )then
		                    ()
		                    else(
		                    <saldo>
		                    {
		                    	for $saldoContable in $cuentaAhorro/ns2:saldo/ns2:saldoContable
	                    		return
		                    	<saldoContable>{ data($saldoContable) }</saldoContable>
		                    }
		                    {
		                    	for $saldoDisponible in $cuentaAhorro/ns2:saldo/ns2:saldoDisponible
	                    		return
		                        <saldoDisponible>{ data($saldoDisponible) }</saldoDisponible>
		                    }
		                    </saldo>
		                    )
	                    }
	                    <moneda>
	                        <codigoMoneda>{ data($cuentaAhorro/ns2:moneda/ns2:codigoMoneda) }</codigoMoneda>
	                    </moneda>
	                </cuentaAhorro>
	             }
	            </cuenta>            
	            )
            }
            {
            if ( (fn:count($iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito)<=0 and fn:empty($clienteResumenproductosObtenerRespParam1/*[1]))
            	(:or ():)
            )then
            ()
            else(
            	<credito>
            	{
	                for $creditoConsumo in $clienteResumenProductosObtenerCompResp1/credito/creditoConsumo
	                return
	                    <creditoConsumo>{ $creditoConsumo/@* , $creditoConsumo/node() }</creditoConsumo>
	            }
            	{   
            		(: PIF PIF :)             
	                if (fn:empty($clienteResumenproductosObtenerRespParam1/*[1]))then
		            ()
		            else(
                    for $creditoConsumo in $clienteResumenproductosObtenerRespParam1/ns2:credito/ns2:creditoConsumo
                    return
                        <creditoConsumo>
                            <creditoConsumo>
                                <codigoProducto>{ data($creditoConsumo/ns2:codigoProducto) }</codigoProducto>
                                <codigoSubProducto>{ data($creditoConsumo/ns2:codigoSubProducto) }</codigoSubProducto>
                                <identificadorProducto>{ data($creditoConsumo/ns2:identificadorProducto) }</identificadorProducto>
                            </creditoConsumo>
                            <moneda>
                                <codigoMoneda>{ data($creditoConsumo/ns2:moneda/ns2:codigoMoneda) }</codigoMoneda>
                            </moneda>
                            <situacion>
                                <estado>
                                { 
                                	(: Conversion de estados :)
                                	if(data($creditoConsumo/ns2:situacion/ns2:estado) = "2") then
                                	"00"
                                	else if(data($creditoConsumo/ns2:situacion/ns2:estado) = "5") then
                                	"01"
                                	else 
                                	("-1") 
                                }
                                </estado>
                                <glosa>
                                { 
                                	(:data($creditoConsumo/ns2:situacion/ns2:glosa):) 
                                	(: Conversion descripcion de estados :)
                                	if(data($creditoConsumo/ns2:situacion/ns2:estado) = "2") then
                                	"Activo"
                                	else if(data($creditoConsumo/ns2:situacion/ns2:estado) = "5") then
                                	"Informativo"
                                	else () 
                                }
                                </glosa>
                            </situacion>
                            {
                            if (fn:empty($creditoConsumo/ns2:montoCredito/ns2:montoSolicitado/text()))then
                            ()
                            else(
                            <montoCredito>                            	
                                <montoSolicitado>{ data($creditoConsumo/ns2:montoCredito/ns2:montoSolicitado) }</montoSolicitado>
                            </montoCredito>
                            )
                            }
                            <cuota>
                            	{
                            	if (fn:empty($creditoConsumo/ns2:cuota/ns2:numeroCuotasPagadas/text()))then
                            	()
                            	else(
                                <numeroCuotasPagadas>{ xs:int($creditoConsumo/ns2:cuota/ns2:numeroCuotasPagadas) }</numeroCuotasPagadas>
                                )
                                }                                
                                {
                                if (fn:empty($creditoConsumo/ns2:cuota/ns2:totalCuota/text()))then
                                ()
                                else(
                                <totalCuota>{ xs:int(xs:double($creditoConsumo/ns2:cuota/ns2:totalCuota)) }</totalCuota>
                                )
                                }                                
                                {
	                                if (fn:empty($creditoConsumo/ns2:cuota/ns2:fechaVencimientoProximaCuota/text()))then
	                                ()
	                                else(
		                                <fechaVencimientoProximaCuota>{ 
		                                fn:concat(
		                                fn:substring(xs:string(data($creditoConsumo/ns2:cuota/ns2:fechaVencimientoProximaCuota)),1,4),'-',
		                                fn:substring(xs:string(data($creditoConsumo/ns2:cuota/ns2:fechaVencimientoProximaCuota)),6,2),'-',
		                                fn:substring(xs:string(data($creditoConsumo/ns2:cuota/ns2:fechaVencimientoProximaCuota)),9,2)
		                                )
		                                }
		                                </fechaVencimientoProximaCuota>
	                                )
                                }                                
                                {
                                if (fn:empty($creditoConsumo/ns2:cuota/ns2:montoProximaCuota/text()))then
                                ()
                                else(
                                <montoProximaCuota>{ data($creditoConsumo/ns2:cuota/ns2:montoProximaCuota) }</montoProximaCuota>
                                )
                                }                                
                                {
                                if (fn:empty($creditoConsumo/ns2:cuota/ns2:numeroProximaCuota/text()))then
                                ()
                                else(
                                <numeroProximaCuota>{ xs:int($creditoConsumo/ns2:cuota/ns2:numeroProximaCuota) }</numeroProximaCuota>
                                )
                                }
                            </cuota>
                        </creditoConsumo>
                        )
	                }
	                (: Refinanciado PIF o ISAAC :)
	                {
	                  
		                for $creditoRefinanciado in $creditoRefinanciadoObtenerRespParam1/ns3:creditoRefinanciado
		                return
		                if ( data($creditoRefinanciado/ns3:codigoProducto) = '80' or 
	                    		data($creditoRefinanciado/ns3:codigoProducto) = '81'
	                    	) then
		                	(: 80 Consumo / Vehicular 81 Hipotercario :)
	                        <creditoConsumo>
	                            <creditoConsumo>
	                                <codigoProducto>500</codigoProducto>
		                            <codigoSubProducto>{ concat('500-0-',$creditoRefinanciado/ns3:codigoProducto) }</codigoSubProducto>
		                            <identificadorProducto>{ data($creditoRefinanciado/ns3:identificadorProducto) }</identificadorProducto>
	                            </creditoConsumo>
	                            <moneda>
	                            	<codigoMoneda>1</codigoMoneda>
	                            </moneda>
	                            <situacion>
	                            	<estado>01</estado>(: Informativo :)
	                            	<glosa>Informativo</glosa>
	                            </situacion>
	                            <montoCredito>
	                                <montoSolicitado>{ data($creditoRefinanciado/ns3:saldoTotal) }</montoSolicitado>
	                            </montoCredito>
	                            <cuota>
	                            	<numeroCuotasPagadas>{ data($creditoRefinanciado/ns3:cuotasPagadas) }</numeroCuotasPagadas>
	                            	<totalCuota>{ data($creditoRefinanciado/ns3:cuotasPactadas) }</totalCuota>
	                            	<montoProximaCuota>{ data($creditoRefinanciado/ns3:montoProximaCuota) }</montoProximaCuota>
					                <numeroProximaCuota>{sum((xs:int($creditoRefinanciado/ns3:cuotasPagadas),xs:int(1)))}</numeroProximaCuota>
	                            </cuota>
	                        </creditoConsumo>
	                        else ()
	                }
	                (: Vencido  :)
	                {
	                    for $creditoVencido in $creditoVencidoObtenerRespParam1/ns1:creditoVencido
	                    return
	                    	if ( data($creditoVencido/ns1:codigoProducto) = '01' or 
	                    		data($creditoVencido/ns1:codigoProducto) = '04' or  data($creditoVencido/ns1:codigoProducto) = '03'
	                    	) then
	                        <creditoConsumo>
	                            <creditoConsumo>
	                                <codigoProducto>600</codigoProducto>
	                                <codigoSubProducto>{ concat('600-0-',$creditoVencido/ns1:codigoProducto) }</codigoSubProducto>
	                                <identificadorProducto>{ data($creditoVencido/ns1:identificadorProducto) }</identificadorProducto>
	                            </creditoConsumo>
	                            <moneda>
	                            	<codigoMoneda>
	                            	{
			                        	if (data($creditoVencido/ns1:moneda) = 'Soles') then
			                        	'01'
			                        	else if (data($creditoVencido/ns1:moneda) = 'Dolares') then
			                        	'02'
			                        	else
			                        	'01'
			                        }
	                            	</codigoMoneda>
	                            </moneda>
	                            <situacion>
	                            	<estado>01</estado>(: Informativo :)
	                            </situacion>
	                            <montoCredito>
	                                <montoSolicitado>{ data($creditoVencido/ns1:saldoTotal) }</montoSolicitado>
	                            </montoCredito>
	                            <cuota>
	                            	<numeroCuotasPagadas>0</numeroCuotasPagadas>
	                            	<totalCuota>0</totalCuota>
	                            </cuota>
	                        </creditoConsumo>
	                        else ()
	                }
	                (: :)
	                {
		           	if (fn:count($iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito)<=0 )then
		           	()
		           	else
		           	(
		           		for $creditoISAAC in $iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito
		           		return  
		           			(: Automotriz :)
		           			if(fn:substring($creditoISAAC/ns1:COD_SUBPRODUCTO,7,2) = "40") then
		           			<creditoConsumo>
			                    <creditoConsumo>
			                        <codigoProducto>{ data($creditoISAAC/ns1:COD_PRODUCTO) }</codigoProducto>
			                        <codigoSubProducto>{ data($creditoISAAC/ns1:COD_SUBPRODUCTO) }</codigoSubProducto>
			                        <identificadorProducto>{ data($creditoISAAC/ns1:NRO_CREDITO) }</identificadorProducto>
			                    </creditoConsumo>
			                    <moneda>                        
			                        <codigoMoneda>{ data($creditoISAAC/ns1:COD_MONEDA) }</codigoMoneda>
			                    </moneda>
			                    <situacion>
			                        <codigo>{ data($creditoISAAC/ns1:CODIGO_ESTADO) }</codigo>
			                        <estado>
			                        { 
			                        	(: Conversion de estados :)
			                        	if(data($creditoISAAC/ns1:ESTADO_CREDITO) = "01" ) then 
			                        	"00"
			                        	else 
			                        	("-1") 
			                        }
			                        </estado>
			                        <glosa>
			                        { 
			                        	(: Conversion descripcion de estados :)
			                        	(:data($creditoISAAC/ns1:GLOSA_ESTADO):) 
			                        	if(data($creditoISAAC/ns1:ESTADO_CREDITO) = "01" ) then 
			                        	"Activo"
			                        	else () 
			                        }
			                        </glosa>
			                    </situacion>                    
			                    {
				                    if (fn:empty($creditoISAAC/ns1:MONTO_CREDITO/text()))then
				                    ()
				                    else(
				                    <montoCredito>
				                        <montoSolicitado>{ data($creditoISAAC/ns1:MONTO_CREDITO) }</montoSolicitado>
				                    </montoCredito>
			                    )
			                    }
			                    <cuota>
			                    	{
			                    	if (fn:empty($creditoISAAC/ns1:CUOTAS_PAGADAS/text()))then
			                    	()
			                    	else(
			                        <numeroCuotasPagadas>{ xs:int($creditoISAAC/ns1:CUOTAS_PAGADAS) }</numeroCuotasPagadas>
			                        )
			                        }
			                        {
			                        if (fn:empty($creditoISAAC/ns1:TOTAL_CUOTAS/text()))then
			                        ()
			                        else(
			                        <totalCuota>{ xs:int($creditoISAAC/ns1:TOTAL_CUOTAS) }</totalCuota>
			                        )
			                        }
			                        {
			                        if (fn:empty($creditoISAAC/ns1:FECVEN_PROX_CUOTA/text()))then
			                        ()
			                        else(
			                        <fechaVencimientoProximaCuota>
			                            {
			                                fn:concat(
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),1,4),'-',
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),6,2),'-',
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),9,2)
			                                )
			                            }
									</fechaVencimientoProximaCuota>
									)
									}									
									{
									if (fn:empty($creditoISAAC/ns1:VAL_PROX_CUOTA/text()))then
									()
									else
									(
			                        <montoProximaCuota>{ data($creditoISAAC/ns1:VAL_PROX_CUOTA) }</montoProximaCuota>
			                        )
			                        }			                        
			                        {
			                        if (fn:empty($creditoISAAC/ns1:NRO_PROX_CUOTA/text()))then
			                        ()
			                        else(
			                        <numeroProximaCuota>{ xs:int($creditoISAAC/ns1:NRO_PROX_CUOTA) }</numeroProximaCuota>
			                        )
			                        }
			                    </cuota>
			                </creditoConsumo>
			                else    
		           		 	(: Hipotecario :)
		           			if(fn:substring($creditoISAAC/ns1:COD_SUBPRODUCTO,7,2) = "20") then
		           			<creditoHipotecario>
			                    <creditoHipotecario>
			                        <codigoProducto>{ data($creditoISAAC/ns1:COD_PRODUCTO) }</codigoProducto>
			                        <codigoSubProducto>{ data($creditoISAAC/ns1:COD_SUBPRODUCTO) }</codigoSubProducto>
			                        <identificadorProducto>{ data($creditoISAAC/ns1:NRO_CREDITO) }</identificadorProducto>
			                    </creditoHipotecario>
			                    <moneda>                        
			                        <codigoMoneda>{ data($creditoISAAC/ns1:COD_MONEDA) }</codigoMoneda>
			                    </moneda>
			                    <situacion>
			                        <codigo>{ data($creditoISAAC/ns1:CODIGO_ESTADO) }</codigo>
			                        <estado>
			                        { 
			                        	(: Conversion de estados :)
			                        	if(data($creditoISAAC/ns1:ESTADO_CREDITO) = "01" ) then
			                        	"00"
			                        	else 
			                        	("-1")			                        	 
			                        }
			                        </estado>
			                        <glosa>
			                        {   
				                        (: Conversion descripcion de estados :)
				                        (: data($creditoISAAC/ns1:GLOSA_ESTADO) :) 
				                        if(data($creditoISAAC/ns1:ESTADO_CREDITO) = "01" ) then
			                        	"Activo"
			                        	else ()	
			                        }
			                        </glosa>
			                    </situacion>                    
			                    {
				                    if (fn:empty($creditoISAAC/ns1:MONTO_CREDITO/text()))then
				                    ()
				                    else(
				                    <montoCredito>
				                        <montoSolicitado>{ data($creditoISAAC/ns1:MONTO_CREDITO) }</montoSolicitado>
				                    </montoCredito>
			                    )
			                    }
			                    <cuota>
			                    	{
			                    	if (fn:empty($creditoISAAC/ns1:CUOTAS_PAGADAS/text()))then
			                    	()
			                    	else(
			                        <numeroCuotasPagadas>{ xs:int($creditoISAAC/ns1:CUOTAS_PAGADAS) }</numeroCuotasPagadas>
			                        )
			                        }			                        
			                        {			                        
			                        if (fn:empty($creditoISAAC/ns1:TOTAL_CUOTAS/text()))then
			                        ()
			                        else(
			                        <totalCuota>{ xs:int($creditoISAAC/ns1:TOTAL_CUOTAS) }</totalCuota>
			                        )
			                        }			                        
			                        {
			                        if (fn:empty($creditoISAAC/ns1:FECVEN_PROX_CUOTA/text()))then
			                        ()
			                        else(
			                        <fechaVencimientoProximaCuota>
			                            {
			                                fn:concat(
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),1,4),'-',
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),6,2),'-',
			                                fn:substring(xs:string(data($creditoISAAC/ns1:FECVEN_PROX_CUOTA)),9,2)
			                                )
			                            }
									</fechaVencimientoProximaCuota>
									)
									}									
									{
									if (fn:empty($creditoISAAC/ns1:VAL_PROX_CUOTA/text()))then
									()
									else
									(
			                        <montoProximaCuota>{ data($creditoISAAC/ns1:VAL_PROX_CUOTA) }</montoProximaCuota>
			                        )
			                        }			                        
			                        {
			                        if (fn:empty($creditoISAAC/ns1:NRO_PROX_CUOTA/text()))then
			                        ()
			                        else(
			                        <numeroProximaCuota>{ xs:int($creditoISAAC/ns1:NRO_PROX_CUOTA) }</numeroProximaCuota>
			                        )
			                        }
			                    </cuota>
			                </creditoHipotecario>
			                else ()
		                )
	                }
	                
	               
	            </credito>
	            )
            }
            
            { (:SOLO CUANDO POSEA LINEA DE CUOTA COMPRAS:)
                for $lineaCredito in $clienteResumenProductosObtenerCompResp1/lineaCredito
                where xs:string($lineaCredito/cupo/cupoTotal) != '0'
                return
                    <lineaCredito>{ $lineaCredito/@* , $lineaCredito/node() }</lineaCredito>
            }
    
    </ns5:clienteResumenProductosObtenerCompResp>
};

local:xq_OUT_PX_CORP_Cliente_ResumenProductosObtenerImpl_to_PX_PE_Cliente_ResumenproductosObtenerComp($ISAAC_ResumenCreditoObtenerImplRes, $ClienteResumenproductosObtenerRespParam, $creditoRefinanciadoObtenerRespParam, $creditoVencidoObtenerRespParam)
