xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLCCT/ConsultaClienteContratos/Req/IMPL/v2017.10";
(:: import schema at "../../../FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_IMPL_Req.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)
declare namespace ns3="http://mdwcorp.falabella.com/common/schema/clientservice";
(:: import schema at "../../../UT_EsquemasComunes_V.2.0/Specifications/XSD/Common/MdwCorp_Common_clientService.xsd" ::)


declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare variable $clientService1 as element(ns3:ClientService) external;

declare variable $credenciales as element() external;

declare function local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratosImpl($clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::),  $credenciales as element()) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
          <msgEnvio>
                    <entidad>0142</entidad>
                    <password>{fn:data($credenciales/password)}</password>
                    <tipoOperacion>VIEW</tipoOperacion>
                     <usuario>{fn:data($credenciales/username)}</usuario>
                    <calpart>TO</calpart>
                    <indcontrato>VI</indcontrato>
                    {
                        for $numeroDocumento in $clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento
                        return
                            <numdoc>{ data($numeroDocumento) }</numdoc>
                    }
                    {
                        for $tipoDocumento in $clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento
                        return
                            <tipdoc>{ fn-bea:pad-left(data($tipoDocumento), 3, '0') }</tipdoc>
                    }
                </msgEnvio>
    </ns2:runService>
};

local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratosImpl($clienteResumenProductosObtenerCompReq1,  $credenciales)
