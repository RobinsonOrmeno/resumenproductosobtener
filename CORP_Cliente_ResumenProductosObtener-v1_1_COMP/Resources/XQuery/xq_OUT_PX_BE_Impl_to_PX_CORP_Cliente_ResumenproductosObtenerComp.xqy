xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)

declare variable $clienteResumenProductosObtenerCompResp as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompResp) ::) external;
declare variable $clienteResumenProductosObtenerCompResp1 as element(ns1:clienteResumenProductosObtenerCompResp) external;
declare function local:xq_OUT_PX_BE_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($clienteResumenProductosObtenerCompResp as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompResp) ::)) as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompResp) ::) {
    <ns1:clienteResumenProductosObtenerCompResp>
    
       <estadoOperacion>
                <codigoOperacion>
                    {
                        if (((((((((fn:empty($clienteResumenProductosObtenerCompResp1/tarjetaCredito) and fn:empty($clienteResumenProductosObtenerCompResp1/inversion/depositoPlazo)) and fn:empty($clienteResumenProductosObtenerCompResp1/inversion/fondosMutuos)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaCorriente)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaAhorro)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaVista)) and fn:empty($clienteResumenProductosObtenerCompResp1/credito/creditoConsumo)) and fn:empty($clienteResumenProductosObtenerCompResp1/credito/creditoHipotecario)) and fn:empty($clienteResumenProductosObtenerCompResp1/lineaCredito))) then
                            ("01")
                        else 
                            "00"
                    }
</codigoOperacion>
                <glosaOperacion>
                    {
                        if (((((((((fn:empty($clienteResumenProductosObtenerCompResp1/tarjetaCredito) and fn:empty($clienteResumenProductosObtenerCompResp1/inversion/depositoPlazo)) and fn:empty($clienteResumenProductosObtenerCompResp1/inversion/fondosMutuos)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaCorriente)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaAhorro)) and fn:empty($clienteResumenProductosObtenerCompResp1/cuenta/cuentaVista)) and fn:empty($clienteResumenProductosObtenerCompResp1/credito/creditoConsumo)) and fn:empty($clienteResumenProductosObtenerCompResp1/credito/creditoHipotecario)) and fn:empty($clienteResumenProductosObtenerCompResp1/lineaCredito))) then
                            ("No se encontraron resultados")
                        else 
                            "Exito"
                    }
</glosaOperacion>
            </estadoOperacion>
            {
                for $tarjetaCredito in $clienteResumenProductosObtenerCompResp1/tarjetaCredito
                return
                    <tarjetaCredito>{ $tarjetaCredito/@* , $tarjetaCredito/node() }</tarjetaCredito>
            }
            {
                for $inversion in $clienteResumenProductosObtenerCompResp1/inversion
                return
                    <inversion>
                        {
                            for $depositoPlazo0 in $inversion/depositoPlazo
                            return
                                <depositoPlazo>
                                    {
                                        for $depositoPlazo in $depositoPlazo0/depositoPlazo
                                        return
                                            <depositoPlazo>
                                                {
                                                    for $codigoProducto in $depositoPlazo/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $depositoPlazo/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $depositoPlazo/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </depositoPlazo>
                                    }
                                    {
                                        for $situacion in $depositoPlazo0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $moneda0 in $depositoPlazo0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                    {
                                        for $fechaInversion in $depositoPlazo0/fechaInversion
                                        return
                                            <fechaInversion>{ data($fechaInversion) }</fechaInversion>
                                    }
                                    {
                                        for $montoInvertido in $depositoPlazo0/montoInvertido
                                        return
                                            <montoInvertido>{ data($montoInvertido) }</montoInvertido>
                                    }
                                    {
                                        for $fechaVencimiento in $depositoPlazo0/fechaVencimiento
                                        return
                                            <fechaVencimiento>{ data($fechaVencimiento) }</fechaVencimiento>
                                    }
                                    {
                                        for $valorRescate in $depositoPlazo0/valorRescate
                                        return
                                            <valorRescate>{ data($valorRescate) }</valorRescate>
                                    }
                                    {
                                        for $puedeCobrar in $depositoPlazo0/puedeCobrar
                                        return
                                            <puedeCobrar>{ data($puedeCobrar) }</puedeCobrar>
                                    }
                                    {
                                        for $esRenovacion in $depositoPlazo0/esRenovacion
                                        return
                                            <esRenovacion>{ data($esRenovacion) }</esRenovacion>
                                    }
                                </depositoPlazo>
                        }
                        {
                            for $fondosMutuos0 in $inversion/fondosMutuos
                            return
                                <fondosMutuos>
                                    {
                                        for $fondosMutuos in $fondosMutuos0/fondosMutuos
                                        return
                                            <fondosMutuos>
                                                {
                                                    for $codigoProducto in $fondosMutuos/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $fondosMutuos/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $fondosMutuos/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </fondosMutuos>
                                    }
                                    {
                                        for $moneda0 in $fondosMutuos0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                    {
                                        for $puedeCobrar in $fondosMutuos0/puedeCobrar
                                        return
                                            <puedeCobrar>{ data($puedeCobrar) }</puedeCobrar>
                                    }
                                    {
                                        for $saldoTotal in $fondosMutuos0/saldoTotal
                                        return
                                            <saldoTotal>{ data($saldoTotal) }</saldoTotal>
                                    }
                                    {
                                        for $alDia in $fondosMutuos0/alDia
                                        return
                                            <alDia>{ data($alDia) }</alDia>
                                    }
                                </fondosMutuos>
                        }
                    </inversion>
            }
            {
                for $cuenta in $clienteResumenProductosObtenerCompResp1/cuenta
                return
                    <cuenta>
                        {
                            for $cuentaCorriente0 in $cuenta/cuentaCorriente
                            return
                                <cuentaCorriente>
                                    {
                                        for $cuentaCorriente in $cuentaCorriente0/cuentaCorriente
                                        return
                                            <cuentaCorriente>
                                                {
                                                    for $codigoProducto in $cuentaCorriente/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $cuentaCorriente/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $cuentaCorriente/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </cuentaCorriente>
                                    }
                                    {
                                        for $situacion in $cuentaCorriente0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $codigoCBU in $cuentaCorriente0/codigoCBU
                                        return
                                            <codigoCBU>{ data($codigoCBU) }</codigoCBU>
                                    }
                                    {
                                        for $CCI in $cuentaCorriente0/CCI
                                        return
                                            <CCI>{ data($CCI) }</CCI>
                                    }
                                    {
                                        for $saldo in $cuentaCorriente0/saldo
                                        return
                                            <saldo>
                                                {
                                                    for $saldoContable in $saldo/saldoContable
                                                    return
                                                        <saldoContable>{ data($saldoContable) }</saldoContable>
                                                }
                                                {
                                                    for $saldoDisponible in $saldo/saldoDisponible
                                                    return
                                                        <saldoDisponible>{ data($saldoDisponible) }</saldoDisponible>
                                                }
                                                {
                                                    for $totalDisponible in $saldo/totalDisponible
                                                    return
                                                        <totalDisponible>{ data($totalDisponible) }</totalDisponible>
                                                }
                                            </saldo>
                                    }
                                    {
                                        for $moneda0 in $cuentaCorriente0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                    {
                                        for $lineaCredito in $cuentaCorriente0/lineaCredito
                                        return
                                            <lineaCredito>
                                                {
                                                    for $identificadorLinea in $lineaCredito/identificadorLinea
                                                    return
                                                        <identificadorLinea>{ data($identificadorLinea) }</identificadorLinea>
                                                }
                                                {
                                                    for $tipoLinea in $lineaCredito/tipoLinea
                                                    return
                                                        <tipoLinea>{ data($tipoLinea) }</tipoLinea>
                                                }
                                                {
                                                    for $cupo in $lineaCredito/cupo
                                                    return
                                                        <cupo>
                                                            {
                                                                for $cupoDisponible in $cupo/cupoDisponible
                                                                return
                                                                    <cupoDisponible>{ data($cupoDisponible) }</cupoDisponible>
                                                            }
                                                            {
                                                                for $cupoTotal in $cupo/cupoTotal
                                                                return
                                                                    <cupoTotal>{ data($cupoTotal) }</cupoTotal>
                                                            }
                                                            {
                                                                for $cupoUtilizado in $cupo/cupoUtilizado
                                                                return
                                                                    <cupoUtilizado>{ data($cupoUtilizado) }</cupoUtilizado>
                                                            }
                                                            {
                                                                for $saldoCMRExtendido in $cupo/saldoCMRExtendido
                                                                return
                                                                    <saldoCMRExtendido>{ data($saldoCMRExtendido) }</saldoCMRExtendido>
                                                            }
                                                            {
                                                                for $moneda0 in $cupo/moneda
                                                                return
                                                                    <moneda>
                                                                        {
                                                                            for $codigoMoneda in $moneda0/codigoMoneda
                                                                            return
                                                                                <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                                        }
                                                                        {
                                                                            for $moneda in $moneda0/moneda
                                                                            return
                                                                                <moneda>{ data($moneda) }</moneda>
                                                                        }
                                                                        {
                                                                            for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                                            return
                                                                                <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                                        }
                                                                    </moneda>
                                                            }
                                                        </cupo>
                                                }
                                            </lineaCredito>
                                    }
                                </cuentaCorriente>
                        }
                        {
                            for $cuentaAhorro0 in $cuenta/cuentaAhorro
                            return
                                <cuentaAhorro>
                                    {
                                        for $cuentaAhorro in $cuentaAhorro0/cuentaAhorro
                                        return
                                            <cuentaAhorro>
                                                {
                                                    for $codigoProducto in $cuentaAhorro/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $cuentaAhorro/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $cuentaAhorro/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </cuentaAhorro>
                                    }
                                    {
                                        for $situacion in $cuentaAhorro0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $codigoCBU in $cuentaAhorro0/codigoCBU
                                        return
                                            <codigoCBU>{ data($codigoCBU) }</codigoCBU>
                                    }
                                    {
                                        for $CCI in $cuentaAhorro0/CCI
                                        return
                                            <CCI>{ data($CCI) }</CCI>
                                    }
                                    {
                                        for $saldo in $cuentaAhorro0/saldo
                                        return
                                            <saldo>
                                                {
                                                    for $saldoContable in $saldo/saldoContable
                                                    return
                                                        <saldoContable>{ data($saldoContable) }</saldoContable>
                                                }
                                                {
                                                    for $saldoDisponible in $saldo/saldoDisponible
                                                    return
                                                        <saldoDisponible>{ data($saldoDisponible) }</saldoDisponible>
                                                }
                                                {
                                                    for $totalDisponible in $saldo/totalDisponible
                                                    return
                                                        <totalDisponible>{ data($totalDisponible) }</totalDisponible>
                                                }
                                            </saldo>
                                    }
                                    {
                                        for $moneda0 in $cuentaAhorro0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                </cuentaAhorro>
                        }
                        {
                            for $cuentaVista0 in $cuenta/cuentaVista
                            return
                                <cuentaVista>
                                    {
                                        for $cuentaVista in $cuentaVista0/cuentaVista
                                        return
                                            <cuentaVista>
                                                {
                                                    for $codigoProducto in $cuentaVista/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $cuentaVista/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $cuentaVista/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </cuentaVista>
                                    }
                                    {
                                        for $situacion in $cuentaVista0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $codigoCBU in $cuentaVista0/codigoCBU
                                        return
                                            <codigoCBU>{ data($codigoCBU) }</codigoCBU>
                                    }
                                    {
                                        for $CCI in $cuentaVista0/CCI
                                        return
                                            <CCI>{ data($CCI) }</CCI>
                                    }
                                    {
                                        for $saldo in $cuentaVista0/saldo
                                        return
                                            <saldo>
                                                {
                                                    for $saldoContable in $saldo/saldoContable
                                                    return
                                                        <saldoContable>{ data($saldoContable) }</saldoContable>
                                                }
                                                {
                                                    for $saldoDisponible in $saldo/saldoDisponible
                                                    return
                                                        <saldoDisponible>{ data($saldoDisponible) }</saldoDisponible>
                                                }
                                                {
                                                    for $totalDisponible in $saldo/totalDisponible
                                                    return
                                                        <totalDisponible>{ data($totalDisponible) }</totalDisponible>
                                                }
                                            </saldo>
                                    }
                                    {
                                        for $moneda0 in $cuentaVista0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                </cuentaVista>
                        }
                    </cuenta>
            }
            {
                for $credito in $clienteResumenProductosObtenerCompResp1/credito
                return
                
                    <credito>
                        {
                            for $creditoConsumo0 in $credito/creditoConsumo
                            return
                                <creditoConsumo>
                                    {
                                        for $creditoConsumo in $creditoConsumo0/creditoConsumo
                                        return
                                
                                            <creditoConsumo>
                                                {
                                                    for $codigoProducto in $creditoConsumo/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $creditoConsumo/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $creditoConsumo/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </creditoConsumo>
                                    }
                                    {
                                        for $moneda0 in $creditoConsumo0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                    {
                                        for $situacion in $creditoConsumo0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $montoCredito in $creditoConsumo0/montoCredito
                                        return
                                            <montoCredito>
                                                {
                                                    for $montoSolicitado in $montoCredito/montoSolicitado
                                                    return
                                                        <montoSolicitado>{ data($montoSolicitado) }</montoSolicitado>
                                                }
                                            </montoCredito>
                                    }
                                    {
                                        for $cuota in $creditoConsumo0/cuota
                                        return
                                            <cuota>
                                                {
                                                    for $cantidadCuota in $cuota/cantidadCuota
                                                    return
                                                        <cantidadCuota>{ data($cantidadCuota) }</cantidadCuota>
                                                }
                                                {
                                                    for $cuotaPendientes in $cuota/cuotaPendientes
                                                    return
                                                        <cuotaPendientes>{ data($cuotaPendientes) }</cuotaPendientes>
                                                }
                                                {
                                                    for $numeroCuotasPagadas in $cuota/numeroCuotasPagadas
                                                    return
                                                        <numeroCuotasPagadas>{ data($numeroCuotasPagadas) }</numeroCuotasPagadas>
                                                }
                                                {
                                                    for $totalCuota in $cuota/totalCuota
                                                    return
                                                        <totalCuota>{ data($totalCuota) }</totalCuota>
                                                }
                                                {
                                                    for $amortizacion in $cuota/amortizacion
                                                    return
                                                        <amortizacion>{ data($amortizacion) }</amortizacion>
                                                }
                                                {
                                                    for $estado in $cuota/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $fechaEmisionCuota in $cuota/fechaEmisionCuota
                                                    return
                                                        <fechaEmisionCuota>{ data($fechaEmisionCuota) }</fechaEmisionCuota>
                                                }
                                                {
                                                    for $fechaVencimiento in $cuota/fechaVencimiento
                                                    return
                                                        <fechaVencimiento>{ data($fechaVencimiento) }</fechaVencimiento>
                                                }
                                                {
                                                    for $fechaVencimientoProximaCuota in $cuota/fechaVencimientoProximaCuota
                                                    return
                                                        <fechaVencimientoProximaCuota>{ data($fechaVencimientoProximaCuota) }</fechaVencimientoProximaCuota>
                                                }
                                                {
                                                    for $habilitadaPago in $cuota/habilitadaPago
                                                    return
                                                        <habilitadaPago>{ data($habilitadaPago) }</habilitadaPago>
                                                }
                                                {
                                                    for $montoCuotasMorosas in $cuota/montoCuotasMorosas
                                                    return
                                                        <montoCuotasMorosas>{ data($montoCuotasMorosas) }</montoCuotasMorosas>
                                                }
                                                {
                                                    for $numeroCuota in $cuota/numeroCuota
                                                    return
                                                        <numeroCuota>{ data($numeroCuota) }</numeroCuota>
                                                }
                                                {
                                                    for $saldoRestante in $cuota/saldoRestante
                                                    return
                                                        <saldoRestante>{ data($saldoRestante) }</saldoRestante>
                                                }
                                                {
                                                    for $permitePago in $cuota/permitePago
                                                    return
                                                        <permitePago>{ data($permitePago) }</permitePago>
                                                }
                                                {
                                                    for $valorCuota in $cuota/valorCuota
                                                    return
                                                        <valorCuota>{ data($valorCuota) }</valorCuota>
                                                }
                                                {
                                                    for $valorCuotaConInteres in $cuota/valorCuotaConInteres
                                                    return
                                                        <valorCuotaConInteres>{ data($valorCuotaConInteres) }</valorCuotaConInteres>
                                                }
                                                {
                                                    for $montoProximaCuota in $cuota/montoProximaCuota
                                                    return
                                                        <montoProximaCuota>{ data($montoProximaCuota) }</montoProximaCuota>
                                                }
                                                {
                                                    for $fechaPago in $cuota/fechaPago
                                                    return
                                                        <fechaPago>{ data($fechaPago) }</fechaPago>
                                                }
                                                {
                                                    for $montoPagado in $cuota/montoPagado
                                                    return
                                                        <montoPagado>{ data($montoPagado) }</montoPagado>
                                                }
                                                {
                                                    for $numeroProximaCuota in $cuota/numeroProximaCuota
                                                    return
                                                        <numeroProximaCuota>{ data($numeroProximaCuota) }</numeroProximaCuota>
                                                }
                                                {
                                                    for $montoIva in $cuota/montoIva
                                                    return
                                                        <montoIva>{ data($montoIva) }</montoIva>
                                                }
                                                {
                                                    for $saldoDeudaCapital in $cuota/saldoDeudaCapital
                                                    return
                                                        <saldoDeudaCapital>{ data($saldoDeudaCapital) }</saldoDeudaCapital>
                                                }
                                                {
                                                    for $valorTotalCuota in $cuota/valorTotalCuota
                                                    return
                                                        <valorTotalCuota>{ data($valorTotalCuota) }</valorTotalCuota>
                                                }
                                                {
                                                    for $deudaPendiente in $cuota/deudaPendiente
                                                    return
                                                        <deudaPendiente>{ data($deudaPendiente) }</deudaPendiente>
                                                }
                                                {
                                                    for $interes in $cuota/interes
                                                    return
                                                        <interes>
                                                            {
                                                                for $codigoInteres in $interes/codigoInteres
                                                                return
                                                                    <codigoInteres>{ data($codigoInteres) }</codigoInteres>
                                                            }
                                                            {
                                                                for $descripcionInteres in $interes/descripcionInteres
                                                                return
                                                                    <descripcionInteres>{ data($descripcionInteres) }</descripcionInteres>
                                                            }
                                                            {
                                                                for $porcentaje in $interes/porcentaje
                                                                return
                                                                    <porcentaje>{ data($porcentaje) }</porcentaje>
                                                            }
                                                            {
                                                                for $tipoInteres in $interes/tipoInteres
                                                                return
                                                                    <tipoInteres>{ data($tipoInteres) }</tipoInteres>
                                                            }
                                                            {
                                                                for $totalInteres in $interes/totalInteres
                                                                return
                                                                    <totalInteres>{ data($totalInteres) }</totalInteres>
                                                            }
                                                            {
                                                                for $valorInteres in $interes/valorInteres
                                                                return
                                                                    <valorInteres>{ data($valorInteres) }</valorInteres>
                                                            }
                                                        </interes>
                                                }
                                                {
                                                    for $moneda0 in $cuota/moneda
                                                    return
                                                        <moneda>
                                                            {
                                                                for $codigoMoneda in $moneda0/codigoMoneda
                                                                return
                                                                    <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                            }
                                                            {
                                                                for $moneda in $moneda0/moneda
                                                                return
                                                                    <moneda>{ data($moneda) }</moneda>
                                                            }
                                                            {
                                                                for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                                return
                                                                    <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                            }
                                                        </moneda>
                                                }
                                                {
                                                    for $seguro in $cuota/seguro
                                                    return
                                                        <seguro>
                                                            {
                                                                for $codigoSeguro in $seguro/codigoSeguro
                                                                return
                                                                    <codigoSeguro>{ data($codigoSeguro) }</codigoSeguro>
                                                            }
                                                            {
                                                                for $glosaSeguro in $seguro/glosaSeguro
                                                                return
                                                                    <glosaSeguro>{ data($glosaSeguro) }</glosaSeguro>
                                                            }
                                                            {
                                                                for $costoMensual in $seguro/costoMensual
                                                                return
                                                                    <costoMensual>{ data($costoMensual) }</costoMensual>
                                                            }
                                                            {
                                                                for $impuesto in $seguro/impuesto
                                                                return
                                                                    <impuesto>{ data($impuesto) }</impuesto>
                                                            }
                                                        </seguro>
                                                }
                                            </cuota>
                                    }
                                </creditoConsumo>
                        }
                        {
                            for $creditoHipotecario0 in $credito/creditoHipotecario
                            return
                                <creditoHipotecario>
                                    {
                                        for $creditoHipotecario in $creditoHipotecario0/creditoHipotecario
                                        return
                                            <creditoHipotecario>
                                                {
                                                    for $codigoProducto in $creditoHipotecario/codigoProducto
                                                    return
                                                        <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                                }
                                                {
                                                    for $codigoSubProducto in $creditoHipotecario/codigoSubProducto
                                                    return
                                                        <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                                }
                                                {
                                                    for $identificadorProducto in $creditoHipotecario/identificadorProducto
                                                    return
                                                        <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                                }
                                            </creditoHipotecario>
                                    }
                                    {
                                        for $moneda0 in $creditoHipotecario0/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                    {
                                        for $situacion in $creditoHipotecario0/situacion
                                        return
                                            <situacion>
                                                {
                                                    for $codigo in $situacion/codigo
                                                    return
                                                        <codigo>{ data($codigo) }</codigo>
                                                }
                                                {
                                                    for $estado in $situacion/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $glosa in $situacion/glosa
                                                    return
                                                        <glosa>{ data($glosa) }</glosa>
                                                }
                                            </situacion>
                                    }
                                    {
                                        for $montoCredito in $creditoHipotecario0/montoCredito
                                        return
                                            <montoCredito>
                                                {
                                                    for $montoSolicitado in $montoCredito/montoSolicitado
                                                    return
                                                        <montoSolicitado>{ data($montoSolicitado) }</montoSolicitado>
                                                }
                                            </montoCredito>
                                    }
                                    {
                                        for $cuota in $creditoHipotecario0/cuota
                                        return
                                            <cuota>
                                                {
                                                    for $cantidadCuota in $cuota/cantidadCuota
                                                    return
                                                        <cantidadCuota>{ data($cantidadCuota) }</cantidadCuota>
                                                }
                                                {
                                                    for $cuotaPendientes in $cuota/cuotaPendientes
                                                    return
                                                        <cuotaPendientes>{ data($cuotaPendientes) }</cuotaPendientes>
                                                }
                                                {
                                                    for $numeroCuotasPagadas in $cuota/numeroCuotasPagadas
                                                    return
                                                        <numeroCuotasPagadas>{ data($numeroCuotasPagadas) }</numeroCuotasPagadas>
                                                }
                                                {
                                                    for $totalCuota in $cuota/totalCuota
                                                    return
                                                        <totalCuota>{ data($totalCuota) }</totalCuota>
                                                }
                                                {
                                                    for $amortizacion in $cuota/amortizacion
                                                    return
                                                        <amortizacion>{ data($amortizacion) }</amortizacion>
                                                }
                                                {
                                                    for $estado in $cuota/estado
                                                    return
                                                        <estado>{ data($estado) }</estado>
                                                }
                                                {
                                                    for $fechaEmisionCuota in $cuota/fechaEmisionCuota
                                                    return
                                                        <fechaEmisionCuota>{ data($fechaEmisionCuota) }</fechaEmisionCuota>
                                                }
                                                {
                                                    for $fechaVencimiento in $cuota/fechaVencimiento
                                                    return
                                                        <fechaVencimiento>{ data($fechaVencimiento) }</fechaVencimiento>
                                                }
                                                {
                                                    for $fechaVencimientoProximaCuota in $cuota/fechaVencimientoProximaCuota
                                                    return
                                                        <fechaVencimientoProximaCuota>{ data($fechaVencimientoProximaCuota) }</fechaVencimientoProximaCuota>
                                                }
                                                {
                                                    for $habilitadaPago in $cuota/habilitadaPago
                                                    return
                                                        <habilitadaPago>{ data($habilitadaPago) }</habilitadaPago>
                                                }
                                                {
                                                    for $montoCuotasMorosas in $cuota/montoCuotasMorosas
                                                    return
                                                        <montoCuotasMorosas>{ data($montoCuotasMorosas) }</montoCuotasMorosas>
                                                }
                                                {
                                                    for $numeroCuota in $cuota/numeroCuota
                                                    return
                                                        <numeroCuota>{ data($numeroCuota) }</numeroCuota>
                                                }
                                                {
                                                    for $saldoRestante in $cuota/saldoRestante
                                                    return
                                                        <saldoRestante>{ data($saldoRestante) }</saldoRestante>
                                                }
                                                {
                                                    for $permitePago in $cuota/permitePago
                                                    return
                                                        <permitePago>{ data($permitePago) }</permitePago>
                                                }
                                                {
                                                    for $valorCuota in $cuota/valorCuota
                                                    return
                                                        <valorCuota>{ data($valorCuota) }</valorCuota>
                                                }
                                                {
                                                    for $valorCuotaConInteres in $cuota/valorCuotaConInteres
                                                    return
                                                        <valorCuotaConInteres>{ data($valorCuotaConInteres) }</valorCuotaConInteres>
                                                }
                                                {
                                                    for $montoProximaCuota in $cuota/montoProximaCuota
                                                    return
                                                        <montoProximaCuota>{ data($montoProximaCuota) }</montoProximaCuota>
                                                }
                                                {
                                                    for $fechaPago in $cuota/fechaPago
                                                    return
                                                        <fechaPago>{ data($fechaPago) }</fechaPago>
                                                }
                                                {
                                                    for $montoPagado in $cuota/montoPagado
                                                    return
                                                        <montoPagado>{ data($montoPagado) }</montoPagado>
                                                }
                                                {
                                                    for $numeroProximaCuota in $cuota/numeroProximaCuota
                                                    return
                                                        <numeroProximaCuota>{ data($numeroProximaCuota) }</numeroProximaCuota>
                                                }
                                                {
                                                    for $montoIva in $cuota/montoIva
                                                    return
                                                        <montoIva>{ data($montoIva) }</montoIva>
                                                }
                                                {
                                                    for $saldoDeudaCapital in $cuota/saldoDeudaCapital
                                                    return
                                                        <saldoDeudaCapital>{ data($saldoDeudaCapital) }</saldoDeudaCapital>
                                                }
                                                {
                                                    for $valorTotalCuota in $cuota/valorTotalCuota
                                                    return
                                                        <valorTotalCuota>{ data($valorTotalCuota) }</valorTotalCuota>
                                                }
                                                {
                                                    for $deudaPendiente in $cuota/deudaPendiente
                                                    return
                                                        <deudaPendiente>{ data($deudaPendiente) }</deudaPendiente>
                                                }
                                                {
                                                    for $interes in $cuota/interes
                                                    return
                                                        <interes>
                                                            {
                                                                for $codigoInteres in $interes/codigoInteres
                                                                return
                                                                    <codigoInteres>{ data($codigoInteres) }</codigoInteres>
                                                            }
                                                            {
                                                                for $descripcionInteres in $interes/descripcionInteres
                                                                return
                                                                    <descripcionInteres>{ data($descripcionInteres) }</descripcionInteres>
                                                            }
                                                            {
                                                                for $porcentaje in $interes/porcentaje
                                                                return
                                                                    <porcentaje>{ data($porcentaje) }</porcentaje>
                                                            }
                                                            {
                                                                for $tipoInteres in $interes/tipoInteres
                                                                return
                                                                    <tipoInteres>{ data($tipoInteres) }</tipoInteres>
                                                            }
                                                            {
                                                                for $totalInteres in $interes/totalInteres
                                                                return
                                                                    <totalInteres>{ data($totalInteres) }</totalInteres>
                                                            }
                                                            {
                                                                for $valorInteres in $interes/valorInteres
                                                                return
                                                                    <valorInteres>{ data($valorInteres) }</valorInteres>
                                                            }
                                                        </interes>
                                                }
                                                {
                                                    for $moneda0 in $cuota/moneda
                                                    return
                                                        <moneda>
                                                            {
                                                                for $codigoMoneda in $moneda0/codigoMoneda
                                                                return
                                                                    <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                            }
                                                            {
                                                                for $moneda in $moneda0/moneda
                                                                return
                                                                    <moneda>{ data($moneda) }</moneda>
                                                            }
                                                            {
                                                                for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                                return
                                                                    <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                            }
                                                        </moneda>
                                                }
                                                {
                                                    for $seguro in $cuota/seguro
                                                    return
                                                        <seguro>
                                                            {
                                                                for $codigoSeguro in $seguro/codigoSeguro
                                                                return
                                                                    <codigoSeguro>{ data($codigoSeguro) }</codigoSeguro>
                                                            }
                                                            {
                                                                for $glosaSeguro in $seguro/glosaSeguro
                                                                return
                                                                    <glosaSeguro>{ data($glosaSeguro) }</glosaSeguro>
                                                            }
                                                            {
                                                                for $costoMensual in $seguro/costoMensual
                                                                return
                                                                    <costoMensual>{ data($costoMensual) }</costoMensual>
                                                            }
                                                            {
                                                                for $impuesto in $seguro/impuesto
                                                                return
                                                                    <impuesto>{ data($impuesto) }</impuesto>
                                                            }
                                                        </seguro>
                                                }
                                            </cuota>
                                    }
                                </creditoHipotecario>
                        }
                    </credito>
            }
            {
                for $lineaCredito0 in $clienteResumenProductosObtenerCompResp1/lineaCredito
                return
                    <lineaCredito>
                        {
                            for $lineaCredito in $lineaCredito0/lineaCredito
                            return
                                <lineaCredito>
                                    {
                                        for $codigoProducto in $lineaCredito/codigoProducto
                                        return
                                            <codigoProducto>{ data($codigoProducto) }</codigoProducto>
                                    }
                                    {
                                        for $codigoSubProducto in $lineaCredito/codigoSubProducto
                                        return
                                            <codigoSubProducto>{ data($codigoSubProducto) }</codigoSubProducto>
                                    }
                                    {
                                        for $identificadorProducto in $lineaCredito/identificadorProducto
                                        return
                                            <identificadorProducto>{ data($identificadorProducto) }</identificadorProducto>
                                    }
                                </lineaCredito>
                        }
                        {
                            for $cuentaCorrienteCargo in $lineaCredito0/cuentaCorrienteCargo
                            return
                                <cuentaCorrienteCargo>{ data($cuentaCorrienteCargo) }</cuentaCorrienteCargo>
                        }
                        {
                            for $situacion in $lineaCredito0/situacion
                            return
                                <situacion>
                                    {
                                        for $codigo in $situacion/codigo
                                        return
                                            <codigo>{ data($codigo) }</codigo>
                                    }
                                    {
                                        for $estado in $situacion/estado
                                        return
                                            <estado>{ data($estado) }</estado>
                                    }
                                    {
                                        for $glosa in $situacion/glosa
                                        return
                                            <glosa>{ data($glosa) }</glosa>
                                    }
                                </situacion>
                        }
                        {
                            for $moneda0 in $lineaCredito0/moneda
                            return
                                <moneda>
                                    {
                                        for $codigoMoneda in $moneda0/codigoMoneda
                                        return
                                            <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                    }
                                    {
                                        for $moneda in $moneda0/moneda
                                        return
                                            <moneda>{ data($moneda) }</moneda>
                                    }
                                    {
                                        for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                        return
                                            <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                    }
                                </moneda>
                        }
                        {
                            for $cupo in $lineaCredito0/cupo
                            return
                                <cupo>
                                    {
                                        for $cupoDisponible in $cupo/cupoDisponible
                                        return
                                            <cupoDisponible>{ data($cupoDisponible) }</cupoDisponible>
                                    }
                                    {
                                        for $cupoTotal in $cupo/cupoTotal
                                        return
                                            <cupoTotal>{ data($cupoTotal) }</cupoTotal>
                                    }
                                    {
                                        for $cupoUtilizado in $cupo/cupoUtilizado
                                        return
                                            <cupoUtilizado>{ data($cupoUtilizado) }</cupoUtilizado>
                                    }
                                    {
                                        for $saldoCMRExtendido in $cupo/saldoCMRExtendido
                                        return
                                            <saldoCMRExtendido>{ data($saldoCMRExtendido) }</saldoCMRExtendido>
                                    }
                                    {
                                        for $moneda0 in $cupo/moneda
                                        return
                                            <moneda>
                                                {
                                                    for $codigoMoneda in $moneda0/codigoMoneda
                                                    return
                                                        <codigoMoneda>{ data($codigoMoneda) }</codigoMoneda>
                                                }
                                                {
                                                    for $moneda in $moneda0/moneda
                                                    return
                                                        <moneda>{ data($moneda) }</moneda>
                                                }
                                                {
                                                    for $abreviacionMoneda in $moneda0/abreviacionMoneda
                                                    return
                                                        <abreviacionMoneda>{ data($abreviacionMoneda) }</abreviacionMoneda>
                                                }
                                            </moneda>
                                    }
                                </cupo>
                        }
                    </lineaCredito>
            }
    </ns1:clienteResumenProductosObtenerCompResp>
};

local:xq_OUT_PX_BE_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($clienteResumenProductosObtenerCompResp)
