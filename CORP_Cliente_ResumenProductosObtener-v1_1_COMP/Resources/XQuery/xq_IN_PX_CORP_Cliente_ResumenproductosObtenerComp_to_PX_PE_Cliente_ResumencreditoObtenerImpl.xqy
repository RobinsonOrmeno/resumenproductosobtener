xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Req-v2015.12";
(:: import schema at "../../../BCO_PE_ISAAC_Cliente_ResumencreditoObtener-v1_0_IMPL/Resources/Schema/OSB_Cliente_Resumencredito_ObtenerReq.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare function local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_PE_Cliente_ResumencreditoObtenerImpl($clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:ISAAC_ResumenCreditoObtenerImplReq) ::) {
    <ns2:ISAAC_ResumenCreditoObtenerImplReq>
                <ns2:TIPO_DOCUMENTO>{ data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento) }</ns2:TIPO_DOCUMENTO>
            <ns2:NRO_DOCUMENTO>{ data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento) }</ns2:NRO_DOCUMENTO>
  
    </ns2:ISAAC_ResumenCreditoObtenerImplReq>
};

local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_PE_Cliente_ResumencreditoObtenerImpl($clienteResumenProductosObtenerCompReq1)
