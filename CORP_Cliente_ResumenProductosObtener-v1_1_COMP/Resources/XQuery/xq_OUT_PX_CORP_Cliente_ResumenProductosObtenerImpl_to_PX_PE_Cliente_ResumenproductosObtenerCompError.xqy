xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare function local:xq_OUT_PX_CORP_Cliente_ResumenProductosObtenerImpl_to_PX_PE_Cliente_ResumenproductosObtenerCompError($clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:clienteResumenProductosObtenerCompResp) ::) {
    <ns2:clienteResumenProductosObtenerCompResp>
       <estadoOperacion>
                <codigoOperacion>01</codigoOperacion>
                <glosaOperacion>
                { 
                	if( fn:empty(data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento)) or
                		data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento) = ""
                	) then
                		"Debe ingresar Numero de Documento"
                	else if( fn:empty(data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento)) or
                		data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento) = ""
                	) then
                		"Debe ingresar Tipo de Documento"
                	else ()                        	
                }
                </glosaOperacion>
                       </estadoOperacion>
    </ns2:clienteResumenProductosObtenerCompResp>
};

local:xq_OUT_PX_CORP_Cliente_ResumenProductosObtenerImpl_to_PX_PE_Cliente_ResumenproductosObtenerCompError($clienteResumenProductosObtenerCompReq1)
