xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLDIS/ConsultaSaldos/Resp/IMPL/v2017.10";
(:: import schema at "../../../FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_IMPL_Resp.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)

declare variable $runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::) external;
declare variable $contrato as element(*) external;
declare variable $satFLCLDISConsultaSaldosImplResp1 as element(ns1:SatFLCLDISConsultaSaldosImplResp) external;
declare function local:xq_OUT_BS_SJ_SAT_FLCLDIS_ConsultaSaldos_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp2($runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::)) as element() (:: schema-element(ns2:clienteResumenProductosObtenerCompResp) ::) {
    <ns2:clienteResumenProductosObtenerCompResp>
    
    
		    {  if(xs:string(data($satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:retorno)) = "000" ) then(
		    
		            if(data($contrato/producto)="01")then(
	              	<tarjetaCredito>
	       		
						<tarjetaCredito>
				        	  <codigoProducto>200</codigoProducto>
			                  {
			                      for $subproducto in $contrato/subprodu
			                      return
			                      	<codigoSubProducto>{ fn:concat('200-0-' , data($subproducto)) }</codigoSubProducto>
			                  }
			                  {
			                      for $pan in $contrato/pan
			                      return
			                      	<identificadorProducto>{ data($pan) }</identificadorProducto>
			                  }
			         	</tarjetaCredito>
							{
							let $indicadorOperatividad :=  
								if(fn:string-length(xs:string(fn:data($contrato/indblqope)))>0) then 
									xs:string(fn:data($contrato/indblqope)) 
								else 
									'N'
			                let $codigoBloqueo := xs:string(fn:data($contrato/codblqtar))
			                return
			                  
			                local:homologacionCuentaOmni($indicadorOperatividad,$codigoBloqueo)
						}
						<moneda>
		                {
			                if ($contrato/clamon1)
			                then <codigoMoneda>{fn:data($contrato/clamon1)}</codigoMoneda>
			                else ()
			            }
		                </moneda>
						<plastico>
				            <titularidad>
	                        { 
	                        	if( fn:data($contrato/calpartsal) = "TI") then
	                        	("Titular")
	                        	else
	                        	("Adicional") 
	                        }
	                        </titularidad>
	                             {
                  let $var := 0
                  let $codigoBloqueo := xs:string(fn:data($contrato/codblqtar))
                  let $codigoSituacion := xs:string(fn:data($contrato/indsittar))
                  let $codigoBaja := xs:string(fn:data($contrato/motbajatar))
                  
                  let $glosaBloqueo := xs:string(fn:data($contrato/desblqtar))
                  let $glosaBaja := xs:string(fn:data($contrato/desmottar))
                  let $glosaSituacion := xs:string(fn:data($contrato/dessittar))
                  return
                  
                   local:homologacionTarjeta(
                              $codigoBloqueo, 
                              $codigoBaja, 
                              $codigoSituacion,
                              $glosaBloqueo,
                              $glosaBaja,
                              $glosaSituacion) 
                 
              }
         
	                     
					       	
		                </plastico>
		                <contrato>
		                	<situacion>
		                		<codigo>{ xs:int(fn:data($contrato/codestcta)) }</codigo>
		                		<glosa>{ xs:string(fn:data($contrato/desestctared)) }</glosa>
		                	</situacion>
		                	<morosidad>
								<diasMora>{ xs:int(fn:data($contrato/diasmora1)) }</diasMora>
							</morosidad>
		                </contrato>
						<cuentaTarjetaCredito>
		                    <identificador>
		                        {
		                        	let $centroAlta := data($contrato/centaltasal)
		                        	let $entidad := data($contrato/codentsal)
		                        	let $cuenta := data($contrato/cuentasal)
		                        	return
		                            fn:concat($centroAlta,$entidad,$cuenta)
		                        }
							</identificador>
		                </cuentaTarjetaCredito>
						<cupoNacional>
						{
							for $dislinCO in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = 'CO']/ns1:dislin
			                return          
	                    		<cupoDisponible>{data($dislinCO)}</cupoDisponible>
	                    }
	                    {
	                    	for $limcrelinCO in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = 'CO']/ns1:limcrelin
			                return
		                    	<cupoTotal>{data($limcrelinCO)}</cupoTotal>
		                }
		                {
		                	for $saldeulinCO in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = 'CO']/ns1:saldeulin
			                return
		                    	<cupoUtilizado>{data($saldeulinCO)}</cupoUtilizado>
		                }
		                </cupoNacional>
		                <avance>
		                {
		                	for $dislin02 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0002']/ns1:dislin
			                return
								<cupoDisponible>{data($dislin02)}</cupoDisponible>
		                }
		                {
		                	for $limcrelin02 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0002']/ns1:limcrelin
			                return
		                    	<cupoTotal>{data($limcrelin02)}</cupoTotal>
		                }
		                {
		                	for $saldeulin02 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0002']/ns1:saldeulin
			                return
		                    	<cupoUtilizado>{data($saldeulin02)}</cupoUtilizado>
						}
						</avance>
						<superAvance>
						{
							for $dislin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:dislin
			                return
								<cupoDisponible>{data($dislin03)}</cupoDisponible>
		                }
		                {
		                	for $limcrelin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:limcrelin
			                return
		                    	<cupoTotal>{data($limcrelin03)}</cupoTotal>
		                }
		                {
		                	for $limcrelin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:saldeulin
			                return
		                    	<cupoUtilizado>{data($limcrelin03)}</cupoUtilizado>
						}
						</superAvance>
						<resumenProximaFacturacion>
				        {
				        if ($contrato/fecvtopro/valueDate)
				        then 
				        <fechaProximoVencimiento>
				        {								
					        fn:concat(
					        fn:substring(data($contrato/fecvtopro/valueDate),7,4),'-',
					        fn:substring(data($contrato/fecvtopro/valueDate),4,2),'-',
					        fn:substring(data($contrato/fecvtopro/valueDate),1,2)       
					        )
					        }
					        </fechaProximoVencimiento>
					        else ()
				        }
				        </resumenProximaFacturacion>
				        
				         <habiente>
              				 <identificador>{data($contrato/numbencta)}</identificador>
           			      </habiente>
				     </tarjetaCredito>	
				     )else(
				     	(: Linea 2 para seguros:)
				     	
				    
				         	<tarjetaCredito>
	       		
						<tarjetaCredito>
							
				        	  <codigoProducto>200</codigoProducto>
			                  {
			                      for $subproducto in $contrato/subprodu
			                      return
			                      	<codigoSubProducto>200-0-0007</codigoSubProducto>
			                  }
			                  {
			                      for $pan in $contrato/pan
			                      return
			                      	<identificadorProducto>{ data($pan) }</identificadorProducto>
			                  }
			         	</tarjetaCredito>
						{
							let $indicadorOperatividad :=  
								if(fn:string-length(xs:string(fn:data($contrato/indblqope)))>0) then 
									xs:string(fn:data($contrato/indblqope)) 
								else 
									'N'
			                let $codigoBloqueo := xs:string(fn:data($contrato/codblqtar))
			                return
			                  
			                local:homologacionCuentaOmni($indicadorOperatividad,$codigoBloqueo)
						}
						<moneda>
		                {
			                if ($contrato/clamon1)
			                then <codigoMoneda>{fn:data($contrato/clamon1)}</codigoMoneda>
			                else ()
			            }
		                </moneda>
					<plastico>
				            <titularidad>
	                        { 
	                        	if( fn:data($contrato/calpartsal) = "TI") then
	                        	("Titular")
	                        	else
	                        	("Adicional") 
	                        }
	                        </titularidad>
	                             {
                  let $var := 0
                  let $codigoBloqueo := xs:string(fn:data($contrato/codblqtar))
                  let $codigoSituacion := xs:string(fn:data($contrato/indsittar))
                  let $codigoBaja := xs:string(fn:data($contrato/motbajatar))
                  
                  let $glosaBloqueo := xs:string(fn:data($contrato/desblqtar))
                  let $glosaBaja := xs:string(fn:data($contrato/desmottar))
                  let $glosaSituacion := xs:string(fn:data($contrato/dessittar))
                  return
                  
                   local:homologacionTarjeta(
                              $codigoBloqueo, 
                              $codigoBaja, 
                              $codigoSituacion,
                              $glosaBloqueo,
                              $glosaBaja,
                              $glosaSituacion) 
                 
              }
         
	                     
					       	
		                </plastico>
						<cuentaTarjetaCredito>
		                    <identificador>
		                        {
		                        	let $centroAlta := data($contrato/centaltasal)
		                        	let $entidad := data($contrato/codentsal)
		                        	let $cuenta := data($contrato/cuentasal)
		                        	return
		                            fn:concat($centroAlta,$entidad,$cuenta)
		                        }
							</identificador>
		                </cuentaTarjetaCredito>
					
						<superAvance>
						{
							for $dislin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:dislin
			                return
								<cupoDisponible>{data($dislin03)}</cupoDisponible>
		                }
		                {
		                	for $limcrelin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:limcrelin
			                return
		                    	<cupoTotal>{data($limcrelin03)}</cupoTotal>
		                }
		                {
		                	for $limcrelin03 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0003']/ns1:saldeulin
			                return
		                    	<cupoUtilizado>{data($limcrelin03)}</cupoUtilizado>
						}
						</superAvance>
						
				     </tarjetaCredito>	
				     
				     
				     )
					)
					else()
					}
					(: Filtro línea de crédito calpartsal)!="BE":)
					{
						if(fn:data($contrato/calpartsal)!="BE")then
						(

							(: Filtro para mostrar solo productos activos y comparacion para saldos en 0.0 | tipoLin 0004 corresponde a Linea Compra Deuda (Linea de credito MDW):)
		        			if(data($satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:retorno) = "000"  and 
		        				xs:decimal(data($satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0004']/ns1:dislin)) != 0.0) then
		        			(
								<lineaCredito>	
										  	<lineaCredito>
												 <codigoProducto>700</codigoProducto>
												 <codigoSubProducto>700-0-1</codigoSubProducto>
								                 <identificadorProducto>{ data($contrato/pan) }</identificadorProducto>
											</lineaCredito>
											<situacion>
									             <estado>01</estado>
									             <glosa>Informativo</glosa>
									        </situacion>
											<moneda>
								            <codigoMoneda>
							               	{
								                if ($contrato/clamon1) then 
								                	<codigoMoneda>{fn:data($contrato/clamon1)}</codigoMoneda>
								                else ()
							            	}
							            	</codigoMoneda>
							             	</moneda>
							            		(: FLCLDIS - Saldos linea compra deuda [tipolin = 0004]. corresponde a linea de crédito :)
								            <cupo>
								            {
								            	for $dislin04 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0004']/ns1:dislin
								                return
													<cupoDisponible>{data($dislin04)}</cupoDisponible>
							                }
							                {
							                	for $limcrelin04 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0004']/ns1:limcrelin
								                return
							                    	<cupoTotal>{data($limcrelin04)}</cupoTotal>
							                }
							                {
							                	for $saldeulin04 in $satFLCLDISConsultaSaldosImplResp1/ns1:runServiceResponse/ns1:return/ns1:registros_FLCLDIS[ns1:tipolin = '0004']/ns1:saldeulin
								                return
							                    <cupoUtilizado>{data($saldeulin04)}</cupoUtilizado>
											}
											</cupo>	
								</lineaCredito>			
								) 
								else ()
								)
						else
						()
					 }
    </ns2:clienteResumenProductosObtenerCompResp>
};


declare function local:homologacionCuentaOmni($indicadorOperatividad as xs:string,
                                       $codigoBloqueo as xs:string) as element(*){
                            
    let $indicador := fn:upper-case(fn:data($indicadorOperatividad))
    return
    <situacion>
    {
	    if(xs:int(fn:data($codigoBloqueo)) = 0 and fn:data($indicadorOperatividad)="N")  then(
	    <estado>00</estado>,
	      <glosa>Activa</glosa>
	   	)else(
	     
	         <estado>02</estado>,
	      <glosa>Basico</glosa>
	    )   
    }</situacion>
};

declare function local:homologacionTarjeta(
                            $codigoBloqueo as xs:string, 
                            $codigoBaja as xs:string, 
                            $codigoSituacion as xs:string,
                            $glosaBloqueo as xs:string,
                            $glosaBaja as xs:string,
                            $glosaSituacion as xs:string) 
                            as element(*){
      <situacion>{
        if($codigoBloqueo = '0' and $codigoBaja = '' and $codigoSituacion = '5')then(
          <codigo>{$codigoSituacion}</codigo>,
          <estado>0</estado>,
          <glosa>{$glosaSituacion}</glosa>
        )else if($codigoBloqueo = '0' and $codigoSituacion != '5' and $codigoBaja = '')then(
          <codigo>{$codigoSituacion}</codigo>,
          <estado>02</estado>,
          <glosa>{$glosaSituacion}</glosa>
        )else if($codigoBloqueo != '0' and $codigoBaja = '')then(
          <codigo>{$codigoBloqueo}</codigo>,
          <estado>03</estado>,
          <glosa>{$glosaBloqueo}</glosa>
        )else if($codigoBaja != '')then(
          <codigo>{$codigoBaja}</codigo>,
          <estado>04</estado>,
          <glosa>{$glosaBaja}</glosa>
        )else
          ()
      }</situacion>
};

local:xq_OUT_BS_SJ_SAT_FLCLDIS_ConsultaSaldos_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp2($runServiceResponse)
