xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLDIS/ConsultaSaldos/Req/IMPL/v2017.10";
(:: import schema at "../../../FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_IMPL_Req.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)
declare namespace ns3="http://mdwcorp.falabella.com/common/schema/clientservice";
(:: import schema at "../../../UT_EsquemasComunes_V.2.0/Specifications/XSD/Common/MdwCorp_Common_clientService.xsd" ::)



declare variable $clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;
declare variable $clientService as element(ns3:ClientService) external;
declare variable $contrato as element(*) external;


declare function local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_Impl($clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
      <msgEnvio>
                    <autoPaginable>false</autoPaginable>
                    <claveFin>000005</claveFin>
                    <claveInicio>000001</claveInicio>
                    {
                        for $codentsal in $contrato/codentsal
                        return
                            <entidad>{ data($codentsal) }</entidad>
                    }
                    <tipoOperacion>VIEW</tipoOperacion>
                    <usuario>{ concat($clientService/ns3:country ,";", $clientService/ns3:commerce ,";", $clientService/ns3:channel) }</usuario>
                    {
                        for $centaltasal in $contrato/centaltasal
                        return
                            <centalta>{ data($centaltasal) }</centalta>
                    }
                    {
                        for $cuentasal in $contrato/cuentasal
                        return
                            <cuenta>{ data($cuentasal) }</cuenta>
                    }
                </msgEnvio>
    </ns2:runService>
};

local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_FIF_CORP_SAT_FLCLDIS_ConsultaSaldos_Impl($clienteResumenProductosObtenerCompReq)
