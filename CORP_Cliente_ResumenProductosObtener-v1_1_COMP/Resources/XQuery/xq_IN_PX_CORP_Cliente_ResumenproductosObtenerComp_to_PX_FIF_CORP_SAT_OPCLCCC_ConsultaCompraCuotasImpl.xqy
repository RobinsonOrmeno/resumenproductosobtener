xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCLCCC/ConsultaCompraCuotas/Req/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_IMPL_Req.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare variable $clientService1 as element(ns1:ClientService) external;
declare variable $contrato as element(*) external;
declare function local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotasImpl($clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::),$clientService1 , $contrato) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
    <autoPaginable>false</autoPaginable>
            <avanzar>false</avanzar>
            <claveFin>0</claveFin>
            <claveInicio>0</claveInicio>
            {
                for $codentsal in $contrato/codentsal
                return
                    <entidad>{ data($codentsal) }</entidad>
            }
            <idioma>ES</idioma>
            <indMasDatos>false</indMasDatos>
            {
                for $centaltasal in $contrato/centaltasal
                return
                    <oficina>{ data($centaltasal) }</oficina>
            }
            <pantPagina>0</pantPagina>
            <password>Falabella08</password>
            <retroceder>false</retroceder>
            <tipoOperacion>VIEW</tipoOperacion>
            <usuario>{ concat($clientService1/ns1:country ,";", $clientService1/ns1:commerce ,";", $clientService1/ns1:channel) }</usuario>
            {
                for $centaltasal in $contrato/centaltasal
                return
                    <centalta_ent>{ data($centaltasal) }</centalta_ent>
            }
            {
                for $codentsal in $contrato/codentsal
                return
                    <codent_ent>{ data($codentsal) }</codent_ent>
            }
            {
                for $cuentasal in $contrato/cuentasal
                return
                    <cuenta_ent>{ data($cuentasal) }</cuenta_ent>
            }
            <limporpag>20</limporpag>
            <tipo_de_linea>0003</tipo_de_linea>
    
    </ns2:runService>
};

local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotasImpl($clienteResumenProductosObtenerCompReq, $clientService1, $contrato)
