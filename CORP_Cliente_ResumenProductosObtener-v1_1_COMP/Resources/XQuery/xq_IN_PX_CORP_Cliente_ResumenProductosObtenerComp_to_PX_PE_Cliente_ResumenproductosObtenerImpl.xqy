xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Pif/cliente/resumenproductos/obtener/Req-v2015.12";
(:: import schema at "../../../BCO_PE_PIF_Cliente_ResumenproductosObtener-v1_0_IMPL/Resources/Schemas/OSB_Cliente_Resumenproductos_ObtenerReq.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare function local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_PE_Cliente_ResumenproductosObtenerImpl($clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:ClienteResumenproductosObtenerReqParam) ::) {
    <ns2:ClienteResumenproductosObtenerReqParam>
         <ns2:documentoIdentidad>
                <ns2:tipoDocumento>{ data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento) }</ns2:tipoDocumento>
                <ns2:numeroDocumento>{ data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento) }</ns2:numeroDocumento>
            </ns2:documentoIdentidad>
    </ns2:ClienteResumenproductosObtenerReqParam>
};

local:xq_IN_PX_CORP_Cliente_ResumenProductosObtenerComp_to_PX_PE_Cliente_ResumenproductosObtenerImpl($clienteResumenProductosObtenerCompReq1)
