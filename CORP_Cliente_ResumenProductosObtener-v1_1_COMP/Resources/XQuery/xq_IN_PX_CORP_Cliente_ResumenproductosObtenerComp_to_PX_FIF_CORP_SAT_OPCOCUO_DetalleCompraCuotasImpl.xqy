xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCOCUO/DetalleCompraCuotas/Req/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_IMPL_Req.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)
declare namespace ns3="http://mdwcorp.falabella.com/common/schema/clientservice";
(:: import schema at "../../../UT_EsquemasComunes_V.2.0/Specifications/XSD/Common/MdwCorp_Common_clientService.xsd" ::)


declare variable $clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;
declare variable $clientService1 as element(ns3:ClientService) external;
declare variable $contrato as element(*) external;
declare variable $numfinan as xs:string external;
declare variable $numopecuo as xs:string external;
declare variable $clamon as xs:string external;

declare function local:func($clienteResumenProductosObtenerCompReq as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
     <autoPaginable>false</autoPaginable>
            <avanzar>false</avanzar>
            <claveFin>0</claveFin>
            <claveInicio>0</claveInicio>
            {
                for $codentsal in $contrato/codentsal
                return
                    <entidad>{ data($codentsal) }</entidad>
            }
            <pantPagina>3</pantPagina>
            <password>Falabella08</password>
            <retroceder>false</retroceder>
            <tipoOperacion>VIEW</tipoOperacion>
            <usuario>{ concat($clientService1/ns3:country ,";", $clientService1/ns3:commerce ,";", $clientService1/ns3:channel) }</usuario>
            {
                for $centaltasal in $contrato/centaltasal
                return
                    <centalta>{ data($centaltasal) }</centalta>
            }
            <clamon>{ xs:int($clamon) }</clamon>
            {
                for $cuentasal in $contrato/cuentasal
                return
                    <cuenta>{ data($cuentasal) }</cuenta>
            }
            <numfinan>{ xs:int($numfinan) }</numfinan>
            <numopecuo>{ xs:int($numopecuo) }</numopecuo>
    </ns2:runService>
};

local:func($clienteResumenProductosObtenerCompReq)
