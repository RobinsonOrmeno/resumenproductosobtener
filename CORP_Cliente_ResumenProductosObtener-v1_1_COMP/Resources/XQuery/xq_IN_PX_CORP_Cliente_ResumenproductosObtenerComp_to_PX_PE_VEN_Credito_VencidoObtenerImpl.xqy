xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ven/credito/vencido/obtener/Req-v2016.02";
(:: import schema at "../../../BCO_PE_VEN_Credito_VencidoObtener-v1_0_IMPL/Resources/Schemas/OSB_VEN_Credito_Vencido_ObtenerReq.xsd" ::)
declare namespace ns1="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Req-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerReq.xsd" ::)

declare variable $clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::) external;

declare function local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_PE_VEN_Credito_VencidoObtenerImpl($clienteResumenProductosObtenerCompReq1 as element() (:: schema-element(ns1:clienteResumenProductosObtenerCompReq) ::)) as element() (:: schema-element(ns2:creditoVencidoObtenerReqParam) ::) {
    <ns2:creditoVencidoObtenerReqParam>
        <ns2:tipoDocumento>{fn:data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/tipoDocumento)}</ns2:tipoDocumento>
        <ns2:numeroDocumento>{fn:data($clienteResumenProductosObtenerCompReq1/documentoIdentidad/numeroDocumento)}</ns2:numeroDocumento></ns2:creditoVencidoObtenerReqParam>
};

local:xq_IN_PX_CORP_Cliente_ResumenproductosObtenerComp_to_PX_PE_VEN_Credito_VencidoObtenerImpl($clienteResumenProductosObtenerCompReq1)
