xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCLCCC/ConsultaCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCOCUO/DetalleCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../../../FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_v1_0_IMPL/Resources/Schemas/OSB_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns3="http://mdwcorp.falabella.com/OSB/schema/FIF/CORP/cliente/resumenProductos/obtener/Resp-v2015.05";
(:: import schema at "../Schemas/OSB_Cliente_ResumenProductos_ObtenerResp.xsd" ::)
declare namespace ns4="http://www.example.org/OSB_Cliente_ListaCreditoEfectivo";
(:: import schema at "../Schemas/OSB_Cliente_ListaCreditoEfectivo.xsd" ::)

declare namespace functx = "http://www.functx.com";

declare variable $runServiceResponseCCC as element() (:: schema-element(ns1:runServiceResponse) ::) external;
declare variable $runServiceResponseCUO as element() (:: schema-element(ns2:runServiceResponse) ::) external;
declare variable $listaCE1 as element(ns4:listaCE) external;
declare variable $clienteResumenProductosObtenerCompResp1 as element(ns3:clienteResumenProductosObtenerCompResp) external;

declare function local:xq_OUT_BS_SJ_SAT_CE_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($runServiceResponseCCC as element() (:: schema-element(ns1:runServiceResponse) ::), 
                                                                                               $runServiceResponseCUO as element() (:: schema-element(ns2:runServiceResponse) ::)) 
                                                                                               as element() (:: schema-element(ns3:clienteResumenProductosObtenerCompResp) ::) {
    <ns3:clienteResumenProductosObtenerCompResp>
     {
                for $tarjetaCredito in $clienteResumenProductosObtenerCompResp1/tarjetaCredito
                return
                    <tarjetaCredito>{ $tarjetaCredito/@* , $tarjetaCredito/node() }</tarjetaCredito>
            }
			<credito>
                {
                if (fn:empty($listaCE1/credito)) then
		        ()
		        else(
                    for $credito in $listaCE1/credito
                    return
                    <creditoConsumo>
                        <creditoConsumo>
                     
                            <codigoProducto>400</codigoProducto>)
                           
                            { if(data($credito/contrato)="01")then(
                            <codigoSubProducto>400-3-1</codigoSubProducto>)
                            else(<codigoSubProducto>400-3-2</codigoSubProducto>)}
                            {
                                for $centalta in $credito/centalta,
                                    $codent in $credito/codent,
                                    $cuenta in $credito/cuenta,
                                	$numopecuo in $credito/numopecuo,
                                    $numfinan in $credito/numfinan
                                return
                                
                              
                            <identificadorProducto>{
                            	concat(
                            		functx:pad-integer-to-length(data($centalta),4) 
                                  , functx:pad-integer-to-length(data($codent),4)
                                  , functx:pad-integer-to-length(data($cuenta),12) ,"-"
                                  , fn-bea:pad-left(xs:string(data($numopecuo)),6,"-")
                                  ,fn-bea:pad-left(xs:string(data($numfinan)),3,"-")
                            	)
                            }</identificadorProducto>
                            }
                        </creditoConsumo>
                        <moneda>
	                        {
	                            for $clamon in $credito/clamon
	                            return
	                                <codigoMoneda>{ data($clamon) }</codigoMoneda>
	                        }
	                    </moneda>
	                    <situacion>
	                        {
	                        	(: Solo se muestran cuotas con estado 01:Vigente Revisar xq OUT_PX_BCO_PE_SAT_OPCLCUOWS :)
	                            for $estcompra in $credito/estcompra
	                            return
	                                <codigo>{ data($estcompra) }</codigo>
	                        }
	                        <estado>00</estado>
	                        <glosa>Activo</glosa>
	                    </situacion>
	                    <montoCredito>
	                        {
	                            for $impope in $credito/impope
	                            return
	                                <montoSolicitado>{ data($impope) }</montoSolicitado>
	                        }
	                    </montoCredito>
	                    <cuota>
	                        {
	                            for $numcuot in $credito/numcuot
	                            return
	                                <numeroCuotasPagadas>{ data($numcuot) }</numeroCuotasPagadas>
	                        }
	                        {
	                            for $numcuot in $credito/numcuot,
	                                $numcuotpen in $credito/numcuotpen
	                            return
	                                <totalCuota>{ xs:int(data($numcuot) + data($numcuotpen)) }</totalCuota>
	                        }
	                        {
	                        	(: Si cuota Pendiente, se muestra ; Revisar xq OUT_PX_BCO_PE_SAT_OPCLCUOWS :)
	                        	if(data($credito/estcuo) = "01") then
	                            for $fecprocuo in $credito/fecprocuo
	                            return
	                                <fechaVencimientoProximaCuota>
	                                    {
	                                        fn:concat(
	                                        fn:substring($credito/fecprocuo,7,4),'-',
	                                        fn:substring($credito/fecprocuo,4,2),'-',
	                                        fn:substring($credito/fecprocuo,1,2)
	                                        )
	                                    }
									</fechaVencimientoProximaCuota>
								else ()
	                        }
	                        {
	                        	(: Si cuota Pendiente, se muestra ; Revisar xq OUT_PX_BCO_PE_SAT_OPCLCUOWS :)
	                        	if(data($credito/estcuo) = "01") then
		                            for $impcuotapp in $credito/impcuotapp
		                            return
		                                <montoProximaCuota>{ data($impcuotapp) }</montoProximaCuota>
	                            else 
	                            	<montoProximaCuota>0.0</montoProximaCuota>
	                        }
	                        {
	                            for $numcuot in $credito/numcuot,
	                                $numcuotpen in $credito/numcuotpen
	                            return
	                                <numeroProximaCuota>
	                                {
	                                	if( data($numcuot) + 1 <= (data($numcuot) + data($numcuotpen)) ) then
	                                		xs:int(data($numcuot) + 1)
	                                	else 
	                                		0
	                                }
	                                </numeroProximaCuota>
	                        }
	                    </cuota>
                    </creditoConsumo>
                )
                }
            </credito>
            {
                for $lineaCredito in $clienteResumenProductosObtenerCompResp1/lineaCredito
                return
                    <lineaCredito>{ $lineaCredito/@* , $lineaCredito/node() }</lineaCredito>
            }
            
            
    
    </ns3:clienteResumenProductosObtenerCompResp>
};

declare function functx:repeat-string
  ( $stringToRepeat as xs:string? ,
    $count as xs:integer )  as xs:string {

   string-join((for $i in 1 to $count return $stringToRepeat),
                        '')
 } ;
declare function functx:pad-integer-to-length
  ( $integerToPad as xs:string ,
    $length as xs:integer )  as xs:string {

   if ($length < string-length(string($integerToPad)))
   then error(xs:QName('functx:Integer_Longer_Than_Length'))
   else concat
         (functx:repeat-string(
            '0',$length - string-length(string($integerToPad))),
          string($integerToPad))
 } ;


local:xq_OUT_BS_SJ_SAT_CE_Impl_to_PX_CORP_Cliente_ResumenproductosObtenerComp($runServiceResponseCCC, $runServiceResponseCUO)
