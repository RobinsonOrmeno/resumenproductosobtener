xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLCCT/ConsultaClienteContratos/Req/IMPL/v2017.10";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_IMPL_Req.xsd" ::)
declare namespace ns2="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_FLCLCCT.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare variable $runService as element() (:: schema-element(ns1:runService) ::) external;

declare function local:func($runService as element() (:: schema-element(ns1:runService) ::)) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
        {
            if ($runService/msgEnvio)
            then <ns2:msgEnvio>
                {
                    if ($runService/msgEnvio/autoPaginable)
                    then <ns3:autoPaginable>{fn:data($runService/msgEnvio/autoPaginable)}</ns3:autoPaginable>
                    else ()
                }
                {
                    if ($runService/msgEnvio/claveFin)
                    then <ns3:claveFin>{fn:data($runService/msgEnvio/claveFin)}</ns3:claveFin>
                    else ()
                }
                {
                    if ($runService/msgEnvio/claveInicio)
                    then <ns3:claveInicio>{fn:data($runService/msgEnvio/claveInicio)}</ns3:claveInicio>
                    else ()
                }
                {
                    if ($runService/msgEnvio/entidad)
                    then <ns3:entidad>{fn:data($runService/msgEnvio/entidad)}</ns3:entidad>
                    else ()
                }
                {
                    if ($runService/msgEnvio/idioma)
                    then <ns3:idioma>{fn:data($runService/msgEnvio/idioma)}</ns3:idioma>
                    else ()
                }
                {
                    if ($runService/msgEnvio/indMasDatos)
                    then <ns3:indMasDatos>{fn:data($runService/msgEnvio/indMasDatos)}</ns3:indMasDatos>
                    else ()
                }
                {
                    if ($runService/msgEnvio/oficina)
                    then <ns3:oficina>{fn:data($runService/msgEnvio/oficina)}</ns3:oficina>
                    else ()
                }
                {
                    if ($runService/msgEnvio/pantPagina)
                    then <ns3:pantPagina>{fn:data($runService/msgEnvio/pantPagina)}</ns3:pantPagina>
                    else ()
                }
                {
                    if ($runService/msgEnvio/password)
                    then <ns3:password>{fn:data($runService/msgEnvio/password)}</ns3:password>
                    else ()
                }
                {
                    if ($runService/msgEnvio/tipoOperacion)
                    then <ns3:tipoOperacion>{fn:data($runService/msgEnvio/tipoOperacion)}</ns3:tipoOperacion>
                    else ()
                }
                {
                    if ($runService/msgEnvio/usuario)
                    then <ns3:usuario>{fn:data($runService/msgEnvio/usuario)}</ns3:usuario>
                    else ()
                }
                {
                    if ($runService/msgEnvio/calpart)
                    then <ns4:calpart>{fn:data($runService/msgEnvio/calpart)}</ns4:calpart>
                    else ()
                }
                {
                    if ($runService/msgEnvio/centalta)
                    then <ns4:centalta>{fn:data($runService/msgEnvio/centalta)}</ns4:centalta>
                    else ()
                }
                {
                    if ($runService/msgEnvio/cuenta)
                    then <ns4:cuenta>{fn:data($runService/msgEnvio/cuenta)}</ns4:cuenta>
                    else ()
                }
                {
                    if ($runService/msgEnvio/identcli)
                    then <ns4:identcli>{fn:data($runService/msgEnvio/identcli)}</ns4:identcli>
                    else ()
                }
                {
                    if ($runService/msgEnvio/indcontrato)
                    then <ns4:indcontrato>{fn:data($runService/msgEnvio/indcontrato)}</ns4:indcontrato>
                    else ()
                }
                {
                    if ($runService/msgEnvio/numdoc)
                    then <ns4:numdoc>{fn:data($runService/msgEnvio/numdoc)}</ns4:numdoc>
                    else ()
                }
                {
                    if ($runService/msgEnvio/tipdoc)
                    then <ns4:tipdoc>{fn:data($runService/msgEnvio/tipdoc)}</ns4:tipdoc>
                    else ()
                }</ns2:msgEnvio>
            else ()
        }</ns2:runService>
};

local:func($runService)
