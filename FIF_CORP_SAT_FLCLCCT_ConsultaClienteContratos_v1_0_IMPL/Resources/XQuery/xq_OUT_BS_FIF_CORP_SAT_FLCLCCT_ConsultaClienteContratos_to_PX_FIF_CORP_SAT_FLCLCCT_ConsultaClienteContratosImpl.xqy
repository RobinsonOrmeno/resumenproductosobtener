xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace my="http://www.w3.org/2005/xquery-local-functions";
import module "http://www.w3.org/2005/xquery-local-functions" at "../../../FIF_CORP_Comun_V1_0/MDW/XQUERY/xq_utils.xqy";

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/FLCLCCT/ConsultaClienteContratos/Resp/IMPL/v2017.10";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_FLCLCCT_ConsultaClienteContratos_IMPL_Resp.xsd" ::)
declare namespace ns1="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_FLCLCCT.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare namespace ns5 = "http://commons.soapwebservices.ease/xsd";

declare variable $runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::) external;

declare function local:func($runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::)) as element() (:: schema-element(ns2:runServiceResponse) ::) {
    <ns2:runServiceResponse>
        {
            if ($runServiceResponse/ns1:return)
            then <return>
                {
                    if ($runServiceResponse/ns1:return/ns3:claveFin)
                    then <claveFin>{fn:data($runServiceResponse/ns1:return/ns3:claveFin)}</claveFin>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:claveInicio)
                    then <claveInicio>{fn:data($runServiceResponse/ns1:return/ns3:claveInicio)}</claveInicio>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:descRetorno)
                    then <descRetorno>{fn:data($runServiceResponse/ns1:return/ns3:descRetorno)}</descRetorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:indMasDatos)
                    then <indMasDatos>{fn:data($runServiceResponse/ns1:return/ns3:indMasDatos)}</indMasDatos>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:pantPagina)
                    then <pantPagina>{fn:data($runServiceResponse/ns1:return/ns3:pantPagina)}</pantPagina>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:retorno)
                    then <retorno>{fn:data($runServiceResponse/ns1:return/ns3:retorno)}</retorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:tiempoEjecucion)
                    then <tiempoEjecucion>{fn:data($runServiceResponse/ns1:return/ns3:tiempoEjecucion)}</tiempoEjecucion>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:totalRegistros)
                    then <totalRegistros>{fn:data($runServiceResponse/ns1:return/ns3:totalRegistros)}</totalRegistros>
                    else ()
                }
                {
                    for $registros_FLCLCCT in $runServiceResponse/ns1:return/ns4:registros_FLCLCCT
                    return 
                    <registros_FLCLCCT>
                        {
                            if ($registros_FLCLCCT/ns5:registro_Numero)
                            then <registro_Numero>{fn:data($registros_FLCLCCT/ns5:registro_Numero)}</registro_Numero>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:apelli1)
                            then <apelli1>{fn:data($registros_FLCLCCT/ns4:apelli1)}</apelli1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:apelli2)
                            then <apelli2>{fn:data($registros_FLCLCCT/ns4:apelli2)}</apelli2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:bin)
                            then <bin>{fn:data($registros_FLCLCCT/ns4:bin)}</bin>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:calpartsal)
                            then <calpartsal>{fn:data($registros_FLCLCCT/ns4:calpartsal)}</calpartsal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:centaltasal)
                            then <centaltasal>{fn:data($registros_FLCLCCT/ns4:centaltasal)}</centaltasal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamon1)
                            then <clamon1>{fn:data($registros_FLCLCCT/ns4:clamon1)}</clamon1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamon2)
                            then <clamon2>{fn:data($registros_FLCLCCT/ns4:clamon2)}</clamon2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamonp)
                            then <clamonp>{fn:data($registros_FLCLCCT/ns4:clamonp)}</clamonp>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamons)
                            then <clamons>{fn:data($registros_FLCLCCT/ns4:clamons)}</clamons>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamontar1)
                            then <clamontar1>{fn:data($registros_FLCLCCT/ns4:clamontar1)}</clamontar1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:clamontar2)
                            then <clamontar2>{fn:data($registros_FLCLCCT/ns4:clamontar2)}</clamontar2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:codblq)
                            then <codblq>{fn:data($registros_FLCLCCT/ns4:codblq)}</codblq>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:codblqtar)
                            then <codblqtar>{fn:data($registros_FLCLCCT/ns4:codblqtar)}</codblqtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:codentsal)
                            then <codentsal>{fn:data($registros_FLCLCCT/ns4:codentsal)}</codentsal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:codestcta)
                            then <codestcta>{fn:data($registros_FLCLCCT/ns4:codestcta)}</codestcta>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:codmar)
                            then <codmar>{fn:data($registros_FLCLCCT/ns4:codmar)}</codmar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:conprod)
                            then <conprod>{fn:data($registros_FLCLCCT/ns4:conprod)}</conprod>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:cuentasal)
                            then <cuentasal>{fn:data($registros_FLCLCCT/ns4:cuentasal)}</cuentasal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desblq)
                            then <desblq>{fn:data($registros_FLCLCCT/ns4:desblq)}</desblq>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desblqred)
                            then <desblqred>{fn:data($registros_FLCLCCT/ns4:desblqred)}</desblqred>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desblqredtar)
                            then <desblqredtar>{fn:data($registros_FLCLCCT/ns4:desblqredtar)}</desblqredtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desblqtar)
                            then <desblqtar>{fn:data($registros_FLCLCCT/ns4:desblqtar)}</desblqtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:descon)
                            then <descon>{fn:data($registros_FLCLCCT/ns4:descon)}</descon>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desestctared)
                            then <desestctared>{fn:data($registros_FLCLCCT/ns4:desestctared)}</desestctared>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desmot)
                            then <desmot>{fn:data($registros_FLCLCCT/ns4:desmot)}</desmot>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desmottar)
                            then <desmottar>{fn:data($registros_FLCLCCT/ns4:desmottar)}</desmottar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desprod)
                            then <desprod>{fn:data($registros_FLCLCCT/ns4:desprod)}</desprod>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:desprodred)
                            then <desprodred>{fn:data($registros_FLCLCCT/ns4:desprodred)}</desprodred>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:dessittar)
                                then <dessittar>{fn:data($registros_FLCLCCT/ns4:dessittar)}</dessittar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:destipred)
                            then <destipred>{fn:data($registros_FLCLCCT/ns4:destipred)}</destipred>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:diasmora1)
                            then <diasmora1>{fn:data($registros_FLCLCCT/ns4:diasmora1)}</diasmora1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:diasmora2)
                            then <diasmora2>{fn:data($registros_FLCLCCT/ns4:diasmora2)}</diasmora2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecacuser)
                            then <fecacuser>
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecacuser/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecacuser/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecacuser>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecalta)
                            then <fecalta>
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecalta/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecalta/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecalta>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecaltatar)
                            then <fecaltatar>
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecaltatar/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecaltatar/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecaltatar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecbaja)
                            then <fecbaja>
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbaja/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecbaja/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecbaja>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecbajatar)
                            then <fecbajatar>
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecbajatar/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecbajatar/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecbajatar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:feccadtar)
                            then <feccadtar>{fn:data($registros_FLCLCCT/ns4:feccadtar)}</feccadtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecfacact)
                            then <fecfacact>
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacact/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecfacact/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecfacact>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecfacant)
                            then <fecfacant>
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacant/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecfacant/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecfacant>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecfacpro)
                            then <fecfacpro>
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecfacpro/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecfacpro/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecfacpro>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecultblq)
                            then <fecultblq>
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblq/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecultblq/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecultblq>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecultblqtar)
                            then <fecultblqtar>
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecultblqtar/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecultblqtar/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecultblqtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecvtoact)
                            then <fecvtoact>
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoact/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecvtoact/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecvtoact>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecvtoant)
                            then <fecvtoant>
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtoant/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecvtoant/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecvtoant>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:fecvtopro)
                            then <fecvtopro>
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:anno)
                                    then <anno>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:dia)
                                    then <dia>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:format)
                                    then <format>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:mes)
                                    then <mes>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_FLCLCCT/ns4:fecvtopro/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_FLCLCCT/ns4:fecvtopro/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecvtopro>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:forpago)
                            then <forpago>{fn:data($registros_FLCLCCT/ns4:forpago)}</forpago>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:grupoliq)
                            then <grupoliq>{fn:data($registros_FLCLCCT/ns4:grupoliq)}</grupoliq>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:horblqtar)
                            then <horblqtar>{fn:data($registros_FLCLCCT/ns4:horblqtar)}</horblqtar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:identclisal)
                            then <identclisal>{fn:data($registros_FLCLCCT/ns4:identclisal)}</identclisal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impaplfac1)
                            then <impaplfac1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impaplfac1)))}</impaplfac1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impaplfac2)
                            then <impaplfac2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impaplfac2)))}</impaplfac2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impimpago1)
                            then <impimpago1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impimpago1)))}</impimpago1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impimpago2)
                            then <impimpago2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impimpago2)))}</impimpago2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impinfac1)
                            then <impinfac1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impinfac1)))}</impinfac1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:impinfac2)
                            then <impinfac2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:impinfac2)))}</impinfac2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:imptotfac1)
                            then <imptotfac1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:imptotfac1)))}</imptotfac1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:imptotfac2)
                            then <imptotfac2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:imptotfac2)))}</imptotfac2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:indblqope)
                            then <indblqope>{fn:data($registros_FLCLCCT/ns4:indblqope)}</indblqope>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:indblqopetar)
                            then <indblqopetar>{fn:data($registros_FLCLCCT/ns4:indblqopetar)}</indblqopetar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:indsittar)
                            then <indsittar>{fn:data($registros_FLCLCCT/ns4:indsittar)}</indsittar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:indtipt)
                            then <indtipt>{fn:data($registros_FLCLCCT/ns4:indtipt)}</indtipt>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:limcretar1)
                            then <limcretar1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:limcretar1)))}</limcretar1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:limcretar2)
                            then <limcretar2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:limcretar2)))}</limcretar2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:maxdiacaj1)
                            then <maxdiacaj1>{fn:data($registros_FLCLCCT/ns4:maxdiacaj1)}</maxdiacaj1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:maxdiacaj2)
                            then <maxdiacaj2>{fn:data($registros_FLCLCCT/ns4:maxdiacaj2)}</maxdiacaj2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:maxodia1)
                            then <maxodia1>{fn:data($registros_FLCLCCT/ns4:maxodia1)}</maxodia1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:maxodia2)
                            then <maxodia2>{fn:data($registros_FLCLCCT/ns4:maxodia2)}</maxodia2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:mesactual1)
                            then <mesactual1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:mesactual1)))}</mesactual1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:mesactual2)
                            then <mesactual2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:mesactual2)))}</mesactual2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:motbaja)
                            then <motbaja>{fn:data($registros_FLCLCCT/ns4:motbaja)}</motbaja>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:motbajatar)
                            then <motbajatar>{fn:data($registros_FLCLCCT/ns4:motbajatar)}</motbajatar>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:nombenred)
                            then <nombenred>{fn:data($registros_FLCLCCT/ns4:nombenred)}</nombenred>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:nombre)
                            then <nombre>{fn:data($registros_FLCLCCT/ns4:nombre)}</nombre>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:numbencta)
                            then <numbencta>{fn:data($registros_FLCLCCT/ns4:numbencta)}</numbencta>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:numdocsal)
                            then <numdocsal>{fn:data($registros_FLCLCCT/ns4:numdocsal)}</numdocsal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:numplastico)
                            then <numplastico>{fn:data($registros_FLCLCCT/ns4:numplastico)}</numplastico>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:numrecimp1)
                            then <numrecimp1>{fn:data($registros_FLCLCCT/ns4:numrecimp1)}</numrecimp1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:numrecimp2)
                            then <numrecimp2>{fn:data($registros_FLCLCCT/ns4:numrecimp2)}</numrecimp2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:pan)
                            then <pan>{fn:data($registros_FLCLCCT/ns4:pan)}</pan>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:panant)
                            then <panant>{fn:data($registros_FLCLCCT/ns4:panant)}</panant>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:porlim1)
                            then <porlim1>{fn:data($registros_FLCLCCT/ns4:porlim1)}</porlim1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:porlim2)
                            then <porlim2>{fn:data($registros_FLCLCCT/ns4:porlim2)}</porlim2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:producto)
                            then <producto>{fn:data($registros_FLCLCCT/ns4:producto)}</producto>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:sdopdte1)
                            then <sdopdte1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:sdopdte1)))}</sdopdte1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:sdopdte2)
                            then <sdopdte2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:sdopdte2)))}</sdopdte2>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:subprodu)
                            then <subprodu>{fn:data($registros_FLCLCCT/ns4:subprodu)}</subprodu>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:tipbon)
                            then <tipbon>{fn:data($registros_FLCLCCT/ns4:tipbon)}</tipbon>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:tipdocsal)
                            then <tipdocsal>{fn:data($registros_FLCLCCT/ns4:tipdocsal)}</tipdocsal>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:total1)
                            then <total1>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:total1)))}</total1>
                            else ()
                        }
                        {
                            if ($registros_FLCLCCT/ns4:total2)
                            then <total2>{my:funcionNotacion(xs:string(fn:data($registros_FLCLCCT/ns4:total2)))}</total2>
                            else ()
                        }</registros_FLCLCCT>
                }</return>
            else ()
        }</ns2:runServiceResponse>
};

local:func($runServiceResponse)
