xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCOCUO/DetalleCompraCuotas/Req/IMPL/v2017.12";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_IMPL_Req.xsd" ::)
declare namespace ns2="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_OPCOCUOWS.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare variable $runService as element() (:: schema-element(ns1:runService) ::) external;

declare function local:XQ_IN_PX_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl_TO_BS_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas($runService as element() (:: schema-element(ns1:runService) ::)) as element() (:: schema-element(ns2:runService) ::) {
    <ns2:runService>
        {
            if ($runService/msgEnvio)
            then <ns2:msgEnvio>
                {
                    if ($runService/msgEnvio/autoPaginable)
                    then <ns3:autoPaginable>{fn:data($runService/msgEnvio/autoPaginable)}</ns3:autoPaginable>
                    else ()
                }
                {
                    if ($runService/msgEnvio/avanzar)
                    then <ns3:avanzar>{fn:data($runService/msgEnvio/avanzar)}</ns3:avanzar>
                    else ()
                }
                {
                    if ($runService/msgEnvio/claveFin)
                    then <ns3:claveFin>{fn:data($runService/msgEnvio/claveFin)}</ns3:claveFin>
                    else ()
                }
                {
                    if ($runService/msgEnvio/claveInicio)
                    then <ns3:claveInicio>{fn:data($runService/msgEnvio/claveInicio)}</ns3:claveInicio>
                    else ()
                }
                {
                    if ($runService/msgEnvio/entidad)
                    then <ns3:entidad>{fn:data($runService/msgEnvio/entidad)}</ns3:entidad>
                    else ()
                }
                {
                    if ($runService/msgEnvio/idioma)
                    then <ns3:idioma>{fn:data($runService/msgEnvio/idioma)}</ns3:idioma>
                    else ()
                }
                {
                    if ($runService/msgEnvio/indMasDatos)
                    then <ns3:indMasDatos>{fn:data($runService/msgEnvio/indMasDatos)}</ns3:indMasDatos>
                    else ()
                }
                {
                    if ($runService/msgEnvio/oficina)
                    then <ns3:oficina>{fn:data($runService/msgEnvio/oficina)}</ns3:oficina>
                    else ()
                }
                {
                    if ($runService/msgEnvio/pantPagina)
                    then <ns3:pantPagina>{fn:data($runService/msgEnvio/pantPagina)}</ns3:pantPagina>
                    else ()
                }
                {
                    if ($runService/msgEnvio/password)
                    then <ns3:password>{fn:data($runService/msgEnvio/password)}</ns3:password>
                    else ()
                }
                {
                    if ($runService/msgEnvio/retroceder)
                    then <ns3:retroceder>{fn:data($runService/msgEnvio/retroceder)}</ns3:retroceder>
                    else ()
                }
                {
                    if ($runService/msgEnvio/tipoOperacion)
                    then <ns3:tipoOperacion>{fn:data($runService/msgEnvio/tipoOperacion)}</ns3:tipoOperacion>
                    else ()
                }
                {
                    if ($runService/msgEnvio/usuario)
                    then <ns3:usuario>{fn:data($runService/msgEnvio/usuario)}</ns3:usuario>
                    else ()
                }
                {
                    if ($runService/msgEnvio/centalta)
                    then <ns4:centalta>{fn:data($runService/msgEnvio/centalta)}</ns4:centalta>
                    else ()
                }
                {
                    if ($runService/msgEnvio/clamon)
                    then <ns4:clamon>{fn:data($runService/msgEnvio/clamon)}</ns4:clamon>
                    else ()
                }
                {
                    if ($runService/msgEnvio/cuenta)
                    then <ns4:cuenta>{fn:data($runService/msgEnvio/cuenta)}</ns4:cuenta>
                    else ()
                }
                {
                    if ($runService/msgEnvio/numfinan)
                    then <ns4:numfinan>{fn:data($runService/msgEnvio/numfinan)}</ns4:numfinan>
                    else ()
                }
                {
                    if ($runService/msgEnvio/numopecuo)
                    then <ns4:numopecuo>{fn:data($runService/msgEnvio/numopecuo)}</ns4:numopecuo>
                    else ()
                }
                {
                    if ($runService/msgEnvio/pan)
                    then <ns4:pan>{fn:data($runService/msgEnvio/pan)}</ns4:pan>
                    else ()
                }</ns2:msgEnvio>
            else ()
        }</ns2:runService>
};

local:XQ_IN_PX_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl_TO_BS_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas($runService)