xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCOCUO/DetalleCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns1="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_OPCOCUOWS.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare namespace ns5 = "http://commons.soapwebservices.ease/xsd";

declare variable $runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::) external;

declare function local:XQ_OUT_BS_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_TO_PX_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl($runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::)) as element() (:: schema-element(ns2:runServiceResponse) ::) {
    <ns2:runServiceResponse>
        {
            if ($runServiceResponse/ns1:return)
            then <return>
                {
                    if ($runServiceResponse/ns1:return/ns3:claveFin)
                    then <claveFin>{fn:data($runServiceResponse/ns1:return/ns3:claveFin)}</claveFin>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:claveInicio)
                    then <claveInicio>{fn:data($runServiceResponse/ns1:return/ns3:claveInicio)}</claveInicio>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:descRetorno)
                    then <descRetorno>{fn:data($runServiceResponse/ns1:return/ns3:descRetorno)}</descRetorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:indMasDatos)
                    then <indMasDatos>{fn:data($runServiceResponse/ns1:return/ns3:indMasDatos)}</indMasDatos>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:pantPagina)
                    then <pantPagina>{fn:data($runServiceResponse/ns1:return/ns3:pantPagina)}</pantPagina>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:retorno)
                    then <retorno>{fn:data($runServiceResponse/ns1:return/ns3:retorno)}</retorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:tiempoEjecucion)
                    then <tiempoEjecucion>{fn:data($runServiceResponse/ns1:return/ns3:tiempoEjecucion)}</tiempoEjecucion>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:totalRegistros)
                    then <totalRegistros>{fn:data($runServiceResponse/ns1:return/ns3:totalRegistros)}</totalRegistros>
                    else ()
                }
                {
                    for $registros_OPCOCUO in $runServiceResponse/ns1:return/ns4:registros_OPCOCUO
                    return 
                    <registros_OPCOCUO>
                        {
                            if ($registros_OPCOCUO/ns5:registro_Numero)
                            then <registro_Numero>{fn:data($registros_OPCOCUO/ns5:registro_Numero)}</registro_Numero>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:cappte)
                            then <cappte>{fn:data($registros_OPCOCUO/ns4:cappte)}</cappte>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:centalta)
                            then <centalta>{fn:data($registros_OPCOCUO/ns4:centalta)}</centalta>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:clamon)
                            then <clamon>{fn:data($registros_OPCOCUO/ns4:clamon)}</clamon>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:codcom)
                            then <codcom>{fn:data($registros_OPCOCUO/ns4:codcom)}</codcom>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:codent)
                            then <codent>{fn:data($registros_OPCOCUO/ns4:codent)}</codent>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:codtipc)
                            then <codtipc>{fn:data($registros_OPCOCUO/ns4:codtipc)}</codtipc>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:comamort)
                            then <comamort>{fn:data($registros_OPCOCUO/ns4:comamort)}</comamort>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:comapertura)
                            then <comapertura>{fn:data($registros_OPCOCUO/ns4:comapertura)}</comapertura>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:comcancel)
                            then <comcancel>{fn:data($registros_OPCOCUO/ns4:comcancel)}</comcancel>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:contcur)
                            then <contcur>{fn:data($registros_OPCOCUO/ns4:contcur)}</contcur>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:cuenta)
                            then <cuenta>{fn:data($registros_OPCOCUO/ns4:cuenta)}</cuenta>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:cuotaspte)
                            then <cuotaspte>{fn:data($registros_OPCOCUO/ns4:cuotaspte)}</cuotaspte>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:desclamon)
                            then <desclamon>{fn:data($registros_OPCOCUO/ns4:desclamon)}</desclamon>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:desestcom)
                            then <desestcom>{fn:data($registros_OPCOCUO/ns4:desestcom)}</desestcom>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:desmotbaja)
                            then <desmotbaja>{fn:data($registros_OPCOCUO/ns4:desmotbaja)}</desmotbaja>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:destipcred)
                            then <destipcred>{fn:data($registros_OPCOCUO/ns4:destipcred)}</destipcred>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:destipolin)
                            then <destipolin>{fn:data($registros_OPCOCUO/ns4:destipolin)}</destipolin>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:estcompra)
                            then <estcompra>{fn:data($registros_OPCOCUO/ns4:estcompra)}</estcompra>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:fecaltad)
                            then <fecaltad>
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:anno)
                                    then <anno>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:dia)
                                    then <dia>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:format)
                                    then <format>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:mes)
                                    then <mes>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecaltad/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_OPCOCUO/ns4:fecaltad/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecaltad>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:fecbajad)
                            then <fecbajad>
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:anno)
                                    then <anno>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:dia)
                                    then <dia>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:format)
                                    then <format>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:mes)
                                    then <mes>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecbajad/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_OPCOCUO/ns4:fecbajad/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecbajad>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:fecfac)
                            then <fecfac>
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:anno)
                                    then <anno>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:dia)
                                    then <dia>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:format)
                                    then <format>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:mes)
                                    then <mes>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecfac/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_OPCOCUO/ns4:fecfac/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecfac>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:fecultliq)
                            then <fecultliq>
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:anno)
                                    then <anno>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:dia)
                                    then <dia>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:format)
                                    then <format>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:mes)
                                    then <mes>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCOCUO/ns4:fecultliq/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_OPCOCUO/ns4:fecultliq/ns5:valueDate)}</valueDate>
                                    else ()
                                }</fecultliq>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impamorant)
                            then <impamorant>{fn:data($registros_OPCOCUO/ns4:impamorant)}</impamorant>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impcapamort)
                            then <impcapamort>{fn:data($registros_OPCOCUO/ns4:impcapamort)}</impcapamort>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impcapital)
                            then <impcapital>{fn:data($registros_OPCOCUO/ns4:impcapital)}</impcapital>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impcccu)
                            then <impcccu>{fn:data($registros_OPCOCUO/ns4:impcccu)}</impcccu>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impcuomod)
                            then <impcuomod>{fn:data($registros_OPCOCUO/ns4:impcuomod)}</impcuomod>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impcuota)
                            then <impcuota>{fn:data($registros_OPCOCUO/ns4:impcuota)}</impcuota>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impfac)
                            then <impfac>{fn:data($registros_OPCOCUO/ns4:impfac)}</impfac>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impintamort)
                            then <impintamort>{fn:data($registros_OPCOCUO/ns4:impintamort)}</impintamort>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impintereses)
                            then <impintereses>{fn:data($registros_OPCOCUO/ns4:impintereses)}</impintereses>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:imptocancel)
                            then <imptocancel>{fn:data($registros_OPCOCUO/ns4:imptocancel)}</imptocancel>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:imptocomape)
                            then <imptocomape>{fn:data($registros_OPCOCUO/ns4:imptocomape)}</imptocomape>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:imptotal)
                            then <imptotal>{fn:data($registros_OPCOCUO/ns4:imptotal)}</imptotal>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:imptotamort)
                            then <imptotamort>{fn:data($registros_OPCOCUO/ns4:imptotamort)}</imptotamort>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:impuesto)
                            then <impuesto>{fn:data($registros_OPCOCUO/ns4:impuesto)}</impuesto>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:indaccion)
                            then <indaccion>{fn:data($registros_OPCOCUO/ns4:indaccion)}</indaccion>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:indsimconf)
                            then <indsimconf>{fn:data($registros_OPCOCUO/ns4:indsimconf)}</indsimconf>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:intanual)
                            then <intanual>{fn:data($registros_OPCOCUO/ns4:intanual)}</intanual>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:intcarencia)
                            then <intcarencia>{fn:data($registros_OPCOCUO/ns4:intcarencia)}</intcarencia>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:intpte)
                            then <intpte>{fn:data($registros_OPCOCUO/ns4:intpte)}</intpte>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:intregulariz)
                            then <intregulariz>{fn:data($registros_OPCOCUO/ns4:intregulariz)}</intregulariz>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:linref)
                            then <linref>{fn:data($registros_OPCOCUO/ns4:linref)}</linref>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:motbaja)
                            then <motbaja>{fn:data($registros_OPCOCUO/ns4:motbaja)}</motbaja>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:nomcomred)
                            then <nomcomred>{fn:data($registros_OPCOCUO/ns4:nomcomred)}</nomcomred>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numaut)
                            then <numaut>{fn:data($registros_OPCOCUO/ns4:numaut)}</numaut>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numcuomod)
                            then <numcuomod>{fn:data($registros_OPCOCUO/ns4:numcuomod)}</numcuomod>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numcuopag)
                            then <numcuopag>{fn:data($registros_OPCOCUO/ns4:numcuopag)}</numcuopag>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numfinan)
                            then <numfinan>{fn:data($registros_OPCOCUO/ns4:numfinan)}</numfinan>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numopecuo)
                            then <numopecuo>{fn:data($registros_OPCOCUO/ns4:numopecuo)}</numopecuo>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:numreffac)
                            then <numreffac>{fn:data($registros_OPCOCUO/ns4:numreffac)}</numreffac>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:pan)
                            then <pan>{fn:data($registros_OPCOCUO/ns4:pan)}</pan>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:porintorig)
                            then <porintorig>{fn:data($registros_OPCOCUO/ns4:porintorig)}</porintorig>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:tipolin)
                            then <tipolin>{fn:data($registros_OPCOCUO/ns4:tipolin)}</tipolin>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:totalpte)
                            then <totalpte>{fn:data($registros_OPCOCUO/ns4:totalpte)}</totalpte>
                            else ()
                        }
                        {
                            if ($registros_OPCOCUO/ns4:totcuotas)
                            then <totcuotas>{fn:data($registros_OPCOCUO/ns4:totcuotas)}</totcuotas>
                            else ()
                        }
                    </registros_OPCOCUO>
                }</return>
            else ()
        }</ns2:runServiceResponse>
};

local:XQ_OUT_BS_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotas_TO_PX_FIF_CORP_SAT_OPCOCUO_DetalleCompraCuotasImpl($runServiceResponse)