xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$vEN_ObtenerCreditosResponse1" element="ns1:VEN_ObtenerCreditosResponse" location="../WSDL/Vencidos.asmx.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:creditoVencidoObtenerRespParam" location="../Schemas/OSB_VEN_Credito_Vencido_ObtenerResp.xsd" ::)

declare namespace ns1 = "http://falabella.Vencidos.servicio.pe";
declare namespace ns0 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ven/credito/vencido/obtener/Resp-v2016.02";
declare namespace xf = "http://tempuri.org/BCO_PE_VEN_Credito_VencidoObtener-v1_0_IMPL/Resources/XQuery/xq_OUT_Credito_VencidoObtenerVEN_to_Credito_VencidoObtenerImpl/";

declare function xf:xq_OUT_Credito_VencidoObtenerVEN_to_Credito_VencidoObtenerImpl($vEN_ObtenerCreditosResponse1 as element(ns1:VEN_ObtenerCreditosResponse))
    as element(ns0:creditoVencidoObtenerRespParam) {
        <ns0:creditoVencidoObtenerRespParam>
            {
                for $CreditoVencido in $vEN_ObtenerCreditosResponse1/ns1:VEN_ObtenerCreditosResult/ns1:LCreditosVencidos/ns1:CreditoVencido
                return
                    <ns0:creditoVencido>
                        <ns0:codigoProducto>{ data(fn:substring($CreditoVencido/ns1:numeroCredito,1,2)) }</ns0:codigoProducto>
                        <ns0:identificadorProducto>{ data($CreditoVencido/ns1:numeroCredito) }</ns0:identificadorProducto>
                        <ns0:saldoTotal>{ data($CreditoVencido/ns1:valBien) }</ns0:saldoTotal>
                        {
                            for $moneda in $CreditoVencido/ns1:moneda
                            return
                                <ns0:moneda>{ data($moneda) }</ns0:moneda>
                        }
                        <ns0:estado>{ data($CreditoVencido/ns1:estado) }</ns0:estado>
                        <ns0:situacion>{ data($CreditoVencido/ns1:situacion) }</ns0:situacion>
                        <ns0:fechaMigracion>{ data($CreditoVencido/ns1:fechaMigracion) }</ns0:fechaMigracion>
                        <ns0:fechaDesembolso>{ data($CreditoVencido/ns1:fechaDesembolso) }</ns0:fechaDesembolso>
                        <ns0:fechaUltCuotaMorosa>{ data($CreditoVencido/ns1:fechaUltCuotaMorosa) }</ns0:fechaUltCuotaMorosa>
                    </ns0:creditoVencido>
            }
        </ns0:creditoVencidoObtenerRespParam>
};

declare variable $vEN_ObtenerCreditosResponse1 as element(ns1:VEN_ObtenerCreditosResponse) external;

xf:xq_OUT_Credito_VencidoObtenerVEN_to_Credito_VencidoObtenerImpl($vEN_ObtenerCreditosResponse1)