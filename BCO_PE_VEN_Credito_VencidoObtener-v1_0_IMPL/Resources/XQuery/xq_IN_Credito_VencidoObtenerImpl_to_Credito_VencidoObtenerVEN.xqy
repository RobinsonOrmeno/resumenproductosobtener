xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$creditoVencidoObtenerReqParam1" element="ns1:creditoVencidoObtenerReqParam" location="../Schemas/OSB_VEN_Credito_Vencido_ObtenerReq.xsd" ::)
(:: pragma bea:global-element-return element="ns0:VEN_ObtenerCreditos" location="../WSDL/Vencidos.asmx.wsdl" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Ven/credito/vencido/obtener/Req-v2016.02";
declare namespace ns0 = "http://falabella.Vencidos.servicio.pe";
declare namespace xf = "http://tempuri.org/BCO_PE_VEN_Credito_VencidoObtener-v1_0_IMPL/Resources/XQuery/xq_IN_Credito_VencidoObtenerImpl_to_Credito_VencidoObtenerVEN/";

declare function xf:xq_IN_Credito_VencidoObtenerImpl_to_Credito_VencidoObtenerVEN($creditoVencidoObtenerReqParam1 as element(ns1:creditoVencidoObtenerReqParam))
    as element(ns0:VEN_ObtenerCreditos) {
        <ns0:VEN_ObtenerCreditos>
			<ns0:TipDoc>{ data($creditoVencidoObtenerReqParam1/ns1:tipoDocumento) }</ns0:TipDoc>
            <ns0:NumDoc>{ data($creditoVencidoObtenerReqParam1/ns1:numeroDocumento) }</ns0:NumDoc>
        </ns0:VEN_ObtenerCreditos>
};

declare variable $creditoVencidoObtenerReqParam1 as element(ns1:creditoVencidoObtenerReqParam) external;

xf:xq_IN_Credito_VencidoObtenerImpl_to_Credito_VencidoObtenerVEN($creditoVencidoObtenerReqParam1)