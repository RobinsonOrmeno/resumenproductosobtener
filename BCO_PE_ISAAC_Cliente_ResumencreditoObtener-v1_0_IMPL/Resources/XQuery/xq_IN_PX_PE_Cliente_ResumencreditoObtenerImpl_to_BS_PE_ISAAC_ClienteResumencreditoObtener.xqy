xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$iSAAC_ResumenCreditoObtenerImplReq1" element="ns1:ISAAC_ResumenCreditoObtenerImplReq" location="../Schema/OSB_Cliente_Resumencredito_ObtenerReq.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ISAAC_ResumenCreditoObtenerImplReq" location="../Schema/WLS_Cliente_Resumencredito_ObtenerReq.xsd" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Req-v2015.12";
declare namespace ns0 = "http://mdwcorp.falabella.com/WLS/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Req-v2015.12";
declare namespace xf = "http://tempuri.org/BCO_PE_ISAAC_Cliente%20_ResumencreditoObtener-v1_0_IMPL/Resources/XQuery/xq_IN_PX_PE_Cliente_ResumencreditoObtenerImpl_to_BS_PE_ISAAC_ClienteResumencreditoObtener/";

declare function xf:xq_IN_PX_PE_Cliente_ResumencreditoObtenerImpl_to_BS_PE_ISAAC_ClienteResumencreditoObtener($iSAAC_ResumenCreditoObtenerImplReq1 as element(ns1:ISAAC_ResumenCreditoObtenerImplReq))
    as element(ns0:ISAAC_ResumenCreditoObtenerImplReq) {
        <ns0:ISAAC_ResumenCreditoObtenerImplReq>
            <ns0:TIPO_DOCUMENTO>{ data($iSAAC_ResumenCreditoObtenerImplReq1/ns1:TIPO_DOCUMENTO) }</ns0:TIPO_DOCUMENTO>
            <ns0:NRO_DOCUMENTO>{ data($iSAAC_ResumenCreditoObtenerImplReq1/ns1:NRO_DOCUMENTO) }</ns0:NRO_DOCUMENTO>
        </ns0:ISAAC_ResumenCreditoObtenerImplReq>
};

declare variable $iSAAC_ResumenCreditoObtenerImplReq1 as element(ns1:ISAAC_ResumenCreditoObtenerImplReq) external;

xf:xq_IN_PX_PE_Cliente_ResumencreditoObtenerImpl_to_BS_PE_ISAAC_ClienteResumencreditoObtener($iSAAC_ResumenCreditoObtenerImplReq1)