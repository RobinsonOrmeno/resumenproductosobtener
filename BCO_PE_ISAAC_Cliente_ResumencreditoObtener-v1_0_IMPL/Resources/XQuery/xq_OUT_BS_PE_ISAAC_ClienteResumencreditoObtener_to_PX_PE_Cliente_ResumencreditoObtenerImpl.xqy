xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$iSAAC_ResumenCreditoObtenerImplRes1" element="ns1:ISAAC_ResumenCreditoObtenerImplRes" location="../Schema/WLS_Cliente_Resumencredito_ObtenerResp.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ISAAC_ResumenCreditoObtenerImplRes" location="../Schema/OSB_Cliente_Resumencredito_ObtenerResp.xsd" ::)

declare namespace ns1 = "http://mdwcorp.falabella.com/WLS/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Resp-v2015.12";
declare namespace ns0 = "http://mdwcorp.falabella.com/OSB/schema/BCO/PE/Isaac/cliente/resumencredito/obtener/Resp-v2015.12";
declare namespace xf = "http://tempuri.org/BCO_PE_ISAAC_Cliente%20_ResumencreditoObtener-v1_0_IMPL/Resources/XQuery/xq_OUT_BS_PE_ISAAC_ClienteResumencreditoObtener_to_PX_PE_Cliente_ResumencreditoObtenerImpl/";

declare function xf:xq_OUT_BS_PE_ISAAC_ClienteResumencreditoObtener_to_PX_PE_Cliente_ResumencreditoObtenerImpl($iSAAC_ResumenCreditoObtenerImplRes1 as element(ns1:ISAAC_ResumenCreditoObtenerImplRes))
    as element(ns0:ISAAC_ResumenCreditoObtenerImplRes) {
        <ns0:ISAAC_ResumenCreditoObtenerImplRes>
            <ns0:creditos>
                {
                    for $credito in $iSAAC_ResumenCreditoObtenerImplRes1/ns1:creditos/ns1:credito
                    return
                        <ns0:credito>
                            <ns0:COD_PRODUCTO>{ data($credito/ns1:COD_PRODUCTO) }</ns0:COD_PRODUCTO>
                            <ns0:DESC_PRODUCTO>{ data($credito/ns1:DESC_PRODUCTO) }</ns0:DESC_PRODUCTO>
                            <ns0:COD_SUBPRODUCTO>{ data($credito/ns1:COD_SUBPRODUCTO) }</ns0:COD_SUBPRODUCTO>
                            <ns0:NRO_CREDITO>{ data($credito/ns1:NRO_CREDITO) }</ns0:NRO_CREDITO>
                            <ns0:CODIGO_ESTADO>{ data($credito/ns1:CODIGO_ESTADO) }</ns0:CODIGO_ESTADO>
                            <ns0:ESTADO_CREDITO>{ data($credito/ns1:ESTADO_CREDITO) }</ns0:ESTADO_CREDITO>
                            <ns0:GLOSA_ESTADO>{ data($credito/ns1:GLOSA_ESTADO) }</ns0:GLOSA_ESTADO>
                            <ns0:COD_MONEDA>{ data($credito/ns1:COD_MONEDA) }</ns0:COD_MONEDA>
                            <ns0:MONTO_CREDITO>{ data($credito/ns1:MONTO_CREDITO) }</ns0:MONTO_CREDITO>
                            <ns0:TOTAL_CUOTAS>{ data($credito/ns1:TOTAL_CUOTAS) }</ns0:TOTAL_CUOTAS>
                            <ns0:CUOTAS_PAGADAS>{ data($credito/ns1:CUOTAS_PAGADAS) }</ns0:CUOTAS_PAGADAS>
                            <ns0:NRO_PROX_CUOTA>{ data($credito/ns1:NRO_PROX_CUOTA) }</ns0:NRO_PROX_CUOTA>
                            <ns0:VAL_PROX_CUOTA>{ data($credito/ns1:VAL_PROX_CUOTA) }</ns0:VAL_PROX_CUOTA>
                            {
                                for $FECVEN_PROX_CUOTA in $credito/ns1:FECVEN_PROX_CUOTA
                                return
                                    <ns0:FECVEN_PROX_CUOTA>{ data($FECVEN_PROX_CUOTA) }</ns0:FECVEN_PROX_CUOTA>
                            }
                        </ns0:credito>
                }
            </ns0:creditos>
        </ns0:ISAAC_ResumenCreditoObtenerImplRes>
};

declare variable $iSAAC_ResumenCreditoObtenerImplRes1 as element(ns1:ISAAC_ResumenCreditoObtenerImplRes) external;

xf:xq_OUT_BS_PE_ISAAC_ClienteResumencreditoObtener_to_PX_PE_Cliente_ResumencreditoObtenerImpl($iSAAC_ResumenCreditoObtenerImplRes1)