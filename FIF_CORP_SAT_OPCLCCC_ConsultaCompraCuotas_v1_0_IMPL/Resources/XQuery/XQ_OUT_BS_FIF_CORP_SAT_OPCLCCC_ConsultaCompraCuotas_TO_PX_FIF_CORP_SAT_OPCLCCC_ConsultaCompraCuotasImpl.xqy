xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace my="http://www.w3.org/2005/xquery-local-functions";
import module "http://www.w3.org/2005/xquery-local-functions" at "../../../FIF_CORP_Comun_V1_0/MDW/XQUERY/xq_utils.xqy";

declare namespace ns2="http://mdwcorp.falabella.com/FIF/CORP/OSB/schema/SAT/OPCLCCC/ConsultaCompraCuotas/Resp/IMPL/v2017.12";
(:: import schema at "../Schemas/OSB_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_IMPL_Resp.xsd" ::)
declare namespace ns1="http://webservice.sat.mediosdepago.tecnocom.com";
(:: import schema at "../WSDL/SAT_OPCLCCCWS.wsdl" ::)

declare namespace ns3 = "http://satNewAge.soapwebservices.ease/xsd";

declare namespace ns4 = "http://webservice.sat.mediosdepago.tecnocom.com/xsd";

declare namespace ns5 = "http://commons.soapwebservices.ease/xsd";

declare variable $runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::) external;

declare function local:XQ_OUT_BS_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_TO_PX_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotasImpl($runServiceResponse as element() (:: schema-element(ns1:runServiceResponse) ::)) as element() (:: schema-element(ns2:runServiceResponse) ::) {
    <ns2:runServiceResponse>
        {
            if ($runServiceResponse/ns1:return)
            then <return>
                {
                    if ($runServiceResponse/ns1:return/ns3:claveFin)
                    then <claveFin>{fn:data($runServiceResponse/ns1:return/ns3:claveFin)}</claveFin>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:claveInicio)
                    then <claveInicio>{fn:data($runServiceResponse/ns1:return/ns3:claveInicio)}</claveInicio>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:descRetorno)
                    then <descRetorno>{fn:data($runServiceResponse/ns1:return/ns3:descRetorno)}</descRetorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:indMasDatos)
                    then <indMasDatos>{fn:data($runServiceResponse/ns1:return/ns3:indMasDatos)}</indMasDatos>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:pantPagina)
                    then <pantPagina>{fn:data($runServiceResponse/ns1:return/ns3:pantPagina)}</pantPagina>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:retorno)
                    then <retorno>{fn:data($runServiceResponse/ns1:return/ns3:retorno)}</retorno>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:tiempoEjecucion)
                    then <tiempoEjecucion>{fn:data($runServiceResponse/ns1:return/ns3:tiempoEjecucion)}</tiempoEjecucion>
                    else ()
                }
                {
                    if ($runServiceResponse/ns1:return/ns3:totalRegistros)
                    then <totalRegistros>{fn:data($runServiceResponse/ns1:return/ns3:totalRegistros)}</totalRegistros>
                    else ()
                }
                {
                    for $registros_OPCLCCC in $runServiceResponse/ns1:return/ns4:registros_OPCLCCC
                    return 
                    <registros_OPCLCCC>
                        {
                            if ($registros_OPCLCCC/ns5:registro_Numero)
                            then <registro_Numero>{fn:data($registros_OPCLCCC/ns5:registro_Numero)}</registro_Numero>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:clamon)
                            then <clamon>{fn:data($registros_OPCLCCC/ns4:clamon)}</clamon>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:codcom)
                            then <codcom>{fn:data($registros_OPCLCCC/ns4:codcom)}</codcom>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:codtipc)
                            then <codtipc>{fn:data($registros_OPCLCCC/ns4:codtipc)}</codtipc>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:contcur)
                            then <contcur>{fn:data($registros_OPCLCCC/ns4:contcur)}</contcur>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:desclamon)
                            then <desclamon>{fn:data($registros_OPCLCCC/ns4:desclamon)}</desclamon>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:desestcom)
                            then <desestcom>{fn:data($registros_OPCLCCC/ns4:desestcom)}</desestcom>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:destipcred)
                            then <destipcred>{fn:data($registros_OPCLCCC/ns4:destipcred)}</destipcred>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:destipolin)
                            then <destipolin>{fn:data($registros_OPCLCCC/ns4:destipolin)}</destipolin>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:estcompra)
                            then <estcompra>{fn:data($registros_OPCLCCC/ns4:estcompra)}</estcompra>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:fecfac)
                            then <fecfac>
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:anno)
                                    then <anno>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:anno)}</anno>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:dia)
                                    then <dia>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:dia)}</dia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:format)
                                    then <format>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:format)}</format>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:mes)
                                    then <mes>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:mes)}</mes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:nombreDia)
                                    then <nombreDia>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:nombreDia)}</nombreDia>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:nombreMes)
                                    then <nombreMes>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:nombreMes)}</nombreMes>
                                    else ()
                                }
                                {
                                    if ($registros_OPCLCCC/ns4:fecfac/ns5:valueDate)
                                    then <valueDate>{fn:data($registros_OPCLCCC/ns4:fecfac/ns5:valueDate)}</valueDate>
                                    else ()
                                }
                                </fecfac>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:impfac)
                            then <impfac>{my:funcionNotacion(xs:string(fn:data($registros_OPCLCCC/ns4:impfac)))}</impfac>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:linref)
                            then <linref>{fn:data($registros_OPCLCCC/ns4:linref)}</linref>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:nomcomred)
                            then <nomcomred>{fn:data($registros_OPCLCCC/ns4:nomcomred)}</nomcomred>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:numfinanl)
                            then <numfinanl>{fn:data($registros_OPCLCCC/ns4:numfinanl)}</numfinanl>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:numopecuol)
                            then <numopecuol>{fn:data($registros_OPCLCCC/ns4:numopecuol)}</numopecuol>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:pand)
                            then <pand>{fn:data($registros_OPCLCCC/ns4:pand)}</pand>
                            else ()
                        }
                        {
                            if ($registros_OPCLCCC/ns4:tipolin)
                            then <tipolin>{fn:data($registros_OPCLCCC/ns4:tipolin)}</tipolin>
                            else ()
                        }</registros_OPCLCCC>
                }</return>
            else ()
        }</ns2:runServiceResponse>
};

local:XQ_OUT_BS_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotas_TO_PX_FIF_CORP_SAT_OPCLCCC_ConsultaCompraCuotasImpl($runServiceResponse)